unit uRemontRight;

interface
uses classes, System.SysUtils,  System.Generics.Collections , uData2,UIAisUmzForMgis,
     UAisUmzforMgisClass , Data.DB, IBCustomDataSet, IBQuery ,System.Variants, Vcl.Forms,

      AISUMZDB.Types, AISUMZDB.SQLTEXTS;

type


    tRemont = class
    private
    myI : IAisUMZForMgis;
    procedure FindRIghtsID(vLP: integer; var Rights_id : TArray<double> );
    function GenRightId(vLP: integer): Double;
    function GenRightRegId(vLP: integer): Double;
    public
    constructor Create();
    destructor Destroy; override;
    property MYINterface:IAisUMZForMgis  read MyI write MyI;

    procedure CheckBase(vlp:integer);
    //procedure GetZu(var ZemUch:TzemUch; vKN:string);overload;
    //procedure GetZu(var ZemRight:TzemRight; vKN:string); overload;
  end;
var Rights_id : TArray<double>;
    Lands_id : TArray<double>;

implementation
uses Unit4;

{ tRemont }
function tRemont.GenRightId(vLP: integer): Double;
var ii:Double;
begin
  with dm2 do
   begin
    ii := StrTofloat(IntToStr(vLP)+'000000000');

    ibqryGenId.Database :=  myI.Mydb;
    ibqryGenId.sql.clear;
    ibqryGenId.sql.add(_sqlGenRightId);
    ibqryGenId.Open;
    Result := ii + ibqryGenId.Fields[0].AsFloat;
    ibqryGenId.Close;
   end;
end;

function tRemont.GenRightRegId(vLP: integer): Double;
var ii:Double;
 begin
  with dm2 do
   begin
    ii := StrTofloat(IntToStr(vLP)+'000000000');

    ibqryGenId.Database :=  myI.Mydb;
    ibqryGenId.sql.clear;
    ibqryGenId.sql.add(_sqlGenryteregId);
    ibqryGenId.Open;
    Result := ii + ibqryGenId.Fields[0].AsFloat;
    ibqryGenId.Close;
   end;

end;

procedure tRemont.CheckBase(vlp: integer);
var i,con,ii:Integer; id,idd:Double;
begin
  FindRIghtsID(vlp,Rights_id);
  if High(Rights_id)>0 then
  begin
     with dm2 do
     begin
      //Form4.mmo1.Lines.Add('������� ������ ����:' + inttostr(High(Rights_id)));
       form4.WriteTofile(ff, '������� ������ ����:' + inttostr(High(Rights_id)),'');
      ibqrySelect.close;
      ibqrySelect.Database :=  myI.Mydb;
      ibqrySelect.sql.clear;
      ibqrySelect.sql.add(_sqlSelectRight);

      ibqrySelect2.close;
      ibqrySelect2.Database :=  myI.Mydb;
      ibqrySelect2.sql.clear;
      ibqrySelect2.sql.add(_sqlSelectLAndRight);

      ibqrySelect3.close;
      ibqrySelect3.Database :=  myI.Mydb;
      ibqrySelect3.sql.clear;
      ibqrySelect3.sql.add(_sqlSelectRightRegtype);

      ibqryInsert.close;
      ibqryInsert.Database :=  myI.Mydb;
      ibqryInsert.sql.clear;
      ibqryInsert.sql.add(_sqlInsertRight_1);
      ibqryInsert.sql.add(_sqlInsertRight_2);

      ibqryInsert2.close;
      ibqryInsert2.Database :=  myI.Mydb;
      ibqryInsert2.sql.clear;
      ibqryInsert2.sql.add(_sqlInsertRightregistration);

      ibqryUpdate.close;
      ibqryUpdate.Database :=  myI.Mydb;
      ibqryUpdate.sql.clear;
      ibqryUpdate.sql.add(_sqlUpdateLR);


      for I := Low(Rights_id) to High(Rights_id) do
      begin
            Application.ProcessMessages;
            //Form4.mmo1.Lines.Add('����� ' + inttostr(i));
             form4.WriteTofile(ff,'����� ' + inttostr(i), '');
            Lands_id := nil;
            con := 0;
         // select right_id
            ibqrySelect.close;
            ibqrySelect.Params[0].AsFloat := Rights_id[i];
            ibqrySelect.Open;

         // select LAND_ID
            ibqrySelect2.close;
            ibqrySelect2.Params[0].AsFloat := Rights_id[i];
            ibqrySelect2.Open;
            ibqrySelect2.First;
            while not ibqrySelect2.eof do
            begin
              con := con+1;
              ibqrySelect2.Next ;
            end;
            ibqrySelect2.First;
            SetLength(Lands_id,con);
            for II := 0 to con-1 do
              begin
                Lands_id[ii] := ibqrySelect2.Fields[0].AsFloat;
                ibqrySelect2.Next;
              end;
            form4.WriteTofile(ff,'������������� �� ����� ' + inttostr(con), '');
            //Form4.mmo1.Lines.Add('������������� �� ����� ' + inttostr(con));
            Application.ProcessMessages;
            for ii := Low(Lands_id) to High(Lands_id) do
             begin
              // insert right_id
                id := GenRightId(vLP);
                ibqryInsert.Close;
                ibqryInsert.Params[0].Value :=  id;   //ibqrySelect.Fields[0].Value;
                ibqryInsert.Params[1].Value :=  ibqrySelect.Fields[1].Value;
                ibqryInsert.Params[2].Value :=  ibqrySelect.Fields[2].Value;
                ibqryInsert.Params[3].Value :=  ibqrySelect.Fields[3].Value;
                ibqryInsert.Params[4].Value :=  ibqrySelect.Fields[4].Value;
                ibqryInsert.Params[5].Value :=  ibqrySelect.Fields[5].Value;
                ibqryInsert.Params[6].Value :=  ibqrySelect.Fields[6].Value;
                ibqryInsert.Params[7].Value :=  ibqrySelect.Fields[7].Value;
                ibqryInsert.Params[8].Value :=  ibqrySelect.Fields[8].Value;
                ibqryInsert.Params[9].Value :=  ibqrySelect.Fields[9].Value;
                ibqryInsert.Params[10].Value :=  ibqrySelect.Fields[10].Value;
                ibqryInsert.Params[11].Value :=  ibqrySelect.Fields[11].Value;
                ibqryInsert.Params[12].Value :=  0;//ibqrySelect.Fields[0].Value;
                ibqryInsert.Params[13].Value :=  ibqrySelect.Fields[13].Value;
                ibqryInsert.Params[14].Value :=  ibqrySelect.Fields[14].Value;
                ibqryInsert.ExecSQL;
              // update land_right
                ibqryUpdate.Close;
                ibqryUpdate.Params[0].Value :=  id;
                ibqryUpdate.Params[1].Value :=  Lands_id[ii];
                ibqryUpdate.Params[2].Value :=  Rights_id[i];
                ibqryUpdate.ExecSQL;
             // ���� �������/����
                ibqrySelect3.close;
                ibqrySelect3.Params[0].AsFloat := Rights_id[i];
                ibqrySelect3.Open;
                ibqrySelect3.First;
                while not ibqrySelect3.eof do
                begin
                  if ibqrySelect3.FieldByName('REGISTRATION_TYPE_ID').Value = 1 then
                    begin
                      idd := GenRightRegId(vLP);
                      ibqryInsert2.Close;
                      ibqryInsert2.Params[0].Value :=  idd;   //ibqrySelect.Fields[0].Value;
                      ibqryInsert2.Params[1].Value :=  1;
                      ibqryInsert2.Params[2].Value :=  id;
                      ibqryInsert2.Params[3].Value :=  ibqrySelect3.Fields[3].Value;
                      ibqryInsert2.Params[4].Value :=  ibqrySelect3.Fields[4].Value;
                      ibqryInsert2.ExecSQL;
                    end;
                  if ibqrySelect3.FieldByName('REGISTRATION_TYPE_ID').Value = 2 then
                    begin
                      idd := GenRightRegId(vLP);
                      ibqryInsert2.Close;
                      ibqryInsert2.Params[0].Value :=  idd;   //ibqrySelect.Fields[0].Value;
                      ibqryInsert2.Params[1].Value :=  2;
                      ibqryInsert2.Params[2].Value :=  id;
                      ibqryInsert2.Params[3].Value :=  ibqrySelect3.Fields[3].Value;
                      ibqryInsert2.Params[4].Value :=  ibqrySelect3.Fields[4].Value;
                      ibqryInsert2.ExecSQL;
                    end;
                  ibqrySelect3.Next;
                end;

             end;
      end;
     end;
  end;
end;

constructor tRemont.Create;
begin

end;

destructor tRemont.Destroy;
begin

  inherited;
end;

procedure tRemont.FindRIghtsID(vLP: integer; var Rights_id : TArray<double> );
var i,icount: Integer;
begin
  Rights_id := nil;
  i:=0;
  icount := 0;
  with dm2 do
  begin
   ibqryFind.Database :=  myI.Mydb;
   ibqryFind.sql.clear;
   ibqryFind.sql.add(_sqlSelectmultiRights);
   ibqryFind.Params[0].AsInteger := vLP;
   ibqryFind.Open;
   if not ibqryFind.IsEmpty then
   begin
     ibqryFind.First;
     while not ibqryFind.Eof do
      begin
       icount := icount +1;
       ibqryFind.Next;
      end;
     ibqryFind.First;
     SetLength(Rights_id,icount);
     for I := 0 to icount-1 do
       begin
        Rights_id[i] := ibqryFind.Fields[0].AsFloat;
        ibqryFind.Next;
       end;
   end;
   ibqryFind.Close;
  end;
end;

end.
