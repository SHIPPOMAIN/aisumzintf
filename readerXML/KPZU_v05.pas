
{****************************************************************}
{                                                                }
{                        XML Data Binding                        }
{                                                                }
{         Generated on: 12.04.2016 10:27:35                      }
{       Generated from: D:\KPZU_v05\KPZU_v05\KPZU\KPZU_v05.xsd   }
{   Settings stored in: D:\KPZU_v05\KPZU_v05\KPZU\KPZU_v05.xdb   }
{                                                                }
{****************************************************************}

unit KPZU_v05;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLKPZU = interface;
  IXMLTParcel = interface;
  IXMLTOldNumbers_Num1 = interface;
  IXMLTOldNumber_Num1 = interface;
  IXMLTCadastralNumbersOut = interface;
  IXMLTAreaOut = interface;
  IXMLTLocation = interface;
  IXMLTElaborationLocation = interface;
  IXMLTAddressOut_adrOut3 = interface;
  IXMLTName_adrOut3 = interface;
  IXMLTUrbanDistrict_adrOut3 = interface;
  IXMLTSovietVillage_adrOut3 = interface;
  IXMLTLevel1_adrOut3 = interface;
  IXMLTLevel2_adrOut3 = interface;
  IXMLTLevel3_adrOut3 = interface;
  IXMLTApartment_adrOut3 = interface;
  IXMLTUtilization = interface;
  IXMLTNaturalObjects_NatObj1 = interface;
  IXMLTNaturalObject_NatObj1 = interface;
  IXMLTParcel_Rights = interface;
  IXMLTRight = interface;
  IXMLTOwners = interface;
  IXMLTOwner = interface;
  IXMLTOwner_Person = interface;
  IXMLTNameOwner = interface;
  IXMLTShare = interface;
  IXMLTRegistration = interface;
  IXMLTRight_Documents = interface;
  IXMLTDocumentWithoutAppliedFile_DocOut3 = interface;
  IXMLTParcel_SubParcels = interface;
  IXMLTSubParcel = interface;
  IXMLTAreaWithoutInaccuracyOut = interface;
  IXMLTEncumbranceZU = interface;
  IXMLTOwnerRestrictionInFavorem = interface;
  IXMLTDuration = interface;
  IXMLTEntitySpatialZUOut_Spa2 = interface;
  IXMLTSpatialElementZUOut_Spa2 = interface;
  IXMLTSpelementUnitZUOut_Spa2 = interface;
  IXMLTOrdinateOut_Spa2 = interface;
  IXMLOrdinate_Spa2 = interface;
  IXMLTParcel_Contours = interface;
  IXMLTContour = interface;
  IXMLTParcel_CompositionEZ = interface;
  IXMLTEntryParcel = interface;
  IXMLTParcel_Encumbrances = interface;
  IXMLTCadastralCost_Cos1 = interface;
  IXMLTCadastralNumberOut = interface;
  IXMLTCertificationDoc_Cer1 = interface;
  IXMLOfficial_Cer1 = interface;
  IXMLKPZU_Contractors = interface;
  IXMLTEngineerOut = interface;
  IXMLTOrganizationNameOut = interface;
  IXMLKPZU_Contractors_Contractor = interface;
  IXMLTCoordSystems_Spa2 = interface;
  IXMLTCoordSystem_Spa2 = interface;

{ IXMLKPZU }

  IXMLKPZU = interface(IXMLNode)
    ['{6642FDF1-F132-4A1E-9273-954004011FA6}']
    { Property Accessors }
    function Get_Parcel: IXMLTParcel;
    function Get_CertificationDoc: IXMLTCertificationDoc_Cer1;
    function Get_Contractors: IXMLKPZU_Contractors;
    function Get_CoordSystems: IXMLTCoordSystems_Spa2;
    { Methods & Properties }
    property Parcel: IXMLTParcel read Get_Parcel;
    property CertificationDoc: IXMLTCertificationDoc_Cer1 read Get_CertificationDoc;
    property Contractors: IXMLKPZU_Contractors read Get_Contractors;
    property CoordSystems: IXMLTCoordSystems_Spa2 read Get_CoordSystems;
  end;

{ IXMLTParcel }

  IXMLTParcel = interface(IXMLNode)
    ['{C0EF0B89-D0F1-436E-8C65-22D9FB338F3C}']
    { Property Accessors }
    function Get_CadastralNumber: UnicodeString;
    function Get_State: UnicodeString;
    function Get_DateExpiry: UnicodeString;
    function Get_DateCreated: UnicodeString;
    function Get_DateRemoved: UnicodeString;
    function Get_Method: UnicodeString;
    function Get_DateCreatedDoc: UnicodeString;
    function Get_CadastralBlock: UnicodeString;
    function Get_Name: UnicodeString;
    function Get_OldNumbers: IXMLTOldNumbers_Num1;
    function Get_PrevCadastralNumbers: IXMLTCadastralNumbersOut;
    function Get_InnerCadastralNumbers: IXMLTCadastralNumbersOut;
    function Get_Area: IXMLTAreaOut;
    function Get_Location: IXMLTLocation;
    function Get_Category: UnicodeString;
    function Get_Utilization: IXMLTUtilization;
    function Get_NaturalObjects: IXMLTNaturalObjects_NatObj1;
    function Get_Rights: IXMLTParcel_Rights;
    function Get_SubParcels: IXMLTParcel_SubParcels;
    function Get_EntitySpatial: IXMLTEntitySpatialZUOut_Spa2;
    function Get_Contours: IXMLTParcel_Contours;
    function Get_CompositionEZ: IXMLTParcel_CompositionEZ;
    function Get_Encumbrances: IXMLTParcel_Encumbrances;
    function Get_CadastralCost: IXMLTCadastralCost_Cos1;
    function Get_SpecialNote: UnicodeString;
    function Get_AllNewParcels: IXMLTCadastralNumbersOut;
    function Get_ChangeParcel: IXMLTCadastralNumberOut;
    function Get_RemoveParcels: IXMLTCadastralNumbersOut;
    function Get_AllOffspringParcel: IXMLTCadastralNumbersOut;
    procedure Set_CadastralNumber(Value: UnicodeString);
    procedure Set_State(Value: UnicodeString);
    procedure Set_DateExpiry(Value: UnicodeString);
    procedure Set_DateCreated(Value: UnicodeString);
    procedure Set_DateRemoved(Value: UnicodeString);
    procedure Set_Method(Value: UnicodeString);
    procedure Set_DateCreatedDoc(Value: UnicodeString);
    procedure Set_CadastralBlock(Value: UnicodeString);
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Category(Value: UnicodeString);
    procedure Set_SpecialNote(Value: UnicodeString);
    { Methods & Properties }
    property CadastralNumber: UnicodeString read Get_CadastralNumber write Set_CadastralNumber;
    property State: UnicodeString read Get_State write Set_State;
    property DateExpiry: UnicodeString read Get_DateExpiry write Set_DateExpiry;
    property DateCreated: UnicodeString read Get_DateCreated write Set_DateCreated;
    property DateRemoved: UnicodeString read Get_DateRemoved write Set_DateRemoved;
    property Method: UnicodeString read Get_Method write Set_Method;
    property DateCreatedDoc: UnicodeString read Get_DateCreatedDoc write Set_DateCreatedDoc;
    property CadastralBlock: UnicodeString read Get_CadastralBlock write Set_CadastralBlock;
    property Name: UnicodeString read Get_Name write Set_Name;
    property OldNumbers: IXMLTOldNumbers_Num1 read Get_OldNumbers;
    property PrevCadastralNumbers: IXMLTCadastralNumbersOut read Get_PrevCadastralNumbers;
    property InnerCadastralNumbers: IXMLTCadastralNumbersOut read Get_InnerCadastralNumbers;
    property Area: IXMLTAreaOut read Get_Area;
    property Location: IXMLTLocation read Get_Location;
    property Category: UnicodeString read Get_Category write Set_Category;
    property Utilization: IXMLTUtilization read Get_Utilization;
    property NaturalObjects: IXMLTNaturalObjects_NatObj1 read Get_NaturalObjects;
    property Rights: IXMLTParcel_Rights read Get_Rights;
    property SubParcels: IXMLTParcel_SubParcels read Get_SubParcels;
    property EntitySpatial: IXMLTEntitySpatialZUOut_Spa2 read Get_EntitySpatial;
    property Contours: IXMLTParcel_Contours read Get_Contours;
    property CompositionEZ: IXMLTParcel_CompositionEZ read Get_CompositionEZ;
    property Encumbrances: IXMLTParcel_Encumbrances read Get_Encumbrances;
    property CadastralCost: IXMLTCadastralCost_Cos1 read Get_CadastralCost;
    property SpecialNote: UnicodeString read Get_SpecialNote write Set_SpecialNote;
    property AllNewParcels: IXMLTCadastralNumbersOut read Get_AllNewParcels;
    property ChangeParcel: IXMLTCadastralNumberOut read Get_ChangeParcel;
    property RemoveParcels: IXMLTCadastralNumbersOut read Get_RemoveParcels;
    property AllOffspringParcel: IXMLTCadastralNumbersOut read Get_AllOffspringParcel;
  end;

{ IXMLTOldNumbers_Num1 }

  IXMLTOldNumbers_Num1 = interface(IXMLNodeCollection)
    ['{27F8E683-430C-412B-AFDD-519D47C3B3D2}']
    { Property Accessors }
    function Get_OldNumber(Index: Integer): IXMLTOldNumber_Num1;
    { Methods & Properties }
    function Add: IXMLTOldNumber_Num1;
    function Insert(const Index: Integer): IXMLTOldNumber_Num1;
    property OldNumber[Index: Integer]: IXMLTOldNumber_Num1 read Get_OldNumber; default;
  end;

{ IXMLTOldNumber_Num1 }

  IXMLTOldNumber_Num1 = interface(IXMLNode)
    ['{0E95F04D-1723-48D3-AB66-1268E654F682}']
    { Property Accessors }
    function Get_Type_: UnicodeString;
    function Get_Number: UnicodeString;
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_Number(Value: UnicodeString);
    { Methods & Properties }
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
    property Number: UnicodeString read Get_Number write Set_Number;
  end;

{ IXMLTCadastralNumbersOut }

  IXMLTCadastralNumbersOut = interface(IXMLNodeCollection)
    ['{82D93AAB-3415-4600-A893-847F1B3A0041}']
    { Property Accessors }
    function Get_CadastralNumber(Index: Integer): UnicodeString;
    { Methods & Properties }
    function Add(const CadastralNumber: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const CadastralNumber: UnicodeString): IXMLNode;
    property CadastralNumber[Index: Integer]: UnicodeString read Get_CadastralNumber; default;
  end;

{ IXMLTAreaOut }

  IXMLTAreaOut = interface(IXMLNode)
    ['{288D2766-EED2-4947-9B90-40340756F719}']
    { Property Accessors }
    function Get_Area: LongWord;
    function Get_Unit_: UnicodeString;
    function Get_Inaccuracy: UnicodeString;
    procedure Set_Area(Value: LongWord);
    procedure Set_Unit_(Value: UnicodeString);
    procedure Set_Inaccuracy(Value: UnicodeString);
    { Methods & Properties }
    property Area: LongWord read Get_Area write Set_Area;
    property Unit_: UnicodeString read Get_Unit_ write Set_Unit_;
    property Inaccuracy: UnicodeString read Get_Inaccuracy write Set_Inaccuracy;
  end;

{ IXMLTLocation }

  IXMLTLocation = interface(IXMLNode)
    ['{DC4A8161-2DAD-4F54-BFF1-A11BB43959B0}']
    { Property Accessors }
    function Get_InBounds: UnicodeString;
    function Get_Placed: UnicodeString;
    function Get_Elaboration: IXMLTElaborationLocation;
    function Get_Address: IXMLTAddressOut_adrOut3;
    procedure Set_InBounds(Value: UnicodeString);
    procedure Set_Placed(Value: UnicodeString);
    { Methods & Properties }
    property InBounds: UnicodeString read Get_InBounds write Set_InBounds;
    property Placed: UnicodeString read Get_Placed write Set_Placed;
    property Elaboration: IXMLTElaborationLocation read Get_Elaboration;
    property Address: IXMLTAddressOut_adrOut3 read Get_Address;
  end;

{ IXMLTElaborationLocation }

  IXMLTElaborationLocation = interface(IXMLNode)
    ['{AC90F495-89EA-4122-9687-4E311308EFA1}']
    { Property Accessors }
    function Get_ReferenceMark: UnicodeString;
    function Get_Distance: UnicodeString;
    function Get_Direction: UnicodeString;
    procedure Set_ReferenceMark(Value: UnicodeString);
    procedure Set_Distance(Value: UnicodeString);
    procedure Set_Direction(Value: UnicodeString);
    { Methods & Properties }
    property ReferenceMark: UnicodeString read Get_ReferenceMark write Set_ReferenceMark;
    property Distance: UnicodeString read Get_Distance write Set_Distance;
    property Direction: UnicodeString read Get_Direction write Set_Direction;
  end;

{ IXMLTAddressOut_adrOut3 }

  IXMLTAddressOut_adrOut3 = interface(IXMLNode)
    ['{854428C2-539F-4E07-A12D-7BCA587F22C4}']
    { Property Accessors }
    function Get_OKATO: UnicodeString;
    function Get_KLADR: UnicodeString;
    function Get_OKTMO: UnicodeString;
    function Get_PostalCode: UnicodeString;
    function Get_Region: UnicodeString;
    function Get_District: IXMLTName_adrOut3;
    function Get_City: IXMLTName_adrOut3;
    function Get_UrbanDistrict: IXMLTUrbanDistrict_adrOut3;
    function Get_SovietVillage: IXMLTSovietVillage_adrOut3;
    function Get_Locality: IXMLTName_adrOut3;
    function Get_Street: IXMLTName_adrOut3;
    function Get_Level1: IXMLTLevel1_adrOut3;
    function Get_Level2: IXMLTLevel2_adrOut3;
    function Get_Level3: IXMLTLevel3_adrOut3;
    function Get_Apartment: IXMLTApartment_adrOut3;
    function Get_Other: UnicodeString;
    function Get_Note: UnicodeString;
    procedure Set_OKATO(Value: UnicodeString);
    procedure Set_KLADR(Value: UnicodeString);
    procedure Set_OKTMO(Value: UnicodeString);
    procedure Set_PostalCode(Value: UnicodeString);
    procedure Set_Region(Value: UnicodeString);
    procedure Set_Other(Value: UnicodeString);
    procedure Set_Note(Value: UnicodeString);
    { Methods & Properties }
    property OKATO: UnicodeString read Get_OKATO write Set_OKATO;
    property KLADR: UnicodeString read Get_KLADR write Set_KLADR;
    property OKTMO: UnicodeString read Get_OKTMO write Set_OKTMO;
    property PostalCode: UnicodeString read Get_PostalCode write Set_PostalCode;
    property Region: UnicodeString read Get_Region write Set_Region;
    property District: IXMLTName_adrOut3 read Get_District;
    property City: IXMLTName_adrOut3 read Get_City;
    property UrbanDistrict: IXMLTUrbanDistrict_adrOut3 read Get_UrbanDistrict;
    property SovietVillage: IXMLTSovietVillage_adrOut3 read Get_SovietVillage;
    property Locality: IXMLTName_adrOut3 read Get_Locality;
    property Street: IXMLTName_adrOut3 read Get_Street;
    property Level1: IXMLTLevel1_adrOut3 read Get_Level1;
    property Level2: IXMLTLevel2_adrOut3 read Get_Level2;
    property Level3: IXMLTLevel3_adrOut3 read Get_Level3;
    property Apartment: IXMLTApartment_adrOut3 read Get_Apartment;
    property Other: UnicodeString read Get_Other write Set_Other;
    property Note: UnicodeString read Get_Note write Set_Note;
  end;

{ IXMLTName_adrOut3 }

  IXMLTName_adrOut3 = interface(IXMLNode)
    ['{10B3D5A0-ED1A-44BD-B75C-6EE8F9F14F91}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    function Get_Type_: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    { Methods & Properties }
    property Name: UnicodeString read Get_Name write Set_Name;
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
  end;

{ IXMLTUrbanDistrict_adrOut3 }

  IXMLTUrbanDistrict_adrOut3 = interface(IXMLNode)
    ['{BE8055A9-1544-4350-A690-BB287B8D5726}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    function Get_Type_: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    { Methods & Properties }
    property Name: UnicodeString read Get_Name write Set_Name;
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
  end;

{ IXMLTSovietVillage_adrOut3 }

  IXMLTSovietVillage_adrOut3 = interface(IXMLNode)
    ['{84092E84-7A5E-4054-A736-2EF4BCE5B9BC}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    function Get_Type_: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    { Methods & Properties }
    property Name: UnicodeString read Get_Name write Set_Name;
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
  end;

{ IXMLTLevel1_adrOut3 }

  IXMLTLevel1_adrOut3 = interface(IXMLNode)
    ['{E00E9E26-CD23-4973-8D67-20385BEBE450}']
    { Property Accessors }
    function Get_Type_: UnicodeString;
    function Get_Value: UnicodeString;
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_Value(Value: UnicodeString);
    { Methods & Properties }
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
    property Value: UnicodeString read Get_Value write Set_Value;
  end;

{ IXMLTLevel2_adrOut3 }

  IXMLTLevel2_adrOut3 = interface(IXMLNode)
    ['{DAE17C24-8593-49C8-A65B-77D5D57C40D9}']
    { Property Accessors }
    function Get_Type_: UnicodeString;
    function Get_Value: UnicodeString;
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_Value(Value: UnicodeString);
    { Methods & Properties }
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
    property Value: UnicodeString read Get_Value write Set_Value;
  end;

{ IXMLTLevel3_adrOut3 }

  IXMLTLevel3_adrOut3 = interface(IXMLNode)
    ['{BCB17CCF-8FE7-421A-B9FF-716F8B792D26}']
    { Property Accessors }
    function Get_Type_: UnicodeString;
    function Get_Value: UnicodeString;
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_Value(Value: UnicodeString);
    { Methods & Properties }
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
    property Value: UnicodeString read Get_Value write Set_Value;
  end;

{ IXMLTApartment_adrOut3 }

  IXMLTApartment_adrOut3 = interface(IXMLNode)
    ['{F73B0D38-B78A-4371-84E7-CB6DFB498A20}']
    { Property Accessors }
    function Get_Type_: UnicodeString;
    function Get_Value: UnicodeString;
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_Value(Value: UnicodeString);
    { Methods & Properties }
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
    property Value: UnicodeString read Get_Value write Set_Value;
  end;

{ IXMLTUtilization }

  IXMLTUtilization = interface(IXMLNode)
    ['{F0815CD0-DC62-4310-A796-12C7D3AB6CA4}']
    { Property Accessors }
    function Get_Utilization: UnicodeString;
    function Get_ByDoc: UnicodeString;
    procedure Set_Utilization(Value: UnicodeString);
    procedure Set_ByDoc(Value: UnicodeString);
    { Methods & Properties }
    property Utilization: UnicodeString read Get_Utilization write Set_Utilization;
    property ByDoc: UnicodeString read Get_ByDoc write Set_ByDoc;
  end;

{ IXMLTNaturalObjects_NatObj1 }

  IXMLTNaturalObjects_NatObj1 = interface(IXMLNodeCollection)
    ['{88DF438D-4048-4EEC-A15C-A61FDE96E8BA}']
    { Property Accessors }
    function Get_NaturalObject(Index: Integer): IXMLTNaturalObject_NatObj1;
    { Methods & Properties }
    function Add: IXMLTNaturalObject_NatObj1;
    function Insert(const Index: Integer): IXMLTNaturalObject_NatObj1;
    property NaturalObject[Index: Integer]: IXMLTNaturalObject_NatObj1 read Get_NaturalObject; default;
  end;

{ IXMLTNaturalObject_NatObj1 }

  IXMLTNaturalObject_NatObj1 = interface(IXMLNode)
    ['{26593A63-F2D8-4FD8-BEC0-D5C5910A3D08}']
    { Property Accessors }
    function Get_Kind: UnicodeString;
    function Get_ForestUse: UnicodeString;
    function Get_ProtectiveForest: UnicodeString;
    function Get_WaterObject: UnicodeString;
    function Get_NameOther: UnicodeString;
    function Get_CharOther: UnicodeString;
    procedure Set_Kind(Value: UnicodeString);
    procedure Set_ForestUse(Value: UnicodeString);
    procedure Set_ProtectiveForest(Value: UnicodeString);
    procedure Set_WaterObject(Value: UnicodeString);
    procedure Set_NameOther(Value: UnicodeString);
    procedure Set_CharOther(Value: UnicodeString);
    { Methods & Properties }
    property Kind: UnicodeString read Get_Kind write Set_Kind;
    property ForestUse: UnicodeString read Get_ForestUse write Set_ForestUse;
    property ProtectiveForest: UnicodeString read Get_ProtectiveForest write Set_ProtectiveForest;
    property WaterObject: UnicodeString read Get_WaterObject write Set_WaterObject;
    property NameOther: UnicodeString read Get_NameOther write Set_NameOther;
    property CharOther: UnicodeString read Get_CharOther write Set_CharOther;
  end;

{ IXMLTParcel_Rights }

  IXMLTParcel_Rights = interface(IXMLNodeCollection)
    ['{42E56D17-672D-48B3-BEEA-5B9F36BE5666}']
    { Property Accessors }
    function Get_Right(Index: Integer): IXMLTRight;
    { Methods & Properties }
    function Add: IXMLTRight;
    function Insert(const Index: Integer): IXMLTRight;
    property Right[Index: Integer]: IXMLTRight read Get_Right; default;
  end;

{ IXMLTRight }

  IXMLTRight = interface(IXMLNode)
    ['{99EF3A05-5B2B-4E8F-85B7-4F5C271740AB}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    function Get_Type_: UnicodeString;
    function Get_Owners: IXMLTOwners;
    function Get_Share: IXMLTShare;
    function Get_ShareText: UnicodeString;
    function Get_Desc: UnicodeString;
    function Get_Registration: IXMLTRegistration;
    function Get_Documents: IXMLTRight_Documents;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_ShareText(Value: UnicodeString);
    procedure Set_Desc(Value: UnicodeString);
    { Methods & Properties }
    property Name: UnicodeString read Get_Name write Set_Name;
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
    property Owners: IXMLTOwners read Get_Owners;
    property Share: IXMLTShare read Get_Share;
    property ShareText: UnicodeString read Get_ShareText write Set_ShareText;
    property Desc: UnicodeString read Get_Desc write Set_Desc;
    property Registration: IXMLTRegistration read Get_Registration;
    property Documents: IXMLTRight_Documents read Get_Documents;
  end;

{ IXMLTOwners }

  IXMLTOwners = interface(IXMLNodeCollection)
    ['{FED455FE-3504-4541-83A2-95D66AF21A08}']
    { Property Accessors }
    function Get_Owner(Index: Integer): IXMLTOwner;
    { Methods & Properties }
    function Add: IXMLTOwner;
    function Insert(const Index: Integer): IXMLTOwner;
    property Owner[Index: Integer]: IXMLTOwner read Get_Owner; default;
  end;

{ IXMLTOwner }

  IXMLTOwner = interface(IXMLNode)
    ['{2BE773A0-ABFE-4F84-9DAE-79A9BFC24AEE}']
    { Property Accessors }
    function Get_Person: IXMLTOwner_Person;
    function Get_Organization: IXMLTNameOwner;
    function Get_Governance: IXMLTNameOwner;
    { Methods & Properties }
    property Person: IXMLTOwner_Person read Get_Person;
    property Organization: IXMLTNameOwner read Get_Organization;
    property Governance: IXMLTNameOwner read Get_Governance;
  end;

{ IXMLTOwner_Person }

  IXMLTOwner_Person = interface(IXMLNode)
    ['{554B62E3-D9E6-4EE7-96F5-063BE6FC0412}']
    { Property Accessors }
    function Get_FamilyName: UnicodeString;
    function Get_FirstName: UnicodeString;
    function Get_Patronymic: UnicodeString;
    procedure Set_FamilyName(Value: UnicodeString);
    procedure Set_FirstName(Value: UnicodeString);
    procedure Set_Patronymic(Value: UnicodeString);
    { Methods & Properties }
    property FamilyName: UnicodeString read Get_FamilyName write Set_FamilyName;
    property FirstName: UnicodeString read Get_FirstName write Set_FirstName;
    property Patronymic: UnicodeString read Get_Patronymic write Set_Patronymic;
  end;

{ IXMLTNameOwner }

  IXMLTNameOwner = interface(IXMLNode)
    ['{86585B32-ECB3-4BE2-9895-8AF6008DF566}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    { Methods & Properties }
    property Name: UnicodeString read Get_Name write Set_Name;
  end;

{ IXMLTShare }

  IXMLTShare = interface(IXMLNode)
    ['{C8D9089D-CE0C-45EF-AE4C-2037CF5BB80A}']
    { Property Accessors }
    function Get_Numerator: Integer;
    function Get_Denominator: Integer;
    procedure Set_Numerator(Value: Integer);
    procedure Set_Denominator(Value: Integer);
    { Methods & Properties }
    property Numerator: Integer read Get_Numerator write Set_Numerator;
    property Denominator: Integer read Get_Denominator write Set_Denominator;
  end;

{ IXMLTRegistration }

  IXMLTRegistration = interface(IXMLNode)
    ['{D8FDFBB6-EF7A-401A-9DA6-8E6F3A826CD9}']
    { Property Accessors }
    function Get_RegNumber: UnicodeString;
    function Get_RegDate: UnicodeString;
    procedure Set_RegNumber(Value: UnicodeString);
    procedure Set_RegDate(Value: UnicodeString);
    { Methods & Properties }
    property RegNumber: UnicodeString read Get_RegNumber write Set_RegNumber;
    property RegDate: UnicodeString read Get_RegDate write Set_RegDate;
  end;

{ IXMLTRight_Documents }

  IXMLTRight_Documents = interface(IXMLNodeCollection)
    ['{5679BBE1-3414-4D1A-AFF3-4B2D52979135}']
    { Property Accessors }
    function Get_Document(Index: Integer): IXMLTDocumentWithoutAppliedFile_DocOut3;
    { Methods & Properties }
    function Add: IXMLTDocumentWithoutAppliedFile_DocOut3;
    function Insert(const Index: Integer): IXMLTDocumentWithoutAppliedFile_DocOut3;
    property Document[Index: Integer]: IXMLTDocumentWithoutAppliedFile_DocOut3 read Get_Document; default;
  end;

{ IXMLTDocumentWithoutAppliedFile_DocOut3 }

  IXMLTDocumentWithoutAppliedFile_DocOut3 = interface(IXMLNode)
    ['{3F05ADB1-7E0B-448F-AFDD-AC79F85172E1}']
    { Property Accessors }
    function Get_CodeDocument: UnicodeString;
    function Get_Name: UnicodeString;
    function Get_Series: UnicodeString;
    function Get_Number: UnicodeString;
    function Get_Date: UnicodeString;
    function Get_IssueOrgan: UnicodeString;
    function Get_Desc: UnicodeString;
    procedure Set_CodeDocument(Value: UnicodeString);
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Series(Value: UnicodeString);
    procedure Set_Number(Value: UnicodeString);
    procedure Set_Date(Value: UnicodeString);
    procedure Set_IssueOrgan(Value: UnicodeString);
    procedure Set_Desc(Value: UnicodeString);
    { Methods & Properties }
    property CodeDocument: UnicodeString read Get_CodeDocument write Set_CodeDocument;
    property Name: UnicodeString read Get_Name write Set_Name;
    property Series: UnicodeString read Get_Series write Set_Series;
    property Number: UnicodeString read Get_Number write Set_Number;
    property Date: UnicodeString read Get_Date write Set_Date;
    property IssueOrgan: UnicodeString read Get_IssueOrgan write Set_IssueOrgan;
    property Desc: UnicodeString read Get_Desc write Set_Desc;
  end;

{ IXMLTParcel_SubParcels }

  IXMLTParcel_SubParcels = interface(IXMLNodeCollection)
    ['{3570A3C5-CB56-49FD-B433-8A6A26E19A63}']
    { Property Accessors }
    function Get_SubParcel(Index: Integer): IXMLTSubParcel;
    { Methods & Properties }
    function Add: IXMLTSubParcel;
    function Insert(const Index: Integer): IXMLTSubParcel;
    property SubParcel[Index: Integer]: IXMLTSubParcel read Get_SubParcel; default;
  end;

{ IXMLTSubParcel }

  IXMLTSubParcel = interface(IXMLNode)
    ['{2B79727C-B8BC-4C3B-A714-97E4CF934E68}']
    { Property Accessors }
    function Get_NumberRecord: UnicodeString;
    function Get_Full: Boolean;
    function Get_State: UnicodeString;
    function Get_DateExpiry: UnicodeString;
    function Get_Area: IXMLTAreaWithoutInaccuracyOut;
    function Get_Encumbrance: IXMLTEncumbranceZU;
    function Get_EntitySpatial: IXMLTEntitySpatialZUOut_Spa2;
    procedure Set_NumberRecord(Value: UnicodeString);
    procedure Set_Full(Value: Boolean);
    procedure Set_State(Value: UnicodeString);
    procedure Set_DateExpiry(Value: UnicodeString);
    { Methods & Properties }
    property NumberRecord: UnicodeString read Get_NumberRecord write Set_NumberRecord;
    property Full: Boolean read Get_Full write Set_Full;
    property State: UnicodeString read Get_State write Set_State;
    property DateExpiry: UnicodeString read Get_DateExpiry write Set_DateExpiry;
    property Area: IXMLTAreaWithoutInaccuracyOut read Get_Area;
    property Encumbrance: IXMLTEncumbranceZU read Get_Encumbrance;
    property EntitySpatial: IXMLTEntitySpatialZUOut_Spa2 read Get_EntitySpatial;
  end;

{ IXMLTAreaWithoutInaccuracyOut }

  IXMLTAreaWithoutInaccuracyOut = interface(IXMLNode)
    ['{ABA9DF03-751C-43B6-A14C-078796EABF90}']
    { Property Accessors }
    function Get_Area: UnicodeString;
    function Get_Unit_: UnicodeString;
    procedure Set_Area(Value: UnicodeString);
    procedure Set_Unit_(Value: UnicodeString);
    { Methods & Properties }
    property Area: UnicodeString read Get_Area write Set_Area;
    property Unit_: UnicodeString read Get_Unit_ write Set_Unit_;
  end;

{ IXMLTEncumbranceZU }

  IXMLTEncumbranceZU = interface(IXMLNode)
    ['{C5102A94-DDF3-44B4-8D13-CD6077A6739A}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    function Get_Type_: UnicodeString;
    function Get_AccountNumber: UnicodeString;
    function Get_CadastralNumberRestriction: UnicodeString;
    function Get_OwnersRestrictionInFavorem: IXMLTOwnerRestrictionInFavorem;
    function Get_Duration: IXMLTDuration;
    function Get_Registration: IXMLTRegistration;
    function Get_Document: IXMLTDocumentWithoutAppliedFile_DocOut3;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_AccountNumber(Value: UnicodeString);
    procedure Set_CadastralNumberRestriction(Value: UnicodeString);
    { Methods & Properties }
    property Name: UnicodeString read Get_Name write Set_Name;
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
    property AccountNumber: UnicodeString read Get_AccountNumber write Set_AccountNumber;
    property CadastralNumberRestriction: UnicodeString read Get_CadastralNumberRestriction write Set_CadastralNumberRestriction;
    property OwnersRestrictionInFavorem: IXMLTOwnerRestrictionInFavorem read Get_OwnersRestrictionInFavorem;
    property Duration: IXMLTDuration read Get_Duration;
    property Registration: IXMLTRegistration read Get_Registration;
    property Document: IXMLTDocumentWithoutAppliedFile_DocOut3 read Get_Document;
  end;

{ IXMLTOwnerRestrictionInFavorem }

  IXMLTOwnerRestrictionInFavorem = interface(IXMLNodeCollection)
    ['{704067A6-E0BB-4C81-984B-4143712F1CB4}']
    { Property Accessors }
    function Get_OwnerRestrictionInFavorem(Index: Integer): IXMLTOwner;
    { Methods & Properties }
    function Add: IXMLTOwner;
    function Insert(const Index: Integer): IXMLTOwner;
    property OwnerRestrictionInFavorem[Index: Integer]: IXMLTOwner read Get_OwnerRestrictionInFavorem; default;
  end;

{ IXMLTDuration }

  IXMLTDuration = interface(IXMLNode)
    ['{247FA905-04BF-4624-9707-13F870BE7648}']
    { Property Accessors }
    function Get_Started: UnicodeString;
    function Get_Stopped: UnicodeString;
    function Get_Term: UnicodeString;
    procedure Set_Started(Value: UnicodeString);
    procedure Set_Stopped(Value: UnicodeString);
    procedure Set_Term(Value: UnicodeString);
    { Methods & Properties }
    property Started: UnicodeString read Get_Started write Set_Started;
    property Stopped: UnicodeString read Get_Stopped write Set_Stopped;
    property Term: UnicodeString read Get_Term write Set_Term;
  end;

{ IXMLTEntitySpatialZUOut_Spa2 }

  IXMLTEntitySpatialZUOut_Spa2 = interface(IXMLNodeCollection)
    ['{E95A6494-A4EA-427B-974D-52BF0EC6A4E1}']
    { Property Accessors }
    function Get_EntSys: UnicodeString;
    function Get_SpatialElement(Index: Integer): IXMLTSpatialElementZUOut_Spa2;
    procedure Set_EntSys(Value: UnicodeString);
    { Methods & Properties }
    function Add: IXMLTSpatialElementZUOut_Spa2;
    function Insert(const Index: Integer): IXMLTSpatialElementZUOut_Spa2;
    property EntSys: UnicodeString read Get_EntSys write Set_EntSys;
    property SpatialElement[Index: Integer]: IXMLTSpatialElementZUOut_Spa2 read Get_SpatialElement; default;
  end;

{ IXMLTSpatialElementZUOut_Spa2 }

  IXMLTSpatialElementZUOut_Spa2 = interface(IXMLNodeCollection)
    ['{8FB5B2E5-624F-474D-9F51-FDC642CA5243}']
    { Property Accessors }
    function Get_SpelementUnit(Index: Integer): IXMLTSpelementUnitZUOut_Spa2;
    { Methods & Properties }
    function Add: IXMLTSpelementUnitZUOut_Spa2;
    function Insert(const Index: Integer): IXMLTSpelementUnitZUOut_Spa2;
    property SpelementUnit[Index: Integer]: IXMLTSpelementUnitZUOut_Spa2 read Get_SpelementUnit; default;
  end;

{ IXMLTSpelementUnitZUOut_Spa2 }

  IXMLTSpelementUnitZUOut_Spa2 = interface(IXMLNode)
    ['{827EAC1C-93CE-4F2A-A826-0942692CBCD3}']
    { Property Accessors }
    function Get_TypeUnit: UnicodeString;
    function Get_SuNmb: LongWord;
    function Get_Ordinate: IXMLOrdinate_Spa2;
    procedure Set_TypeUnit(Value: UnicodeString);
    procedure Set_SuNmb(Value: LongWord);
    { Methods & Properties }
    property TypeUnit: UnicodeString read Get_TypeUnit write Set_TypeUnit;
    property SuNmb: LongWord read Get_SuNmb write Set_SuNmb;
    property Ordinate: IXMLOrdinate_Spa2 read Get_Ordinate;
  end;

{ IXMLTOrdinateOut_Spa2 }

  IXMLTOrdinateOut_Spa2 = interface(IXMLNode)
    ['{307D8B56-6FF9-45E2-BBEF-0D7219EF631B}']
    { Property Accessors }
    function Get_X: UnicodeString;
    function Get_Y: UnicodeString;
    function Get_OrdNmb: LongWord;
    function Get_NumGeopoint: LongWord;
    function Get_DeltaGeopoint: UnicodeString;
    procedure Set_X(Value: UnicodeString);
    procedure Set_Y(Value: UnicodeString);
    procedure Set_OrdNmb(Value: LongWord);
    procedure Set_NumGeopoint(Value: LongWord);
    procedure Set_DeltaGeopoint(Value: UnicodeString);
    { Methods & Properties }
    property X: UnicodeString read Get_X write Set_X;
    property Y: UnicodeString read Get_Y write Set_Y;
    property OrdNmb: LongWord read Get_OrdNmb write Set_OrdNmb;
    property NumGeopoint: LongWord read Get_NumGeopoint write Set_NumGeopoint;
    property DeltaGeopoint: UnicodeString read Get_DeltaGeopoint write Set_DeltaGeopoint;
  end;

{ IXMLOrdinate_Spa2 }

  IXMLOrdinate_Spa2 = interface(IXMLTOrdinateOut_Spa2)
    ['{CB33F4E0-1EF0-462F-9FF3-917F8C29C12E}']
    { Property Accessors }
    function Get_GeopointZacrep: UnicodeString;
    procedure Set_GeopointZacrep(Value: UnicodeString);
    { Methods & Properties }
    property GeopointZacrep: UnicodeString read Get_GeopointZacrep write Set_GeopointZacrep;
  end;

{ IXMLTParcel_Contours }

  IXMLTParcel_Contours = interface(IXMLNodeCollection)
    ['{09B6EC1F-46A1-462F-B2D2-9BACD2BCFA68}']
    { Property Accessors }
    function Get_Contour(Index: Integer): IXMLTContour;
    { Methods & Properties }
    function Add: IXMLTContour;
    function Insert(const Index: Integer): IXMLTContour;
    property Contour[Index: Integer]: IXMLTContour read Get_Contour; default;
  end;

{ IXMLTContour }

  IXMLTContour = interface(IXMLNode)
    ['{A613F090-A106-4F1F-BBF3-AE3B60B3016C}']
    { Property Accessors }
    function Get_NumberRecord: LongWord;
    function Get_Area: IXMLTAreaWithoutInaccuracyOut;
    function Get_EntitySpatial: IXMLTEntitySpatialZUOut_Spa2;
    procedure Set_NumberRecord(Value: LongWord);
    { Methods & Properties }
    property NumberRecord: LongWord read Get_NumberRecord write Set_NumberRecord;
    property Area: IXMLTAreaWithoutInaccuracyOut read Get_Area;
    property EntitySpatial: IXMLTEntitySpatialZUOut_Spa2 read Get_EntitySpatial;
  end;

{ IXMLTParcel_CompositionEZ }

  IXMLTParcel_CompositionEZ = interface(IXMLNodeCollection)
    ['{61EE25D5-9473-414F-B7DD-3A86E10CCCBD}']
    { Property Accessors }
    function Get_EntryParcel(Index: Integer): IXMLTEntryParcel;
    { Methods & Properties }
    function Add: IXMLTEntryParcel;
    function Insert(const Index: Integer): IXMLTEntryParcel;
    property EntryParcel[Index: Integer]: IXMLTEntryParcel read Get_EntryParcel; default;
  end;

{ IXMLTEntryParcel }

  IXMLTEntryParcel = interface(IXMLNode)
    ['{0DABC433-D493-4AE1-B197-5EA0212F1242}']
    { Property Accessors }
    function Get_CadastralNumber: UnicodeString;
    function Get_Area: IXMLTAreaWithoutInaccuracyOut;
    function Get_EntitySpatial: IXMLTEntitySpatialZUOut_Spa2;
    procedure Set_CadastralNumber(Value: UnicodeString);
    { Methods & Properties }
    property CadastralNumber: UnicodeString read Get_CadastralNumber write Set_CadastralNumber;
    property Area: IXMLTAreaWithoutInaccuracyOut read Get_Area;
    property EntitySpatial: IXMLTEntitySpatialZUOut_Spa2 read Get_EntitySpatial;
  end;

{ IXMLTParcel_Encumbrances }

  IXMLTParcel_Encumbrances = interface(IXMLNodeCollection)
    ['{56C6AA66-75B7-4105-9BFA-DE75B1357B28}']
    { Property Accessors }
    function Get_Encumbrance(Index: Integer): IXMLTEncumbranceZU;
    { Methods & Properties }
    function Add: IXMLTEncumbranceZU;
    function Insert(const Index: Integer): IXMLTEncumbranceZU;
    property Encumbrance[Index: Integer]: IXMLTEncumbranceZU read Get_Encumbrance; default;
  end;

{ IXMLTCadastralCost_Cos1 }

  IXMLTCadastralCost_Cos1 = interface(IXMLNode)
    ['{9BDD8710-28A1-488F-8C78-02117C5F51D0}']
    { Property Accessors }
    function Get_Value: UnicodeString;
    function Get_Unit_: UnicodeString;
    procedure Set_Value(Value: UnicodeString);
    procedure Set_Unit_(Value: UnicodeString);
    { Methods & Properties }
    property Value: UnicodeString read Get_Value write Set_Value;
    property Unit_: UnicodeString read Get_Unit_ write Set_Unit_;
  end;

{ IXMLTCadastralNumberOut }

  IXMLTCadastralNumberOut = interface(IXMLNode)
    ['{30D7E6D0-E407-4055-A77B-33A269EB3725}']
    { Property Accessors }
    function Get_CadastralNumber: UnicodeString;
    procedure Set_CadastralNumber(Value: UnicodeString);
    { Methods & Properties }
    property CadastralNumber: UnicodeString read Get_CadastralNumber write Set_CadastralNumber;
  end;

{ IXMLTCertificationDoc_Cer1 }

  IXMLTCertificationDoc_Cer1 = interface(IXMLNode)
    ['{F392446B-B816-4DC8-9CB0-E28AE55E1A86}']
    { Property Accessors }
    function Get_Organization: UnicodeString;
    function Get_Date: UnicodeString;
    function Get_Number: UnicodeString;
    function Get_Official: IXMLOfficial_Cer1;
    procedure Set_Organization(Value: UnicodeString);
    procedure Set_Date(Value: UnicodeString);
    procedure Set_Number(Value: UnicodeString);
    { Methods & Properties }
    property Organization: UnicodeString read Get_Organization write Set_Organization;
    property Date: UnicodeString read Get_Date write Set_Date;
    property Number: UnicodeString read Get_Number write Set_Number;
    property Official: IXMLOfficial_Cer1 read Get_Official;
  end;

{ IXMLOfficial_Cer1 }

  IXMLOfficial_Cer1 = interface(IXMLNode)
    ['{98B8C45F-F6E5-4EF2-9280-28EC9DB3E7D6}']
    { Property Accessors }
    function Get_Appointment: UnicodeString;
    function Get_FamilyName: UnicodeString;
    function Get_FirstName: UnicodeString;
    function Get_Patronymic: UnicodeString;
    procedure Set_Appointment(Value: UnicodeString);
    procedure Set_FamilyName(Value: UnicodeString);
    procedure Set_FirstName(Value: UnicodeString);
    procedure Set_Patronymic(Value: UnicodeString);
    { Methods & Properties }
    property Appointment: UnicodeString read Get_Appointment write Set_Appointment;
    property FamilyName: UnicodeString read Get_FamilyName write Set_FamilyName;
    property FirstName: UnicodeString read Get_FirstName write Set_FirstName;
    property Patronymic: UnicodeString read Get_Patronymic write Set_Patronymic;
  end;

{ IXMLKPZU_Contractors }

  IXMLKPZU_Contractors = interface(IXMLNodeCollection)
    ['{77691929-01BF-4AFB-967C-B7DAABDB2524}']
    { Property Accessors }
    function Get_Contractor(Index: Integer): IXMLKPZU_Contractors_Contractor;
    { Methods & Properties }
    function Add: IXMLKPZU_Contractors_Contractor;
    function Insert(const Index: Integer): IXMLKPZU_Contractors_Contractor;
    property Contractor[Index: Integer]: IXMLKPZU_Contractors_Contractor read Get_Contractor; default;
  end;

{ IXMLTEngineerOut }

  IXMLTEngineerOut = interface(IXMLNode)
    ['{CE4B5038-51DD-4EEA-B98D-9267B98E3851}']
    { Property Accessors }
    function Get_FamilyName: UnicodeString;
    function Get_FirstName: UnicodeString;
    function Get_Patronymic: UnicodeString;
    function Get_NCertificate: UnicodeString;
    function Get_Organization: IXMLTOrganizationNameOut;
    procedure Set_FamilyName(Value: UnicodeString);
    procedure Set_FirstName(Value: UnicodeString);
    procedure Set_Patronymic(Value: UnicodeString);
    procedure Set_NCertificate(Value: UnicodeString);
    { Methods & Properties }
    property FamilyName: UnicodeString read Get_FamilyName write Set_FamilyName;
    property FirstName: UnicodeString read Get_FirstName write Set_FirstName;
    property Patronymic: UnicodeString read Get_Patronymic write Set_Patronymic;
    property NCertificate: UnicodeString read Get_NCertificate write Set_NCertificate;
    property Organization: IXMLTOrganizationNameOut read Get_Organization;
  end;

{ IXMLTOrganizationNameOut }

  IXMLTOrganizationNameOut = interface(IXMLNode)
    ['{666B3A32-E987-437E-8235-3264A815CF16}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    { Methods & Properties }
    property Name: UnicodeString read Get_Name write Set_Name;
  end;

{ IXMLKPZU_Contractors_Contractor }

  IXMLKPZU_Contractors_Contractor = interface(IXMLTEngineerOut)
    ['{13005545-8374-4F2A-A273-DDB2D6D2AF66}']
    { Property Accessors }
    function Get_Date: UnicodeString;
    procedure Set_Date(Value: UnicodeString);
    { Methods & Properties }
    property Date: UnicodeString read Get_Date write Set_Date;
  end;

{ IXMLTCoordSystems_Spa2 }

  IXMLTCoordSystems_Spa2 = interface(IXMLNodeCollection)
    ['{58EADEE6-49FB-4377-9D20-E81BA8267AB8}']
    { Property Accessors }
    function Get_CoordSystem(Index: Integer): IXMLTCoordSystem_Spa2;
    { Methods & Properties }
    function Add: IXMLTCoordSystem_Spa2;
    function Insert(const Index: Integer): IXMLTCoordSystem_Spa2;
    property CoordSystem[Index: Integer]: IXMLTCoordSystem_Spa2 read Get_CoordSystem; default;
  end;

{ IXMLTCoordSystem_Spa2 }

  IXMLTCoordSystem_Spa2 = interface(IXMLNode)
    ['{01C8E08F-6007-4D5C-A19F-2246E9AA6351}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    function Get_CsId: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_CsId(Value: UnicodeString);
    { Methods & Properties }
    property Name: UnicodeString read Get_Name write Set_Name;
    property CsId: UnicodeString read Get_CsId write Set_CsId;
  end;

{ Forward Decls }

  TXMLKPZU = class;
  TXMLTParcel = class;
  TXMLTOldNumbers_Num1 = class;
  TXMLTOldNumber_Num1 = class;
  TXMLTCadastralNumbersOut = class;
  TXMLTAreaOut = class;
  TXMLTLocation = class;
  TXMLTElaborationLocation = class;
  TXMLTAddressOut_adrOut3 = class;
  TXMLTName_adrOut3 = class;
  TXMLTUrbanDistrict_adrOut3 = class;
  TXMLTSovietVillage_adrOut3 = class;
  TXMLTLevel1_adrOut3 = class;
  TXMLTLevel2_adrOut3 = class;
  TXMLTLevel3_adrOut3 = class;
  TXMLTApartment_adrOut3 = class;
  TXMLTUtilization = class;
  TXMLTNaturalObjects_NatObj1 = class;
  TXMLTNaturalObject_NatObj1 = class;
  TXMLTParcel_Rights = class;
  TXMLTRight = class;
  TXMLTOwners = class;
  TXMLTOwner = class;
  TXMLTOwner_Person = class;
  TXMLTNameOwner = class;
  TXMLTShare = class;
  TXMLTRegistration = class;
  TXMLTRight_Documents = class;
  TXMLTDocumentWithoutAppliedFile_DocOut3 = class;
  TXMLTParcel_SubParcels = class;
  TXMLTSubParcel = class;
  TXMLTAreaWithoutInaccuracyOut = class;
  TXMLTEncumbranceZU = class;
  TXMLTOwnerRestrictionInFavorem = class;
  TXMLTDuration = class;
  TXMLTEntitySpatialZUOut_Spa2 = class;
  TXMLTSpatialElementZUOut_Spa2 = class;
  TXMLTSpelementUnitZUOut_Spa2 = class;
  TXMLTOrdinateOut_Spa2 = class;
  TXMLOrdinate_Spa2 = class;
  TXMLTParcel_Contours = class;
  TXMLTContour = class;
  TXMLTParcel_CompositionEZ = class;
  TXMLTEntryParcel = class;
  TXMLTParcel_Encumbrances = class;
  TXMLTCadastralCost_Cos1 = class;
  TXMLTCadastralNumberOut = class;
  TXMLTCertificationDoc_Cer1 = class;
  TXMLOfficial_Cer1 = class;
  TXMLKPZU_Contractors = class;
  TXMLTEngineerOut = class;
  TXMLTOrganizationNameOut = class;
  TXMLKPZU_Contractors_Contractor = class;
  TXMLTCoordSystems_Spa2 = class;
  TXMLTCoordSystem_Spa2 = class;

{ TXMLKPZU }

  TXMLKPZU = class(TXMLNode, IXMLKPZU)
  protected
    { IXMLKPZU }
    function Get_Parcel: IXMLTParcel;
    function Get_CertificationDoc: IXMLTCertificationDoc_Cer1;
    function Get_Contractors: IXMLKPZU_Contractors;
    function Get_CoordSystems: IXMLTCoordSystems_Spa2;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTParcel }

  TXMLTParcel = class(TXMLNode, IXMLTParcel)
  protected
    { IXMLTParcel }
    function Get_CadastralNumber: UnicodeString;
    function Get_State: UnicodeString;
    function Get_DateExpiry: UnicodeString;
    function Get_DateCreated: UnicodeString;
    function Get_DateRemoved: UnicodeString;
    function Get_Method: UnicodeString;
    function Get_DateCreatedDoc: UnicodeString;
    function Get_CadastralBlock: UnicodeString;
    function Get_Name: UnicodeString;
    function Get_OldNumbers: IXMLTOldNumbers_Num1;
    function Get_PrevCadastralNumbers: IXMLTCadastralNumbersOut;
    function Get_InnerCadastralNumbers: IXMLTCadastralNumbersOut;
    function Get_Area: IXMLTAreaOut;
    function Get_Location: IXMLTLocation;
    function Get_Category: UnicodeString;
    function Get_Utilization: IXMLTUtilization;
    function Get_NaturalObjects: IXMLTNaturalObjects_NatObj1;
    function Get_Rights: IXMLTParcel_Rights;
    function Get_SubParcels: IXMLTParcel_SubParcels;
    function Get_EntitySpatial: IXMLTEntitySpatialZUOut_Spa2;
    function Get_Contours: IXMLTParcel_Contours;
    function Get_CompositionEZ: IXMLTParcel_CompositionEZ;
    function Get_Encumbrances: IXMLTParcel_Encumbrances;
    function Get_CadastralCost: IXMLTCadastralCost_Cos1;
    function Get_SpecialNote: UnicodeString;
    function Get_AllNewParcels: IXMLTCadastralNumbersOut;
    function Get_ChangeParcel: IXMLTCadastralNumberOut;
    function Get_RemoveParcels: IXMLTCadastralNumbersOut;
    function Get_AllOffspringParcel: IXMLTCadastralNumbersOut;
    procedure Set_CadastralNumber(Value: UnicodeString);
    procedure Set_State(Value: UnicodeString);
    procedure Set_DateExpiry(Value: UnicodeString);
    procedure Set_DateCreated(Value: UnicodeString);
    procedure Set_DateRemoved(Value: UnicodeString);
    procedure Set_Method(Value: UnicodeString);
    procedure Set_DateCreatedDoc(Value: UnicodeString);
    procedure Set_CadastralBlock(Value: UnicodeString);
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Category(Value: UnicodeString);
    procedure Set_SpecialNote(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTOldNumbers_Num1 }

  TXMLTOldNumbers_Num1 = class(TXMLNodeCollection, IXMLTOldNumbers_Num1)
  protected
    { IXMLTOldNumbers_Num1 }
    function Get_OldNumber(Index: Integer): IXMLTOldNumber_Num1;
    function Add: IXMLTOldNumber_Num1;
    function Insert(const Index: Integer): IXMLTOldNumber_Num1;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTOldNumber_Num1 }

  TXMLTOldNumber_Num1 = class(TXMLNode, IXMLTOldNumber_Num1)
  protected
    { IXMLTOldNumber_Num1 }
    function Get_Type_: UnicodeString;
    function Get_Number: UnicodeString;
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_Number(Value: UnicodeString);
  end;

{ TXMLTCadastralNumbersOut }

  TXMLTCadastralNumbersOut = class(TXMLNodeCollection, IXMLTCadastralNumbersOut)
  protected
    { IXMLTCadastralNumbersOut }
    function Get_CadastralNumber(Index: Integer): UnicodeString;
    function Add(const CadastralNumber: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const CadastralNumber: UnicodeString): IXMLNode;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTAreaOut }

  TXMLTAreaOut = class(TXMLNode, IXMLTAreaOut)
  protected
    { IXMLTAreaOut }
    function Get_Area: LongWord;
    function Get_Unit_: UnicodeString;
    function Get_Inaccuracy: UnicodeString;
    procedure Set_Area(Value: LongWord);
    procedure Set_Unit_(Value: UnicodeString);
    procedure Set_Inaccuracy(Value: UnicodeString);
  end;

{ TXMLTLocation }

  TXMLTLocation = class(TXMLNode, IXMLTLocation)
  protected
    { IXMLTLocation }
    function Get_InBounds: UnicodeString;
    function Get_Placed: UnicodeString;
    function Get_Elaboration: IXMLTElaborationLocation;
    function Get_Address: IXMLTAddressOut_adrOut3;
    procedure Set_InBounds(Value: UnicodeString);
    procedure Set_Placed(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTElaborationLocation }

  TXMLTElaborationLocation = class(TXMLNode, IXMLTElaborationLocation)
  protected
    { IXMLTElaborationLocation }
    function Get_ReferenceMark: UnicodeString;
    function Get_Distance: UnicodeString;
    function Get_Direction: UnicodeString;
    procedure Set_ReferenceMark(Value: UnicodeString);
    procedure Set_Distance(Value: UnicodeString);
    procedure Set_Direction(Value: UnicodeString);
  end;

{ TXMLTAddressOut_adrOut3 }

  TXMLTAddressOut_adrOut3 = class(TXMLNode, IXMLTAddressOut_adrOut3)
  protected
    { IXMLTAddressOut_adrOut3 }
    function Get_OKATO: UnicodeString;
    function Get_KLADR: UnicodeString;
    function Get_OKTMO: UnicodeString;
    function Get_PostalCode: UnicodeString;
    function Get_Region: UnicodeString;
    function Get_District: IXMLTName_adrOut3;
    function Get_City: IXMLTName_adrOut3;
    function Get_UrbanDistrict: IXMLTUrbanDistrict_adrOut3;
    function Get_SovietVillage: IXMLTSovietVillage_adrOut3;
    function Get_Locality: IXMLTName_adrOut3;
    function Get_Street: IXMLTName_adrOut3;
    function Get_Level1: IXMLTLevel1_adrOut3;
    function Get_Level2: IXMLTLevel2_adrOut3;
    function Get_Level3: IXMLTLevel3_adrOut3;
    function Get_Apartment: IXMLTApartment_adrOut3;
    function Get_Other: UnicodeString;
    function Get_Note: UnicodeString;
    procedure Set_OKATO(Value: UnicodeString);
    procedure Set_KLADR(Value: UnicodeString);
    procedure Set_OKTMO(Value: UnicodeString);
    procedure Set_PostalCode(Value: UnicodeString);
    procedure Set_Region(Value: UnicodeString);
    procedure Set_Other(Value: UnicodeString);
    procedure Set_Note(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTName_adrOut3 }

  TXMLTName_adrOut3 = class(TXMLNode, IXMLTName_adrOut3)
  protected
    { IXMLTName_adrOut3 }
    function Get_Name: UnicodeString;
    function Get_Type_: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
  end;

{ TXMLTUrbanDistrict_adrOut3 }

  TXMLTUrbanDistrict_adrOut3 = class(TXMLNode, IXMLTUrbanDistrict_adrOut3)
  protected
    { IXMLTUrbanDistrict_adrOut3 }
    function Get_Name: UnicodeString;
    function Get_Type_: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
  end;

{ TXMLTSovietVillage_adrOut3 }

  TXMLTSovietVillage_adrOut3 = class(TXMLNode, IXMLTSovietVillage_adrOut3)
  protected
    { IXMLTSovietVillage_adrOut3 }
    function Get_Name: UnicodeString;
    function Get_Type_: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
  end;

{ TXMLTLevel1_adrOut3 }

  TXMLTLevel1_adrOut3 = class(TXMLNode, IXMLTLevel1_adrOut3)
  protected
    { IXMLTLevel1_adrOut3 }
    function Get_Type_: UnicodeString;
    function Get_Value: UnicodeString;
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_Value(Value: UnicodeString);
  end;

{ TXMLTLevel2_adrOut3 }

  TXMLTLevel2_adrOut3 = class(TXMLNode, IXMLTLevel2_adrOut3)
  protected
    { IXMLTLevel2_adrOut3 }
    function Get_Type_: UnicodeString;
    function Get_Value: UnicodeString;
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_Value(Value: UnicodeString);
  end;

{ TXMLTLevel3_adrOut3 }

  TXMLTLevel3_adrOut3 = class(TXMLNode, IXMLTLevel3_adrOut3)
  protected
    { IXMLTLevel3_adrOut3 }
    function Get_Type_: UnicodeString;
    function Get_Value: UnicodeString;
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_Value(Value: UnicodeString);
  end;

{ TXMLTApartment_adrOut3 }

  TXMLTApartment_adrOut3 = class(TXMLNode, IXMLTApartment_adrOut3)
  protected
    { IXMLTApartment_adrOut3 }
    function Get_Type_: UnicodeString;
    function Get_Value: UnicodeString;
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_Value(Value: UnicodeString);
  end;

{ TXMLTUtilization }

  TXMLTUtilization = class(TXMLNode, IXMLTUtilization)
  protected
    { IXMLTUtilization }
    function Get_Utilization: UnicodeString;
    function Get_ByDoc: UnicodeString;
    procedure Set_Utilization(Value: UnicodeString);
    procedure Set_ByDoc(Value: UnicodeString);
  end;

{ TXMLTNaturalObjects_NatObj1 }

  TXMLTNaturalObjects_NatObj1 = class(TXMLNodeCollection, IXMLTNaturalObjects_NatObj1)
  protected
    { IXMLTNaturalObjects_NatObj1 }
    function Get_NaturalObject(Index: Integer): IXMLTNaturalObject_NatObj1;
    function Add: IXMLTNaturalObject_NatObj1;
    function Insert(const Index: Integer): IXMLTNaturalObject_NatObj1;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTNaturalObject_NatObj1 }

  TXMLTNaturalObject_NatObj1 = class(TXMLNode, IXMLTNaturalObject_NatObj1)
  protected
    { IXMLTNaturalObject_NatObj1 }
    function Get_Kind: UnicodeString;
    function Get_ForestUse: UnicodeString;
    function Get_ProtectiveForest: UnicodeString;
    function Get_WaterObject: UnicodeString;
    function Get_NameOther: UnicodeString;
    function Get_CharOther: UnicodeString;
    procedure Set_Kind(Value: UnicodeString);
    procedure Set_ForestUse(Value: UnicodeString);
    procedure Set_ProtectiveForest(Value: UnicodeString);
    procedure Set_WaterObject(Value: UnicodeString);
    procedure Set_NameOther(Value: UnicodeString);
    procedure Set_CharOther(Value: UnicodeString);
  end;

{ TXMLTParcel_Rights }

  TXMLTParcel_Rights = class(TXMLNodeCollection, IXMLTParcel_Rights)
  protected
    { IXMLTParcel_Rights }
    function Get_Right(Index: Integer): IXMLTRight;
    function Add: IXMLTRight;
    function Insert(const Index: Integer): IXMLTRight;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTRight }

  TXMLTRight = class(TXMLNode, IXMLTRight)
  protected
    { IXMLTRight }
    function Get_Name: UnicodeString;
    function Get_Type_: UnicodeString;
    function Get_Owners: IXMLTOwners;
    function Get_Share: IXMLTShare;
    function Get_ShareText: UnicodeString;
    function Get_Desc: UnicodeString;
    function Get_Registration: IXMLTRegistration;
    function Get_Documents: IXMLTRight_Documents;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_ShareText(Value: UnicodeString);
    procedure Set_Desc(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTOwners }

  TXMLTOwners = class(TXMLNodeCollection, IXMLTOwners)
  protected
    { IXMLTOwners }
    function Get_Owner(Index: Integer): IXMLTOwner;
    function Add: IXMLTOwner;
    function Insert(const Index: Integer): IXMLTOwner;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTOwner }

  TXMLTOwner = class(TXMLNode, IXMLTOwner)
  protected
    { IXMLTOwner }
    function Get_Person: IXMLTOwner_Person;
    function Get_Organization: IXMLTNameOwner;
    function Get_Governance: IXMLTNameOwner;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTOwner_Person }

  TXMLTOwner_Person = class(TXMLNode, IXMLTOwner_Person)
  protected
    { IXMLTOwner_Person }
    function Get_FamilyName: UnicodeString;
    function Get_FirstName: UnicodeString;
    function Get_Patronymic: UnicodeString;
    procedure Set_FamilyName(Value: UnicodeString);
    procedure Set_FirstName(Value: UnicodeString);
    procedure Set_Patronymic(Value: UnicodeString);
  end;

{ TXMLTNameOwner }

  TXMLTNameOwner = class(TXMLNode, IXMLTNameOwner)
  protected
    { IXMLTNameOwner }
    function Get_Name: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
  end;

{ TXMLTShare }

  TXMLTShare = class(TXMLNode, IXMLTShare)
  protected
    { IXMLTShare }
    function Get_Numerator: Integer;
    function Get_Denominator: Integer;
    procedure Set_Numerator(Value: Integer);
    procedure Set_Denominator(Value: Integer);
  end;

{ TXMLTRegistration }

  TXMLTRegistration = class(TXMLNode, IXMLTRegistration)
  protected
    { IXMLTRegistration }
    function Get_RegNumber: UnicodeString;
    function Get_RegDate: UnicodeString;
    procedure Set_RegNumber(Value: UnicodeString);
    procedure Set_RegDate(Value: UnicodeString);
  end;

{ TXMLTRight_Documents }

  TXMLTRight_Documents = class(TXMLNodeCollection, IXMLTRight_Documents)
  protected
    { IXMLTRight_Documents }
    function Get_Document(Index: Integer): IXMLTDocumentWithoutAppliedFile_DocOut3;
    function Add: IXMLTDocumentWithoutAppliedFile_DocOut3;
    function Insert(const Index: Integer): IXMLTDocumentWithoutAppliedFile_DocOut3;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTDocumentWithoutAppliedFile_DocOut3 }

  TXMLTDocumentWithoutAppliedFile_DocOut3 = class(TXMLNode, IXMLTDocumentWithoutAppliedFile_DocOut3)
  protected
    { IXMLTDocumentWithoutAppliedFile_DocOut3 }
    function Get_CodeDocument: UnicodeString;
    function Get_Name: UnicodeString;
    function Get_Series: UnicodeString;
    function Get_Number: UnicodeString;
    function Get_Date: UnicodeString;
    function Get_IssueOrgan: UnicodeString;
    function Get_Desc: UnicodeString;
    procedure Set_CodeDocument(Value: UnicodeString);
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Series(Value: UnicodeString);
    procedure Set_Number(Value: UnicodeString);
    procedure Set_Date(Value: UnicodeString);
    procedure Set_IssueOrgan(Value: UnicodeString);
    procedure Set_Desc(Value: UnicodeString);
  end;

{ TXMLTParcel_SubParcels }

  TXMLTParcel_SubParcels = class(TXMLNodeCollection, IXMLTParcel_SubParcels)
  protected
    { IXMLTParcel_SubParcels }
    function Get_SubParcel(Index: Integer): IXMLTSubParcel;
    function Add: IXMLTSubParcel;
    function Insert(const Index: Integer): IXMLTSubParcel;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTSubParcel }

  TXMLTSubParcel = class(TXMLNode, IXMLTSubParcel)
  protected
    { IXMLTSubParcel }
    function Get_NumberRecord: UnicodeString;
    function Get_Full: Boolean;
    function Get_State: UnicodeString;
    function Get_DateExpiry: UnicodeString;
    function Get_Area: IXMLTAreaWithoutInaccuracyOut;
    function Get_Encumbrance: IXMLTEncumbranceZU;
    function Get_EntitySpatial: IXMLTEntitySpatialZUOut_Spa2;
    procedure Set_NumberRecord(Value: UnicodeString);
    procedure Set_Full(Value: Boolean);
    procedure Set_State(Value: UnicodeString);
    procedure Set_DateExpiry(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTAreaWithoutInaccuracyOut }

  TXMLTAreaWithoutInaccuracyOut = class(TXMLNode, IXMLTAreaWithoutInaccuracyOut)
  protected
    { IXMLTAreaWithoutInaccuracyOut }
    function Get_Area: UnicodeString;
    function Get_Unit_: UnicodeString;
    procedure Set_Area(Value: UnicodeString);
    procedure Set_Unit_(Value: UnicodeString);
  end;

{ TXMLTEncumbranceZU }

  TXMLTEncumbranceZU = class(TXMLNode, IXMLTEncumbranceZU)
  protected
    { IXMLTEncumbranceZU }
    function Get_Name: UnicodeString;
    function Get_Type_: UnicodeString;
    function Get_AccountNumber: UnicodeString;
    function Get_CadastralNumberRestriction: UnicodeString;
    function Get_OwnersRestrictionInFavorem: IXMLTOwnerRestrictionInFavorem;
    function Get_Duration: IXMLTDuration;
    function Get_Registration: IXMLTRegistration;
    function Get_Document: IXMLTDocumentWithoutAppliedFile_DocOut3;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_AccountNumber(Value: UnicodeString);
    procedure Set_CadastralNumberRestriction(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTOwnerRestrictionInFavorem }

  TXMLTOwnerRestrictionInFavorem = class(TXMLNodeCollection, IXMLTOwnerRestrictionInFavorem)
  protected
    { IXMLTOwnerRestrictionInFavorem }
    function Get_OwnerRestrictionInFavorem(Index: Integer): IXMLTOwner;
    function Add: IXMLTOwner;
    function Insert(const Index: Integer): IXMLTOwner;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTDuration }

  TXMLTDuration = class(TXMLNode, IXMLTDuration)
  protected
    { IXMLTDuration }
    function Get_Started: UnicodeString;
    function Get_Stopped: UnicodeString;
    function Get_Term: UnicodeString;
    procedure Set_Started(Value: UnicodeString);
    procedure Set_Stopped(Value: UnicodeString);
    procedure Set_Term(Value: UnicodeString);
  end;

{ TXMLTEntitySpatialZUOut_Spa2 }

  TXMLTEntitySpatialZUOut_Spa2 = class(TXMLNodeCollection, IXMLTEntitySpatialZUOut_Spa2)
  protected
    { IXMLTEntitySpatialZUOut_Spa2 }
    function Get_EntSys: UnicodeString;
    function Get_SpatialElement(Index: Integer): IXMLTSpatialElementZUOut_Spa2;
    procedure Set_EntSys(Value: UnicodeString);
    function Add: IXMLTSpatialElementZUOut_Spa2;
    function Insert(const Index: Integer): IXMLTSpatialElementZUOut_Spa2;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTSpatialElementZUOut_Spa2 }

  TXMLTSpatialElementZUOut_Spa2 = class(TXMLNodeCollection, IXMLTSpatialElementZUOut_Spa2)
  protected
    { IXMLTSpatialElementZUOut_Spa2 }
    function Get_SpelementUnit(Index: Integer): IXMLTSpelementUnitZUOut_Spa2;
    function Add: IXMLTSpelementUnitZUOut_Spa2;
    function Insert(const Index: Integer): IXMLTSpelementUnitZUOut_Spa2;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTSpelementUnitZUOut_Spa2 }

  TXMLTSpelementUnitZUOut_Spa2 = class(TXMLNode, IXMLTSpelementUnitZUOut_Spa2)
  protected
    { IXMLTSpelementUnitZUOut_Spa2 }
    function Get_TypeUnit: UnicodeString;
    function Get_SuNmb: LongWord;
    function Get_Ordinate: IXMLOrdinate_Spa2;
    procedure Set_TypeUnit(Value: UnicodeString);
    procedure Set_SuNmb(Value: LongWord);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTOrdinateOut_Spa2 }

  TXMLTOrdinateOut_Spa2 = class(TXMLNode, IXMLTOrdinateOut_Spa2)
  protected
    { IXMLTOrdinateOut_Spa2 }
    function Get_X: UnicodeString;
    function Get_Y: UnicodeString;
    function Get_OrdNmb: LongWord;
    function Get_NumGeopoint: LongWord;
    function Get_DeltaGeopoint: UnicodeString;
    procedure Set_X(Value: UnicodeString);
    procedure Set_Y(Value: UnicodeString);
    procedure Set_OrdNmb(Value: LongWord);
    procedure Set_NumGeopoint(Value: LongWord);
    procedure Set_DeltaGeopoint(Value: UnicodeString);
  end;

{ TXMLOrdinate_Spa2 }

  TXMLOrdinate_Spa2 = class(TXMLTOrdinateOut_Spa2, IXMLOrdinate_Spa2)
  protected
    { IXMLOrdinate_Spa2 }
    function Get_GeopointZacrep: UnicodeString;
    procedure Set_GeopointZacrep(Value: UnicodeString);
  end;

{ TXMLTParcel_Contours }

  TXMLTParcel_Contours = class(TXMLNodeCollection, IXMLTParcel_Contours)
  protected
    { IXMLTParcel_Contours }
    function Get_Contour(Index: Integer): IXMLTContour;
    function Add: IXMLTContour;
    function Insert(const Index: Integer): IXMLTContour;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTContour }

  TXMLTContour = class(TXMLNode, IXMLTContour)
  protected
    { IXMLTContour }
    function Get_NumberRecord: LongWord;
    function Get_Area: IXMLTAreaWithoutInaccuracyOut;
    function Get_EntitySpatial: IXMLTEntitySpatialZUOut_Spa2;
    procedure Set_NumberRecord(Value: LongWord);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTParcel_CompositionEZ }

  TXMLTParcel_CompositionEZ = class(TXMLNodeCollection, IXMLTParcel_CompositionEZ)
  protected
    { IXMLTParcel_CompositionEZ }
    function Get_EntryParcel(Index: Integer): IXMLTEntryParcel;
    function Add: IXMLTEntryParcel;
    function Insert(const Index: Integer): IXMLTEntryParcel;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTEntryParcel }

  TXMLTEntryParcel = class(TXMLNode, IXMLTEntryParcel)
  protected
    { IXMLTEntryParcel }
    function Get_CadastralNumber: UnicodeString;
    function Get_Area: IXMLTAreaWithoutInaccuracyOut;
    function Get_EntitySpatial: IXMLTEntitySpatialZUOut_Spa2;
    procedure Set_CadastralNumber(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTParcel_Encumbrances }

  TXMLTParcel_Encumbrances = class(TXMLNodeCollection, IXMLTParcel_Encumbrances)
  protected
    { IXMLTParcel_Encumbrances }
    function Get_Encumbrance(Index: Integer): IXMLTEncumbranceZU;
    function Add: IXMLTEncumbranceZU;
    function Insert(const Index: Integer): IXMLTEncumbranceZU;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTCadastralCost_Cos1 }

  TXMLTCadastralCost_Cos1 = class(TXMLNode, IXMLTCadastralCost_Cos1)
  protected
    { IXMLTCadastralCost_Cos1 }
    function Get_Value: UnicodeString;
    function Get_Unit_: UnicodeString;
    procedure Set_Value(Value: UnicodeString);
    procedure Set_Unit_(Value: UnicodeString);
  end;

{ TXMLTCadastralNumberOut }

  TXMLTCadastralNumberOut = class(TXMLNode, IXMLTCadastralNumberOut)
  protected
    { IXMLTCadastralNumberOut }
    function Get_CadastralNumber: UnicodeString;
    procedure Set_CadastralNumber(Value: UnicodeString);
  end;

{ TXMLTCertificationDoc_Cer1 }

  TXMLTCertificationDoc_Cer1 = class(TXMLNode, IXMLTCertificationDoc_Cer1)
  protected
    { IXMLTCertificationDoc_Cer1 }
    function Get_Organization: UnicodeString;
    function Get_Date: UnicodeString;
    function Get_Number: UnicodeString;
    function Get_Official: IXMLOfficial_Cer1;
    procedure Set_Organization(Value: UnicodeString);
    procedure Set_Date(Value: UnicodeString);
    procedure Set_Number(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLOfficial_Cer1 }

  TXMLOfficial_Cer1 = class(TXMLNode, IXMLOfficial_Cer1)
  protected
    { IXMLOfficial_Cer1 }
    function Get_Appointment: UnicodeString;
    function Get_FamilyName: UnicodeString;
    function Get_FirstName: UnicodeString;
    function Get_Patronymic: UnicodeString;
    procedure Set_Appointment(Value: UnicodeString);
    procedure Set_FamilyName(Value: UnicodeString);
    procedure Set_FirstName(Value: UnicodeString);
    procedure Set_Patronymic(Value: UnicodeString);
  end;

{ TXMLKPZU_Contractors }

  TXMLKPZU_Contractors = class(TXMLNodeCollection, IXMLKPZU_Contractors)
  protected
    { IXMLKPZU_Contractors }
    function Get_Contractor(Index: Integer): IXMLKPZU_Contractors_Contractor;
    function Add: IXMLKPZU_Contractors_Contractor;
    function Insert(const Index: Integer): IXMLKPZU_Contractors_Contractor;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTEngineerOut }

  TXMLTEngineerOut = class(TXMLNode, IXMLTEngineerOut)
  protected
    { IXMLTEngineerOut }
    function Get_FamilyName: UnicodeString;
    function Get_FirstName: UnicodeString;
    function Get_Patronymic: UnicodeString;
    function Get_NCertificate: UnicodeString;
    function Get_Organization: IXMLTOrganizationNameOut;
    procedure Set_FamilyName(Value: UnicodeString);
    procedure Set_FirstName(Value: UnicodeString);
    procedure Set_Patronymic(Value: UnicodeString);
    procedure Set_NCertificate(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTOrganizationNameOut }

  TXMLTOrganizationNameOut = class(TXMLNode, IXMLTOrganizationNameOut)
  protected
    { IXMLTOrganizationNameOut }
    function Get_Name: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
  end;

{ TXMLKPZU_Contractors_Contractor }

  TXMLKPZU_Contractors_Contractor = class(TXMLTEngineerOut, IXMLKPZU_Contractors_Contractor)
  protected
    { IXMLKPZU_Contractors_Contractor }
    function Get_Date: UnicodeString;
    procedure Set_Date(Value: UnicodeString);
  end;

{ TXMLTCoordSystems_Spa2 }

  TXMLTCoordSystems_Spa2 = class(TXMLNodeCollection, IXMLTCoordSystems_Spa2)
  protected
    { IXMLTCoordSystems_Spa2 }
    function Get_CoordSystem(Index: Integer): IXMLTCoordSystem_Spa2;
    function Add: IXMLTCoordSystem_Spa2;
    function Insert(const Index: Integer): IXMLTCoordSystem_Spa2;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTCoordSystem_Spa2 }

  TXMLTCoordSystem_Spa2 = class(TXMLNode, IXMLTCoordSystem_Spa2)
  protected
    { IXMLTCoordSystem_Spa2 }
    function Get_Name: UnicodeString;
    function Get_CsId: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_CsId(Value: UnicodeString);
  end;

{ Global Functions }

function GetKPZU(Doc: IXMLDocument): IXMLKPZU;
function LoadKPZU(const FileName: string): IXMLKPZU;
function NewKPZU: IXMLKPZU;

const
  TargetNamespace = 'urn://x-artefacts-rosreestr-ru/outgoing/kpzu/5.0.8';


implementation

{ Global Functions }

function GetKPZU(Doc: IXMLDocument): IXMLKPZU;
begin
  Result := Doc.GetDocBinding('KPZU', TXMLKPZU, TargetNamespace) as IXMLKPZU;
end;

function LoadKPZU(const FileName: string): IXMLKPZU;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('KPZU', TXMLKPZU, TargetNamespace) as IXMLKPZU;
end;

function NewKPZU: IXMLKPZU;
begin
  Result := NewXMLDocument.GetDocBinding('KPZU', TXMLKPZU, TargetNamespace) as IXMLKPZU;
end;


{ TXMLKPZU }

procedure TXMLKPZU.AfterConstruction;
begin
  RegisterChildNode('Parcel', TXMLTParcel);
  RegisterChildNode('CertificationDoc', TXMLTCertificationDoc_Cer1);
  RegisterChildNode('Contractors', TXMLKPZU_Contractors);
  RegisterChildNode('CoordSystems', TXMLTCoordSystems_Spa2);
  inherited;
end;

function TXMLKPZU.Get_Parcel: IXMLTParcel;
begin
  Result := ChildNodes['Parcel'] as IXMLTParcel;
end;

function TXMLKPZU.Get_CertificationDoc: IXMLTCertificationDoc_Cer1;
begin
  Result := ChildNodes['CertificationDoc'] as IXMLTCertificationDoc_Cer1;
end;

function TXMLKPZU.Get_Contractors: IXMLKPZU_Contractors;
begin
  Result := ChildNodes['Contractors'] as IXMLKPZU_Contractors;
end;

function TXMLKPZU.Get_CoordSystems: IXMLTCoordSystems_Spa2;
begin
  Result := ChildNodes['CoordSystems'] as IXMLTCoordSystems_Spa2;
end;

{ TXMLTParcel }

procedure TXMLTParcel.AfterConstruction;
begin
  RegisterChildNode('OldNumbers', TXMLTOldNumbers_Num1);
  RegisterChildNode('PrevCadastralNumbers', TXMLTCadastralNumbersOut);
  RegisterChildNode('InnerCadastralNumbers', TXMLTCadastralNumbersOut);
  RegisterChildNode('Area', TXMLTAreaOut);
  RegisterChildNode('Location', TXMLTLocation);
  RegisterChildNode('Utilization', TXMLTUtilization);
  RegisterChildNode('NaturalObjects', TXMLTNaturalObjects_NatObj1);
  RegisterChildNode('Rights', TXMLTParcel_Rights);
  RegisterChildNode('SubParcels', TXMLTParcel_SubParcels);
  RegisterChildNode('EntitySpatial', TXMLTEntitySpatialZUOut_Spa2);
  RegisterChildNode('Contours', TXMLTParcel_Contours);
  RegisterChildNode('CompositionEZ', TXMLTParcel_CompositionEZ);
  RegisterChildNode('Encumbrances', TXMLTParcel_Encumbrances);
  RegisterChildNode('CadastralCost', TXMLTCadastralCost_Cos1);
  RegisterChildNode('AllNewParcels', TXMLTCadastralNumbersOut);
  RegisterChildNode('ChangeParcel', TXMLTCadastralNumberOut);
  RegisterChildNode('RemoveParcels', TXMLTCadastralNumbersOut);
  RegisterChildNode('AllOffspringParcel', TXMLTCadastralNumbersOut);
  inherited;
end;

function TXMLTParcel.Get_CadastralNumber: UnicodeString;
begin
  Result := AttributeNodes['CadastralNumber'].Text;
end;

procedure TXMLTParcel.Set_CadastralNumber(Value: UnicodeString);
begin
  SetAttribute('CadastralNumber', Value);
end;

function TXMLTParcel.Get_State: UnicodeString;
begin
  Result := AttributeNodes['State'].Text;
end;

procedure TXMLTParcel.Set_State(Value: UnicodeString);
begin
  SetAttribute('State', Value);
end;

function TXMLTParcel.Get_DateExpiry: UnicodeString;
begin
  Result := AttributeNodes['DateExpiry'].Text;
end;

procedure TXMLTParcel.Set_DateExpiry(Value: UnicodeString);
begin
  SetAttribute('DateExpiry', Value);
end;

function TXMLTParcel.Get_DateCreated: UnicodeString;
begin
  Result := AttributeNodes['DateCreated'].Text;
end;

procedure TXMLTParcel.Set_DateCreated(Value: UnicodeString);
begin
  SetAttribute('DateCreated', Value);
end;

function TXMLTParcel.Get_DateRemoved: UnicodeString;
begin
  Result := AttributeNodes['DateRemoved'].Text;
end;

procedure TXMLTParcel.Set_DateRemoved(Value: UnicodeString);
begin
  SetAttribute('DateRemoved', Value);
end;

function TXMLTParcel.Get_Method: UnicodeString;
begin
  Result := AttributeNodes['Method'].Text;
end;

procedure TXMLTParcel.Set_Method(Value: UnicodeString);
begin
  SetAttribute('Method', Value);
end;

function TXMLTParcel.Get_DateCreatedDoc: UnicodeString;
begin
  Result := AttributeNodes['DateCreatedDoc'].Text;
end;

procedure TXMLTParcel.Set_DateCreatedDoc(Value: UnicodeString);
begin
  SetAttribute('DateCreatedDoc', Value);
end;

function TXMLTParcel.Get_CadastralBlock: UnicodeString;
begin
  Result := ChildNodes['CadastralBlock'].Text;
end;

procedure TXMLTParcel.Set_CadastralBlock(Value: UnicodeString);
begin
  ChildNodes['CadastralBlock'].NodeValue := Value;
end;

function TXMLTParcel.Get_Name: UnicodeString;
begin
  Result := ChildNodes['Name'].Text;
end;

procedure TXMLTParcel.Set_Name(Value: UnicodeString);
begin
  ChildNodes['Name'].NodeValue := Value;
end;

function TXMLTParcel.Get_OldNumbers: IXMLTOldNumbers_Num1;
begin
  Result := ChildNodes['OldNumbers'] as IXMLTOldNumbers_Num1;
end;

function TXMLTParcel.Get_PrevCadastralNumbers: IXMLTCadastralNumbersOut;
begin
  Result := ChildNodes['PrevCadastralNumbers'] as IXMLTCadastralNumbersOut;
end;

function TXMLTParcel.Get_InnerCadastralNumbers: IXMLTCadastralNumbersOut;
begin
  Result := ChildNodes['InnerCadastralNumbers'] as IXMLTCadastralNumbersOut;
end;

function TXMLTParcel.Get_Area: IXMLTAreaOut;
begin
  Result := ChildNodes['Area'] as IXMLTAreaOut;
end;

function TXMLTParcel.Get_Location: IXMLTLocation;
begin
  Result := ChildNodes['Location'] as IXMLTLocation;
end;

function TXMLTParcel.Get_Category: UnicodeString;
begin
  Result := ChildNodes['Category'].Text;
end;

procedure TXMLTParcel.Set_Category(Value: UnicodeString);
begin
  ChildNodes['Category'].NodeValue := Value;
end;

function TXMLTParcel.Get_Utilization: IXMLTUtilization;
begin
  Result := ChildNodes['Utilization'] as IXMLTUtilization;
end;

function TXMLTParcel.Get_NaturalObjects: IXMLTNaturalObjects_NatObj1;
begin
  Result := ChildNodes['NaturalObjects'] as IXMLTNaturalObjects_NatObj1;
end;

function TXMLTParcel.Get_Rights: IXMLTParcel_Rights;
begin
  Result := ChildNodes['Rights'] as IXMLTParcel_Rights;
end;

function TXMLTParcel.Get_SubParcels: IXMLTParcel_SubParcels;
begin
  Result := ChildNodes['SubParcels'] as IXMLTParcel_SubParcels;
end;

function TXMLTParcel.Get_EntitySpatial: IXMLTEntitySpatialZUOut_Spa2;
begin
  Result := ChildNodes['EntitySpatial'] as IXMLTEntitySpatialZUOut_Spa2;
end;

function TXMLTParcel.Get_Contours: IXMLTParcel_Contours;
begin
  Result := ChildNodes['Contours'] as IXMLTParcel_Contours;
end;

function TXMLTParcel.Get_CompositionEZ: IXMLTParcel_CompositionEZ;
begin
  Result := ChildNodes['CompositionEZ'] as IXMLTParcel_CompositionEZ;
end;

function TXMLTParcel.Get_Encumbrances: IXMLTParcel_Encumbrances;
begin
  Result := ChildNodes['Encumbrances'] as IXMLTParcel_Encumbrances;
end;

function TXMLTParcel.Get_CadastralCost: IXMLTCadastralCost_Cos1;
begin
  Result := ChildNodes['CadastralCost'] as IXMLTCadastralCost_Cos1;
end;

function TXMLTParcel.Get_SpecialNote: UnicodeString;
begin
  Result := ChildNodes['SpecialNote'].Text;
end;

procedure TXMLTParcel.Set_SpecialNote(Value: UnicodeString);
begin
  ChildNodes['SpecialNote'].NodeValue := Value;
end;

function TXMLTParcel.Get_AllNewParcels: IXMLTCadastralNumbersOut;
begin
  Result := ChildNodes['AllNewParcels'] as IXMLTCadastralNumbersOut;
end;

function TXMLTParcel.Get_ChangeParcel: IXMLTCadastralNumberOut;
begin
  Result := ChildNodes['ChangeParcel'] as IXMLTCadastralNumberOut;
end;

function TXMLTParcel.Get_RemoveParcels: IXMLTCadastralNumbersOut;
begin
  Result := ChildNodes['RemoveParcels'] as IXMLTCadastralNumbersOut;
end;

function TXMLTParcel.Get_AllOffspringParcel: IXMLTCadastralNumbersOut;
begin
  Result := ChildNodes['AllOffspringParcel'] as IXMLTCadastralNumbersOut;
end;

{ TXMLTOldNumbers_Num1 }

procedure TXMLTOldNumbers_Num1.AfterConstruction;
begin
  RegisterChildNode('OldNumber', TXMLTOldNumber_Num1);
  ItemTag := 'OldNumber';
  ItemInterface := IXMLTOldNumber_Num1;
  inherited;
end;

function TXMLTOldNumbers_Num1.Get_OldNumber(Index: Integer): IXMLTOldNumber_Num1;
begin
  Result := List[Index] as IXMLTOldNumber_Num1;
end;

function TXMLTOldNumbers_Num1.Add: IXMLTOldNumber_Num1;
begin
  Result := AddItem(-1) as IXMLTOldNumber_Num1;
end;

function TXMLTOldNumbers_Num1.Insert(const Index: Integer): IXMLTOldNumber_Num1;
begin
  Result := AddItem(Index) as IXMLTOldNumber_Num1;
end;

{ TXMLTOldNumber_Num1 }

function TXMLTOldNumber_Num1.Get_Type_: UnicodeString;
begin
  Result := AttributeNodes['Type'].Text;
end;

procedure TXMLTOldNumber_Num1.Set_Type_(Value: UnicodeString);
begin
  SetAttribute('Type', Value);
end;

function TXMLTOldNumber_Num1.Get_Number: UnicodeString;
begin
  Result := AttributeNodes['Number'].Text;
end;

procedure TXMLTOldNumber_Num1.Set_Number(Value: UnicodeString);
begin
  SetAttribute('Number', Value);
end;

{ TXMLTCadastralNumbersOut }

procedure TXMLTCadastralNumbersOut.AfterConstruction;
begin
  ItemTag := 'CadastralNumber';
  ItemInterface := IXMLNode;
  inherited;
end;

function TXMLTCadastralNumbersOut.Get_CadastralNumber(Index: Integer): UnicodeString;
begin
  Result := List[Index].Text;
end;

function TXMLTCadastralNumbersOut.Add(const CadastralNumber: UnicodeString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := CadastralNumber;
end;

function TXMLTCadastralNumbersOut.Insert(const Index: Integer; const CadastralNumber: UnicodeString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := CadastralNumber;
end;

{ TXMLTAreaOut }

function TXMLTAreaOut.Get_Area: LongWord;
begin
  Result := ChildNodes['Area'].NodeValue;
end;

procedure TXMLTAreaOut.Set_Area(Value: LongWord);
begin
  ChildNodes['Area'].NodeValue := Value;
end;

function TXMLTAreaOut.Get_Unit_: UnicodeString;
begin
  Result := ChildNodes['Unit'].Text;
end;

procedure TXMLTAreaOut.Set_Unit_(Value: UnicodeString);
begin
  ChildNodes['Unit'].NodeValue := Value;
end;

function TXMLTAreaOut.Get_Inaccuracy: UnicodeString;
begin
  Result := ChildNodes['Inaccuracy'].Text;
end;

procedure TXMLTAreaOut.Set_Inaccuracy(Value: UnicodeString);
begin
  ChildNodes['Inaccuracy'].NodeValue := Value;
end;

{ TXMLTLocation }

procedure TXMLTLocation.AfterConstruction;
begin
  RegisterChildNode('Elaboration', TXMLTElaborationLocation);
  RegisterChildNode('Address', TXMLTAddressOut_adrOut3);
  inherited;
end;

function TXMLTLocation.Get_InBounds: UnicodeString;
begin
  Result := ChildNodes['inBounds'].Text;
end;

procedure TXMLTLocation.Set_InBounds(Value: UnicodeString);
begin
  ChildNodes['inBounds'].NodeValue := Value;
end;

function TXMLTLocation.Get_Placed: UnicodeString;
begin
  Result := ChildNodes['Placed'].Text;
end;

procedure TXMLTLocation.Set_Placed(Value: UnicodeString);
begin
  ChildNodes['Placed'].NodeValue := Value;
end;

function TXMLTLocation.Get_Elaboration: IXMLTElaborationLocation;
begin
  Result := ChildNodes['Elaboration'] as IXMLTElaborationLocation;
end;

function TXMLTLocation.Get_Address: IXMLTAddressOut_adrOut3;
begin
  Result := ChildNodes['Address'] as IXMLTAddressOut_adrOut3;
end;

{ TXMLTElaborationLocation }

function TXMLTElaborationLocation.Get_ReferenceMark: UnicodeString;
begin
  Result := ChildNodes['ReferenceMark'].Text;
end;

procedure TXMLTElaborationLocation.Set_ReferenceMark(Value: UnicodeString);
begin
  ChildNodes['ReferenceMark'].NodeValue := Value;
end;

function TXMLTElaborationLocation.Get_Distance: UnicodeString;
begin
  Result := ChildNodes['Distance'].Text;
end;

procedure TXMLTElaborationLocation.Set_Distance(Value: UnicodeString);
begin
  ChildNodes['Distance'].NodeValue := Value;
end;

function TXMLTElaborationLocation.Get_Direction: UnicodeString;
begin
  Result := ChildNodes['Direction'].Text;
end;

procedure TXMLTElaborationLocation.Set_Direction(Value: UnicodeString);
begin
  ChildNodes['Direction'].NodeValue := Value;
end;

{ TXMLTAddressOut_adrOut3 }

procedure TXMLTAddressOut_adrOut3.AfterConstruction;
begin
  RegisterChildNode('District', TXMLTName_adrOut3);
  RegisterChildNode('City', TXMLTName_adrOut3);
  RegisterChildNode('UrbanDistrict', TXMLTUrbanDistrict_adrOut3);
  RegisterChildNode('SovietVillage', TXMLTSovietVillage_adrOut3);
  RegisterChildNode('Locality', TXMLTName_adrOut3);
  RegisterChildNode('Street', TXMLTName_adrOut3);
  RegisterChildNode('Level1', TXMLTLevel1_adrOut3);
  RegisterChildNode('Level2', TXMLTLevel2_adrOut3);
  RegisterChildNode('Level3', TXMLTLevel3_adrOut3);
  RegisterChildNode('Apartment', TXMLTApartment_adrOut3);
  inherited;
end;

function TXMLTAddressOut_adrOut3.Get_OKATO: UnicodeString;
begin
  //Result := ChildNodes.FindNode['OKATO','urn://x-artefacts-rosreestr-ru/commons/complex-types/address-output/3.0.1'].Text;
end;

procedure TXMLTAddressOut_adrOut3.Set_OKATO(Value: UnicodeString);
begin
  ChildNodes['OKATO'].NodeValue := Value;
end;

function TXMLTAddressOut_adrOut3.Get_KLADR: UnicodeString;
begin
  //Result := ChildNodes.FindNode['KLADR','urn://x-artefacts-rosreestr-ru/commons/complex-types/address-output/3.0.1'].Text;
end;

procedure TXMLTAddressOut_adrOut3.Set_KLADR(Value: UnicodeString);
begin
  ChildNodes['KLADR'].NodeValue := Value;
end;

function TXMLTAddressOut_adrOut3.Get_OKTMO: UnicodeString;
begin
  Result := ChildNodes['OKTMO'].Text;
end;

procedure TXMLTAddressOut_adrOut3.Set_OKTMO(Value: UnicodeString);
begin
  ChildNodes['OKTMO'].NodeValue := Value;
end;

function TXMLTAddressOut_adrOut3.Get_PostalCode: UnicodeString;
begin
  Result := ChildNodes['PostalCode'].Text;
end;

procedure TXMLTAddressOut_adrOut3.Set_PostalCode(Value: UnicodeString);
begin
  ChildNodes['PostalCode'].NodeValue := Value;
end;

function TXMLTAddressOut_adrOut3.Get_Region: UnicodeString;
begin
  Result := ChildNodes['Region'].Text;
end;

procedure TXMLTAddressOut_adrOut3.Set_Region(Value: UnicodeString);
begin
  ChildNodes['Region'].NodeValue := Value;
end;

function TXMLTAddressOut_adrOut3.Get_District: IXMLTName_adrOut3;
begin
  Result := ChildNodes['District'] as IXMLTName_adrOut3;
end;

function TXMLTAddressOut_adrOut3.Get_City: IXMLTName_adrOut3;
begin
  Result := ChildNodes['City'] as IXMLTName_adrOut3;
end;

function TXMLTAddressOut_adrOut3.Get_UrbanDistrict: IXMLTUrbanDistrict_adrOut3;
begin
  Result := ChildNodes['UrbanDistrict'] as IXMLTUrbanDistrict_adrOut3;
end;

function TXMLTAddressOut_adrOut3.Get_SovietVillage: IXMLTSovietVillage_adrOut3;
begin
  Result := ChildNodes['SovietVillage'] as IXMLTSovietVillage_adrOut3;
end;

function TXMLTAddressOut_adrOut3.Get_Locality: IXMLTName_adrOut3;
begin
  Result := ChildNodes['Locality'] as IXMLTName_adrOut3;
end;

function TXMLTAddressOut_adrOut3.Get_Street: IXMLTName_adrOut3;
begin
  Result := ChildNodes['Street'] as IXMLTName_adrOut3;
end;

function TXMLTAddressOut_adrOut3.Get_Level1: IXMLTLevel1_adrOut3;
begin
  Result := ChildNodes['Level1'] as IXMLTLevel1_adrOut3;
end;

function TXMLTAddressOut_adrOut3.Get_Level2: IXMLTLevel2_adrOut3;
begin
  Result := ChildNodes['Level2'] as IXMLTLevel2_adrOut3;
end;

function TXMLTAddressOut_adrOut3.Get_Level3: IXMLTLevel3_adrOut3;
begin
  Result := ChildNodes['Level3'] as IXMLTLevel3_adrOut3;
end;

function TXMLTAddressOut_adrOut3.Get_Apartment: IXMLTApartment_adrOut3;
begin
  Result := ChildNodes['Apartment'] as IXMLTApartment_adrOut3;
end;

function TXMLTAddressOut_adrOut3.Get_Other: UnicodeString;
begin
  Result := ChildNodes['Other'].Text;
end;

procedure TXMLTAddressOut_adrOut3.Set_Other(Value: UnicodeString);
begin
  ChildNodes['Other'].NodeValue := Value;
end;

function TXMLTAddressOut_adrOut3.Get_Note: UnicodeString;
begin
//  Result := ChildNodes.FindNode['Note','urn://x-artefacts-rosreestr-ru/commons/complex-types/address-output/3.0.1'].Text;
end;

procedure TXMLTAddressOut_adrOut3.Set_Note(Value: UnicodeString);
begin
  ChildNodes['Note'].NodeValue := Value;
end;

{ TXMLTName_adrOut3 }

function TXMLTName_adrOut3.Get_Name: UnicodeString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLTName_adrOut3.Set_Name(Value: UnicodeString);
begin
  SetAttribute('Name', Value);
end;

function TXMLTName_adrOut3.Get_Type_: UnicodeString;
begin
  Result := AttributeNodes['Type'].Text;
end;

procedure TXMLTName_adrOut3.Set_Type_(Value: UnicodeString);
begin
  SetAttribute('Type', Value);
end;

{ TXMLTUrbanDistrict_adrOut3 }

function TXMLTUrbanDistrict_adrOut3.Get_Name: UnicodeString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLTUrbanDistrict_adrOut3.Set_Name(Value: UnicodeString);
begin
  SetAttribute('Name', Value);
end;

function TXMLTUrbanDistrict_adrOut3.Get_Type_: UnicodeString;
begin
  Result := AttributeNodes['Type'].Text;
end;

procedure TXMLTUrbanDistrict_adrOut3.Set_Type_(Value: UnicodeString);
begin
  SetAttribute('Type', Value);
end;

{ TXMLTSovietVillage_adrOut3 }

function TXMLTSovietVillage_adrOut3.Get_Name: UnicodeString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLTSovietVillage_adrOut3.Set_Name(Value: UnicodeString);
begin
  SetAttribute('Name', Value);
end;

function TXMLTSovietVillage_adrOut3.Get_Type_: UnicodeString;
begin
  Result := AttributeNodes['Type'].Text;
end;

procedure TXMLTSovietVillage_adrOut3.Set_Type_(Value: UnicodeString);
begin
  SetAttribute('Type', Value);
end;

{ TXMLTLevel1_adrOut3 }

function TXMLTLevel1_adrOut3.Get_Type_: UnicodeString;
begin
  Result := AttributeNodes['Type'].Text;
end;

procedure TXMLTLevel1_adrOut3.Set_Type_(Value: UnicodeString);
begin
  SetAttribute('Type', Value);
end;

function TXMLTLevel1_adrOut3.Get_Value: UnicodeString;
begin
  Result := AttributeNodes['Value'].Text;
end;

procedure TXMLTLevel1_adrOut3.Set_Value(Value: UnicodeString);
begin
  SetAttribute('Value', Value);
end;

{ TXMLTLevel2_adrOut3 }

function TXMLTLevel2_adrOut3.Get_Type_: UnicodeString;
begin
  Result := AttributeNodes['Type'].Text;
end;

procedure TXMLTLevel2_adrOut3.Set_Type_(Value: UnicodeString);
begin
  SetAttribute('Type', Value);
end;

function TXMLTLevel2_adrOut3.Get_Value: UnicodeString;
begin
  Result := AttributeNodes['Value'].Text;
end;

procedure TXMLTLevel2_adrOut3.Set_Value(Value: UnicodeString);
begin
  SetAttribute('Value', Value);
end;

{ TXMLTLevel3_adrOut3 }

function TXMLTLevel3_adrOut3.Get_Type_: UnicodeString;
begin
  Result := AttributeNodes['Type'].Text;
end;

procedure TXMLTLevel3_adrOut3.Set_Type_(Value: UnicodeString);
begin
  SetAttribute('Type', Value);
end;

function TXMLTLevel3_adrOut3.Get_Value: UnicodeString;
begin
  Result := AttributeNodes['Value'].Text;
end;

procedure TXMLTLevel3_adrOut3.Set_Value(Value: UnicodeString);
begin
  SetAttribute('Value', Value);
end;

{ TXMLTApartment_adrOut3 }

function TXMLTApartment_adrOut3.Get_Type_: UnicodeString;
begin
  Result := AttributeNodes['Type'].Text;
end;

procedure TXMLTApartment_adrOut3.Set_Type_(Value: UnicodeString);
begin
  SetAttribute('Type', Value);
end;

function TXMLTApartment_adrOut3.Get_Value: UnicodeString;
begin
  Result := AttributeNodes['Value'].Text;
end;

procedure TXMLTApartment_adrOut3.Set_Value(Value: UnicodeString);
begin
  SetAttribute('Value', Value);
end;

{ TXMLTUtilization }

function TXMLTUtilization.Get_Utilization: UnicodeString;
begin
  Result := AttributeNodes['Utilization'].Text;
end;

procedure TXMLTUtilization.Set_Utilization(Value: UnicodeString);
begin
  SetAttribute('Utilization', Value);
end;

function TXMLTUtilization.Get_ByDoc: UnicodeString;
begin
  Result := AttributeNodes['ByDoc'].Text;
end;

procedure TXMLTUtilization.Set_ByDoc(Value: UnicodeString);
begin
  SetAttribute('ByDoc', Value);
end;

{ TXMLTNaturalObjects_NatObj1 }

procedure TXMLTNaturalObjects_NatObj1.AfterConstruction;
begin
  RegisterChildNode('NaturalObject', TXMLTNaturalObject_NatObj1);
  ItemTag := 'NaturalObject';
  ItemInterface := IXMLTNaturalObject_NatObj1;
  inherited;
end;

function TXMLTNaturalObjects_NatObj1.Get_NaturalObject(Index: Integer): IXMLTNaturalObject_NatObj1;
begin
  Result := List[Index] as IXMLTNaturalObject_NatObj1;
end;

function TXMLTNaturalObjects_NatObj1.Add: IXMLTNaturalObject_NatObj1;
begin
  Result := AddItem(-1) as IXMLTNaturalObject_NatObj1;
end;

function TXMLTNaturalObjects_NatObj1.Insert(const Index: Integer): IXMLTNaturalObject_NatObj1;
begin
  Result := AddItem(Index) as IXMLTNaturalObject_NatObj1;
end;

{ TXMLTNaturalObject_NatObj1 }

function TXMLTNaturalObject_NatObj1.Get_Kind: UnicodeString;
begin
  Result := ChildNodes['Kind'].Text;
end;

procedure TXMLTNaturalObject_NatObj1.Set_Kind(Value: UnicodeString);
begin
  ChildNodes['Kind'].NodeValue := Value;
end;

function TXMLTNaturalObject_NatObj1.Get_ForestUse: UnicodeString;
begin
  Result := ChildNodes['ForestUse'].Text;
end;

procedure TXMLTNaturalObject_NatObj1.Set_ForestUse(Value: UnicodeString);
begin
  ChildNodes['ForestUse'].NodeValue := Value;
end;

function TXMLTNaturalObject_NatObj1.Get_ProtectiveForest: UnicodeString;
begin
  Result := ChildNodes['ProtectiveForest'].Text;
end;

procedure TXMLTNaturalObject_NatObj1.Set_ProtectiveForest(Value: UnicodeString);
begin
  ChildNodes['ProtectiveForest'].NodeValue := Value;
end;

function TXMLTNaturalObject_NatObj1.Get_WaterObject: UnicodeString;
begin
  Result := ChildNodes['WaterObject'].Text;
end;

procedure TXMLTNaturalObject_NatObj1.Set_WaterObject(Value: UnicodeString);
begin
  ChildNodes['WaterObject'].NodeValue := Value;
end;

function TXMLTNaturalObject_NatObj1.Get_NameOther: UnicodeString;
begin
  Result := ChildNodes['NameOther'].Text;
end;

procedure TXMLTNaturalObject_NatObj1.Set_NameOther(Value: UnicodeString);
begin
  ChildNodes['NameOther'].NodeValue := Value;
end;

function TXMLTNaturalObject_NatObj1.Get_CharOther: UnicodeString;
begin
  Result := ChildNodes['CharOther'].Text;
end;

procedure TXMLTNaturalObject_NatObj1.Set_CharOther(Value: UnicodeString);
begin
  ChildNodes['CharOther'].NodeValue := Value;
end;

{ TXMLTParcel_Rights }

procedure TXMLTParcel_Rights.AfterConstruction;
begin
  RegisterChildNode('Right', TXMLTRight);
  ItemTag := 'Right';
  ItemInterface := IXMLTRight;
  inherited;
end;

function TXMLTParcel_Rights.Get_Right(Index: Integer): IXMLTRight;
begin
  Result := List[Index] as IXMLTRight;
end;

function TXMLTParcel_Rights.Add: IXMLTRight;
begin
  Result := AddItem(-1) as IXMLTRight;
end;

function TXMLTParcel_Rights.Insert(const Index: Integer): IXMLTRight;
begin
  Result := AddItem(Index) as IXMLTRight;
end;

{ TXMLTRight }

procedure TXMLTRight.AfterConstruction;
begin
  RegisterChildNode('Owners', TXMLTOwners);
  RegisterChildNode('Share', TXMLTShare);
  RegisterChildNode('Registration', TXMLTRegistration);
  RegisterChildNode('Documents', TXMLTRight_Documents);
  inherited;
end;

function TXMLTRight.Get_Name: UnicodeString;
begin
  Result := ChildNodes['Name'].Text;
end;

procedure TXMLTRight.Set_Name(Value: UnicodeString);
begin
  ChildNodes['Name'].NodeValue := Value;
end;

function TXMLTRight.Get_Type_: UnicodeString;
begin
  Result := ChildNodes['Type'].Text;
end;

procedure TXMLTRight.Set_Type_(Value: UnicodeString);
begin
  ChildNodes['Type'].NodeValue := Value;
end;

function TXMLTRight.Get_Owners: IXMLTOwners;
begin
  Result := ChildNodes['Owners'] as IXMLTOwners;
end;

function TXMLTRight.Get_Share: IXMLTShare;
begin
  Result := ChildNodes['Share'] as IXMLTShare;
end;

function TXMLTRight.Get_ShareText: UnicodeString;
begin
  Result := ChildNodes['ShareText'].Text;
end;

procedure TXMLTRight.Set_ShareText(Value: UnicodeString);
begin
  ChildNodes['ShareText'].NodeValue := Value;
end;

function TXMLTRight.Get_Desc: UnicodeString;
begin
  Result := ChildNodes['Desc'].Text;
end;

procedure TXMLTRight.Set_Desc(Value: UnicodeString);
begin
  ChildNodes['Desc'].NodeValue := Value;
end;

function TXMLTRight.Get_Registration: IXMLTRegistration;
begin
  Result := ChildNodes['Registration'] as IXMLTRegistration;
end;

function TXMLTRight.Get_Documents: IXMLTRight_Documents;
begin
  Result := ChildNodes['Documents'] as IXMLTRight_Documents;
end;

{ TXMLTOwners }

procedure TXMLTOwners.AfterConstruction;
begin
  RegisterChildNode('Owner', TXMLTOwner);
  ItemTag := 'Owner';
  ItemInterface := IXMLTOwner;
  inherited;
end;

function TXMLTOwners.Get_Owner(Index: Integer): IXMLTOwner;
begin
  Result := List[Index] as IXMLTOwner;
end;

function TXMLTOwners.Add: IXMLTOwner;
begin
  Result := AddItem(-1) as IXMLTOwner;
end;

function TXMLTOwners.Insert(const Index: Integer): IXMLTOwner;
begin
  Result := AddItem(Index) as IXMLTOwner;
end;

{ TXMLTOwner }

procedure TXMLTOwner.AfterConstruction;
begin
  RegisterChildNode('Person', TXMLTOwner_Person);
  RegisterChildNode('Organization', TXMLTNameOwner);
  RegisterChildNode('Governance', TXMLTNameOwner);
  inherited;
end;

function TXMLTOwner.Get_Person: IXMLTOwner_Person;
begin
  Result := ChildNodes['Person'] as IXMLTOwner_Person;
end;

function TXMLTOwner.Get_Organization: IXMLTNameOwner;
begin
  Result := ChildNodes['Organization'] as IXMLTNameOwner;
end;

function TXMLTOwner.Get_Governance: IXMLTNameOwner;
begin
  Result := ChildNodes['Governance'] as IXMLTNameOwner;
end;

{ TXMLTOwner_Person }

function TXMLTOwner_Person.Get_FamilyName: UnicodeString;
begin
  Result := ChildNodes['FamilyName'].Text;
end;

procedure TXMLTOwner_Person.Set_FamilyName(Value: UnicodeString);
begin
  ChildNodes['FamilyName'].NodeValue := Value;
end;

function TXMLTOwner_Person.Get_FirstName: UnicodeString;
begin
  Result := ChildNodes['FirstName'].Text;
end;

procedure TXMLTOwner_Person.Set_FirstName(Value: UnicodeString);
begin
  ChildNodes['FirstName'].NodeValue := Value;
end;

function TXMLTOwner_Person.Get_Patronymic: UnicodeString;
begin
  Result := ChildNodes['Patronymic'].Text;
end;

procedure TXMLTOwner_Person.Set_Patronymic(Value: UnicodeString);
begin
  ChildNodes['Patronymic'].NodeValue := Value;
end;

{ TXMLTNameOwner }

function TXMLTNameOwner.Get_Name: UnicodeString;
begin
  Result := ChildNodes['Name'].Text;
end;

procedure TXMLTNameOwner.Set_Name(Value: UnicodeString);
begin
  ChildNodes['Name'].NodeValue := Value;
end;

{ TXMLTShare }

function TXMLTShare.Get_Numerator: Integer;
begin
  Result := AttributeNodes['Numerator'].NodeValue;
end;

procedure TXMLTShare.Set_Numerator(Value: Integer);
begin
  SetAttribute('Numerator', Value);
end;

function TXMLTShare.Get_Denominator: Integer;
begin
  Result := AttributeNodes['Denominator'].NodeValue;
end;

procedure TXMLTShare.Set_Denominator(Value: Integer);
begin
  SetAttribute('Denominator', Value);
end;

{ TXMLTRegistration }

function TXMLTRegistration.Get_RegNumber: UnicodeString;
begin
  Result := ChildNodes['RegNumber'].Text;
end;

procedure TXMLTRegistration.Set_RegNumber(Value: UnicodeString);
begin
  ChildNodes['RegNumber'].NodeValue := Value;
end;

function TXMLTRegistration.Get_RegDate: UnicodeString;
begin
  Result := ChildNodes['RegDate'].Text;
end;

procedure TXMLTRegistration.Set_RegDate(Value: UnicodeString);
begin
  ChildNodes['RegDate'].NodeValue := Value;
end;

{ TXMLTRight_Documents }

procedure TXMLTRight_Documents.AfterConstruction;
begin
  RegisterChildNode('Document', TXMLTDocumentWithoutAppliedFile_DocOut3);
  ItemTag := 'Document';
  ItemInterface := IXMLTDocumentWithoutAppliedFile_DocOut3;
  inherited;
end;

function TXMLTRight_Documents.Get_Document(Index: Integer): IXMLTDocumentWithoutAppliedFile_DocOut3;
begin
  Result := List[Index] as IXMLTDocumentWithoutAppliedFile_DocOut3;
end;

function TXMLTRight_Documents.Add: IXMLTDocumentWithoutAppliedFile_DocOut3;
begin
  Result := AddItem(-1) as IXMLTDocumentWithoutAppliedFile_DocOut3;
end;

function TXMLTRight_Documents.Insert(const Index: Integer): IXMLTDocumentWithoutAppliedFile_DocOut3;
begin
  Result := AddItem(Index) as IXMLTDocumentWithoutAppliedFile_DocOut3;
end;

{ TXMLTDocumentWithoutAppliedFile_DocOut3 }

function TXMLTDocumentWithoutAppliedFile_DocOut3.Get_CodeDocument: UnicodeString;
begin
  Result := ChildNodes['CodeDocument'].Text;
end;

procedure TXMLTDocumentWithoutAppliedFile_DocOut3.Set_CodeDocument(Value: UnicodeString);
begin
  ChildNodes['CodeDocument'].NodeValue := Value;
end;

function TXMLTDocumentWithoutAppliedFile_DocOut3.Get_Name: UnicodeString;
begin
  Result := ChildNodes['Name'].Text;
end;

procedure TXMLTDocumentWithoutAppliedFile_DocOut3.Set_Name(Value: UnicodeString);
begin
  ChildNodes['Name'].NodeValue := Value;
end;

function TXMLTDocumentWithoutAppliedFile_DocOut3.Get_Series: UnicodeString;
begin
  Result := ChildNodes['Series'].Text;
end;

procedure TXMLTDocumentWithoutAppliedFile_DocOut3.Set_Series(Value: UnicodeString);
begin
  ChildNodes['Series'].NodeValue := Value;
end;

function TXMLTDocumentWithoutAppliedFile_DocOut3.Get_Number: UnicodeString;
begin
  Result := ChildNodes['Number'].Text;
end;

procedure TXMLTDocumentWithoutAppliedFile_DocOut3.Set_Number(Value: UnicodeString);
begin
  ChildNodes['Number'].NodeValue := Value;
end;

function TXMLTDocumentWithoutAppliedFile_DocOut3.Get_Date: UnicodeString;
begin
  Result := ChildNodes['Date'].Text;
end;

procedure TXMLTDocumentWithoutAppliedFile_DocOut3.Set_Date(Value: UnicodeString);
begin
  ChildNodes['Date'].NodeValue := Value;
end;

function TXMLTDocumentWithoutAppliedFile_DocOut3.Get_IssueOrgan: UnicodeString;
begin
  Result := ChildNodes['IssueOrgan'].Text;
end;

procedure TXMLTDocumentWithoutAppliedFile_DocOut3.Set_IssueOrgan(Value: UnicodeString);
begin
  ChildNodes['IssueOrgan'].NodeValue := Value;
end;

function TXMLTDocumentWithoutAppliedFile_DocOut3.Get_Desc: UnicodeString;
begin
  Result := ChildNodes['Desc'].Text;
end;

procedure TXMLTDocumentWithoutAppliedFile_DocOut3.Set_Desc(Value: UnicodeString);
begin
  ChildNodes['Desc'].NodeValue := Value;
end;

{ TXMLTParcel_SubParcels }

procedure TXMLTParcel_SubParcels.AfterConstruction;
begin
  RegisterChildNode('SubParcel', TXMLTSubParcel);
  ItemTag := 'SubParcel';
  ItemInterface := IXMLTSubParcel;
  inherited;
end;

function TXMLTParcel_SubParcels.Get_SubParcel(Index: Integer): IXMLTSubParcel;
begin
  Result := List[Index] as IXMLTSubParcel;
end;

function TXMLTParcel_SubParcels.Add: IXMLTSubParcel;
begin
  Result := AddItem(-1) as IXMLTSubParcel;
end;

function TXMLTParcel_SubParcels.Insert(const Index: Integer): IXMLTSubParcel;
begin
  Result := AddItem(Index) as IXMLTSubParcel;
end;

{ TXMLTSubParcel }

procedure TXMLTSubParcel.AfterConstruction;
begin
  RegisterChildNode('Area', TXMLTAreaWithoutInaccuracyOut);
  RegisterChildNode('Encumbrance', TXMLTEncumbranceZU);
  RegisterChildNode('EntitySpatial', TXMLTEntitySpatialZUOut_Spa2);
  inherited;
end;

function TXMLTSubParcel.Get_NumberRecord: UnicodeString;
begin
  Result := AttributeNodes['NumberRecord'].Text;
end;

procedure TXMLTSubParcel.Set_NumberRecord(Value: UnicodeString);
begin
  SetAttribute('NumberRecord', Value);
end;

function TXMLTSubParcel.Get_Full: Boolean;
begin
  Result := AttributeNodes['Full'].NodeValue;
end;

procedure TXMLTSubParcel.Set_Full(Value: Boolean);
begin
  SetAttribute('Full', Value);
end;

function TXMLTSubParcel.Get_State: UnicodeString;
begin
  Result := AttributeNodes['State'].Text;
end;

procedure TXMLTSubParcel.Set_State(Value: UnicodeString);
begin
  SetAttribute('State', Value);
end;

function TXMLTSubParcel.Get_DateExpiry: UnicodeString;
begin
  Result := AttributeNodes['DateExpiry'].Text;
end;

procedure TXMLTSubParcel.Set_DateExpiry(Value: UnicodeString);
begin
  SetAttribute('DateExpiry', Value);
end;

function TXMLTSubParcel.Get_Area: IXMLTAreaWithoutInaccuracyOut;
begin
  Result := ChildNodes['Area'] as IXMLTAreaWithoutInaccuracyOut;
end;

function TXMLTSubParcel.Get_Encumbrance: IXMLTEncumbranceZU;
begin
  Result := ChildNodes['Encumbrance'] as IXMLTEncumbranceZU;
end;

function TXMLTSubParcel.Get_EntitySpatial: IXMLTEntitySpatialZUOut_Spa2;
begin
  Result := ChildNodes['EntitySpatial'] as IXMLTEntitySpatialZUOut_Spa2;
end;

{ TXMLTAreaWithoutInaccuracyOut }

function TXMLTAreaWithoutInaccuracyOut.Get_Area: UnicodeString;
begin
  Result := ChildNodes['Area'].Text;
end;

procedure TXMLTAreaWithoutInaccuracyOut.Set_Area(Value: UnicodeString);
begin
  ChildNodes['Area'].NodeValue := Value;
end;

function TXMLTAreaWithoutInaccuracyOut.Get_Unit_: UnicodeString;
begin
  Result := ChildNodes['Unit'].Text;
end;

procedure TXMLTAreaWithoutInaccuracyOut.Set_Unit_(Value: UnicodeString);
begin
  ChildNodes['Unit'].NodeValue := Value;
end;

{ TXMLTEncumbranceZU }

procedure TXMLTEncumbranceZU.AfterConstruction;
begin
  RegisterChildNode('OwnersRestrictionInFavorem', TXMLTOwnerRestrictionInFavorem);
  RegisterChildNode('Duration', TXMLTDuration);
  RegisterChildNode('Registration', TXMLTRegistration);
  RegisterChildNode('Document', TXMLTDocumentWithoutAppliedFile_DocOut3);
  inherited;
end;

function TXMLTEncumbranceZU.Get_Name: UnicodeString;
begin
  Result := ChildNodes['Name'].Text;
end;

procedure TXMLTEncumbranceZU.Set_Name(Value: UnicodeString);
begin
  ChildNodes['Name'].NodeValue := Value;
end;

function TXMLTEncumbranceZU.Get_Type_: UnicodeString;
begin
  Result := ChildNodes['Type'].Text;
end;

procedure TXMLTEncumbranceZU.Set_Type_(Value: UnicodeString);
begin
  ChildNodes['Type'].NodeValue := Value;
end;

function TXMLTEncumbranceZU.Get_AccountNumber: UnicodeString;
begin
  Result := ChildNodes['AccountNumber'].Text;
end;

procedure TXMLTEncumbranceZU.Set_AccountNumber(Value: UnicodeString);
begin
  ChildNodes['AccountNumber'].NodeValue := Value;
end;

function TXMLTEncumbranceZU.Get_CadastralNumberRestriction: UnicodeString;
begin
  Result := ChildNodes['CadastralNumberRestriction'].Text;
end;

procedure TXMLTEncumbranceZU.Set_CadastralNumberRestriction(Value: UnicodeString);
begin
  ChildNodes['CadastralNumberRestriction'].NodeValue := Value;
end;

function TXMLTEncumbranceZU.Get_OwnersRestrictionInFavorem: IXMLTOwnerRestrictionInFavorem;
begin
  Result := ChildNodes['OwnersRestrictionInFavorem'] as IXMLTOwnerRestrictionInFavorem;
end;

function TXMLTEncumbranceZU.Get_Duration: IXMLTDuration;
begin
  Result := ChildNodes['Duration'] as IXMLTDuration;
end;

function TXMLTEncumbranceZU.Get_Registration: IXMLTRegistration;
begin
  Result := ChildNodes['Registration'] as IXMLTRegistration;
end;

function TXMLTEncumbranceZU.Get_Document: IXMLTDocumentWithoutAppliedFile_DocOut3;
begin
  Result := ChildNodes['Document'] as IXMLTDocumentWithoutAppliedFile_DocOut3;
end;

{ TXMLTOwnerRestrictionInFavorem }

procedure TXMLTOwnerRestrictionInFavorem.AfterConstruction;
begin
  RegisterChildNode('OwnerRestrictionInFavorem', TXMLTOwner);
  ItemTag := 'OwnerRestrictionInFavorem';
  ItemInterface := IXMLTOwner;
  inherited;
end;

function TXMLTOwnerRestrictionInFavorem.Get_OwnerRestrictionInFavorem(Index: Integer): IXMLTOwner;
begin
  Result := List[Index] as IXMLTOwner;
end;

function TXMLTOwnerRestrictionInFavorem.Add: IXMLTOwner;
begin
  Result := AddItem(-1) as IXMLTOwner;
end;

function TXMLTOwnerRestrictionInFavorem.Insert(const Index: Integer): IXMLTOwner;
begin
  Result := AddItem(Index) as IXMLTOwner;
end;

{ TXMLTDuration }

function TXMLTDuration.Get_Started: UnicodeString;
begin
  Result := ChildNodes['Started'].Text;
end;

procedure TXMLTDuration.Set_Started(Value: UnicodeString);
begin
  ChildNodes['Started'].NodeValue := Value;
end;

function TXMLTDuration.Get_Stopped: UnicodeString;
begin
  Result := ChildNodes['Stopped'].Text;
end;

procedure TXMLTDuration.Set_Stopped(Value: UnicodeString);
begin
  ChildNodes['Stopped'].NodeValue := Value;
end;

function TXMLTDuration.Get_Term: UnicodeString;
begin
  Result := ChildNodes['Term'].Text;
end;

procedure TXMLTDuration.Set_Term(Value: UnicodeString);
begin
  ChildNodes['Term'].NodeValue := Value;
end;

{ TXMLTEntitySpatialZUOut_Spa2 }

procedure TXMLTEntitySpatialZUOut_Spa2.AfterConstruction;
begin
  RegisterChildNode('SpatialElement', TXMLTSpatialElementZUOut_Spa2);
  ItemTag := 'SpatialElement';
  ItemInterface := IXMLTSpatialElementZUOut_Spa2;
  inherited;
end;

function TXMLTEntitySpatialZUOut_Spa2.Get_EntSys: UnicodeString;
begin
  Result := AttributeNodes['EntSys'].Text;
end;

procedure TXMLTEntitySpatialZUOut_Spa2.Set_EntSys(Value: UnicodeString);
begin
  SetAttribute('EntSys', Value);
end;

function TXMLTEntitySpatialZUOut_Spa2.Get_SpatialElement(Index: Integer): IXMLTSpatialElementZUOut_Spa2;
begin
  Result := List[Index] as IXMLTSpatialElementZUOut_Spa2;
end;

function TXMLTEntitySpatialZUOut_Spa2.Add: IXMLTSpatialElementZUOut_Spa2;
begin
  Result := AddItem(-1) as IXMLTSpatialElementZUOut_Spa2;
end;

function TXMLTEntitySpatialZUOut_Spa2.Insert(const Index: Integer): IXMLTSpatialElementZUOut_Spa2;
begin
  Result := AddItem(Index) as IXMLTSpatialElementZUOut_Spa2;
end;

{ TXMLTSpatialElementZUOut_Spa2 }

procedure TXMLTSpatialElementZUOut_Spa2.AfterConstruction;
begin
  RegisterChildNode('SpelementUnit', TXMLTSpelementUnitZUOut_Spa2);
  ItemTag := 'SpelementUnit';
  ItemInterface := IXMLTSpelementUnitZUOut_Spa2;
  inherited;
end;

function TXMLTSpatialElementZUOut_Spa2.Get_SpelementUnit(Index: Integer): IXMLTSpelementUnitZUOut_Spa2;
begin
  Result := List[Index] as IXMLTSpelementUnitZUOut_Spa2;
end;

function TXMLTSpatialElementZUOut_Spa2.Add: IXMLTSpelementUnitZUOut_Spa2;
begin
  Result := AddItem(-1) as IXMLTSpelementUnitZUOut_Spa2;
end;

function TXMLTSpatialElementZUOut_Spa2.Insert(const Index: Integer): IXMLTSpelementUnitZUOut_Spa2;
begin
  Result := AddItem(Index) as IXMLTSpelementUnitZUOut_Spa2;
end;

{ TXMLTSpelementUnitZUOut_Spa2 }

procedure TXMLTSpelementUnitZUOut_Spa2.AfterConstruction;
begin
  RegisterChildNode('Ordinate', TXMLOrdinate_Spa2);
  inherited;
end;

function TXMLTSpelementUnitZUOut_Spa2.Get_TypeUnit: UnicodeString;
begin
  Result := AttributeNodes['TypeUnit'].Text;
end;

procedure TXMLTSpelementUnitZUOut_Spa2.Set_TypeUnit(Value: UnicodeString);
begin
  SetAttribute('TypeUnit', Value);
end;

function TXMLTSpelementUnitZUOut_Spa2.Get_SuNmb: LongWord;
begin
  Result := AttributeNodes['SuNmb'].NodeValue;
end;

procedure TXMLTSpelementUnitZUOut_Spa2.Set_SuNmb(Value: LongWord);
begin
  SetAttribute('SuNmb', Value);
end;

function TXMLTSpelementUnitZUOut_Spa2.Get_Ordinate: IXMLOrdinate_Spa2;
begin
  Result := ChildNodes['Ordinate'] as IXMLOrdinate_Spa2;
end;

{ TXMLTOrdinateOut_Spa2 }

function TXMLTOrdinateOut_Spa2.Get_X: UnicodeString;
begin
  Result := AttributeNodes[WideString('X')].Text;
end;

procedure TXMLTOrdinateOut_Spa2.Set_X(Value: UnicodeString);
begin
  SetAttribute(WideString('X'), Value);
end;

function TXMLTOrdinateOut_Spa2.Get_Y: UnicodeString;
begin
  Result := AttributeNodes[WideString('Y')].Text;
end;

procedure TXMLTOrdinateOut_Spa2.Set_Y(Value: UnicodeString);
begin
  SetAttribute(WideString('Y'), Value);
end;

function TXMLTOrdinateOut_Spa2.Get_OrdNmb: LongWord;
begin
  Result := AttributeNodes['OrdNmb'].NodeValue;
end;

procedure TXMLTOrdinateOut_Spa2.Set_OrdNmb(Value: LongWord);
begin
  SetAttribute('OrdNmb', Value);
end;

function TXMLTOrdinateOut_Spa2.Get_NumGeopoint: LongWord;
begin
  Result := AttributeNodes['NumGeopoint'].NodeValue;
end;

procedure TXMLTOrdinateOut_Spa2.Set_NumGeopoint(Value: LongWord);
begin
  SetAttribute('NumGeopoint', Value);
end;

function TXMLTOrdinateOut_Spa2.Get_DeltaGeopoint: UnicodeString;
begin
  Result := AttributeNodes['DeltaGeopoint'].Text;
end;

procedure TXMLTOrdinateOut_Spa2.Set_DeltaGeopoint(Value: UnicodeString);
begin
  SetAttribute('DeltaGeopoint', Value);
end;

{ TXMLOrdinate_Spa2 }

function TXMLOrdinate_Spa2.Get_GeopointZacrep: UnicodeString;
begin
  Result := AttributeNodes['GeopointZacrep'].Text;
end;

procedure TXMLOrdinate_Spa2.Set_GeopointZacrep(Value: UnicodeString);
begin
  SetAttribute('GeopointZacrep', Value);
end;

{ TXMLTParcel_Contours }

procedure TXMLTParcel_Contours.AfterConstruction;
begin
  RegisterChildNode('Contour', TXMLTContour);
  ItemTag := 'Contour';
  ItemInterface := IXMLTContour;
  inherited;
end;

function TXMLTParcel_Contours.Get_Contour(Index: Integer): IXMLTContour;
begin
  Result := List[Index] as IXMLTContour;
end;

function TXMLTParcel_Contours.Add: IXMLTContour;
begin
  Result := AddItem(-1) as IXMLTContour;
end;

function TXMLTParcel_Contours.Insert(const Index: Integer): IXMLTContour;
begin
  Result := AddItem(Index) as IXMLTContour;
end;

{ TXMLTContour }

procedure TXMLTContour.AfterConstruction;
begin
  RegisterChildNode('Area', TXMLTAreaWithoutInaccuracyOut);
  RegisterChildNode('EntitySpatial', TXMLTEntitySpatialZUOut_Spa2);
  inherited;
end;

function TXMLTContour.Get_NumberRecord: LongWord;
begin
  Result := AttributeNodes['NumberRecord'].NodeValue;
end;

procedure TXMLTContour.Set_NumberRecord(Value: LongWord);
begin
  SetAttribute('NumberRecord', Value);
end;

function TXMLTContour.Get_Area: IXMLTAreaWithoutInaccuracyOut;
begin
  Result := ChildNodes['Area'] as IXMLTAreaWithoutInaccuracyOut;
end;

function TXMLTContour.Get_EntitySpatial: IXMLTEntitySpatialZUOut_Spa2;
begin
  Result := ChildNodes['EntitySpatial'] as IXMLTEntitySpatialZUOut_Spa2;
end;

{ TXMLTParcel_CompositionEZ }

procedure TXMLTParcel_CompositionEZ.AfterConstruction;
begin
  RegisterChildNode('EntryParcel', TXMLTEntryParcel);
  ItemTag := 'EntryParcel';
  ItemInterface := IXMLTEntryParcel;
  inherited;
end;

function TXMLTParcel_CompositionEZ.Get_EntryParcel(Index: Integer): IXMLTEntryParcel;
begin
  Result := List[Index] as IXMLTEntryParcel;
end;

function TXMLTParcel_CompositionEZ.Add: IXMLTEntryParcel;
begin
  Result := AddItem(-1) as IXMLTEntryParcel;
end;

function TXMLTParcel_CompositionEZ.Insert(const Index: Integer): IXMLTEntryParcel;
begin
  Result := AddItem(Index) as IXMLTEntryParcel;
end;

{ TXMLTEntryParcel }

procedure TXMLTEntryParcel.AfterConstruction;
begin
  RegisterChildNode('Area', TXMLTAreaWithoutInaccuracyOut);
  RegisterChildNode('EntitySpatial', TXMLTEntitySpatialZUOut_Spa2);
  inherited;
end;

function TXMLTEntryParcel.Get_CadastralNumber: UnicodeString;
begin
  Result := AttributeNodes['CadastralNumber'].Text;
end;

procedure TXMLTEntryParcel.Set_CadastralNumber(Value: UnicodeString);
begin
  SetAttribute('CadastralNumber', Value);
end;

function TXMLTEntryParcel.Get_Area: IXMLTAreaWithoutInaccuracyOut;
begin
  Result := ChildNodes['Area'] as IXMLTAreaWithoutInaccuracyOut;
end;

function TXMLTEntryParcel.Get_EntitySpatial: IXMLTEntitySpatialZUOut_Spa2;
begin
  Result := ChildNodes['EntitySpatial'] as IXMLTEntitySpatialZUOut_Spa2;
end;

{ TXMLTParcel_Encumbrances }

procedure TXMLTParcel_Encumbrances.AfterConstruction;
begin
  RegisterChildNode('Encumbrance', TXMLTEncumbranceZU);
  ItemTag := 'Encumbrance';
  ItemInterface := IXMLTEncumbranceZU;
  inherited;
end;

function TXMLTParcel_Encumbrances.Get_Encumbrance(Index: Integer): IXMLTEncumbranceZU;
begin
  Result := List[Index] as IXMLTEncumbranceZU;
end;

function TXMLTParcel_Encumbrances.Add: IXMLTEncumbranceZU;
begin
  Result := AddItem(-1) as IXMLTEncumbranceZU;
end;

function TXMLTParcel_Encumbrances.Insert(const Index: Integer): IXMLTEncumbranceZU;
begin
  Result := AddItem(Index) as IXMLTEncumbranceZU;
end;

{ TXMLTCadastralCost_Cos1 }

function TXMLTCadastralCost_Cos1.Get_Value: UnicodeString;
begin
  Result := AttributeNodes['Value'].Text;
end;

procedure TXMLTCadastralCost_Cos1.Set_Value(Value: UnicodeString);
begin
  SetAttribute('Value', Value);
end;

function TXMLTCadastralCost_Cos1.Get_Unit_: UnicodeString;
begin
  Result := AttributeNodes['Unit'].Text;
end;

procedure TXMLTCadastralCost_Cos1.Set_Unit_(Value: UnicodeString);
begin
  SetAttribute('Unit', Value);
end;

{ TXMLTCadastralNumberOut }

function TXMLTCadastralNumberOut.Get_CadastralNumber: UnicodeString;
begin
  Result := ChildNodes['CadastralNumber'].Text;
end;

procedure TXMLTCadastralNumberOut.Set_CadastralNumber(Value: UnicodeString);
begin
  ChildNodes['CadastralNumber'].NodeValue := Value;
end;

{ TXMLTCertificationDoc_Cer1 }

procedure TXMLTCertificationDoc_Cer1.AfterConstruction;
begin
  RegisterChildNode('Official', TXMLOfficial_Cer1);
  inherited;
end;

function TXMLTCertificationDoc_Cer1.Get_Organization: UnicodeString;
begin
  Result := ChildNodes['Organization'].Text;
end;

procedure TXMLTCertificationDoc_Cer1.Set_Organization(Value: UnicodeString);
begin
  ChildNodes['Organization'].NodeValue := Value;
end;

function TXMLTCertificationDoc_Cer1.Get_Date: UnicodeString;
begin
  Result := ChildNodes['Date'].Text;
end;

procedure TXMLTCertificationDoc_Cer1.Set_Date(Value: UnicodeString);
begin
  ChildNodes['Date'].NodeValue := Value;
end;

function TXMLTCertificationDoc_Cer1.Get_Number: UnicodeString;
begin
  Result := ChildNodes['Number'].Text;
end;

procedure TXMLTCertificationDoc_Cer1.Set_Number(Value: UnicodeString);
begin
  ChildNodes['Number'].NodeValue := Value;
end;

function TXMLTCertificationDoc_Cer1.Get_Official: IXMLOfficial_Cer1;
begin
  Result := ChildNodes['Official'] as IXMLOfficial_Cer1;
end;

{ TXMLOfficial_Cer1 }

function TXMLOfficial_Cer1.Get_Appointment: UnicodeString;
begin
  Result := ChildNodes['Appointment'].Text;
end;

procedure TXMLOfficial_Cer1.Set_Appointment(Value: UnicodeString);
begin
  ChildNodes['Appointment'].NodeValue := Value;
end;

function TXMLOfficial_Cer1.Get_FamilyName: UnicodeString;
begin
  Result := ChildNodes['FamilyName'].Text;
end;

procedure TXMLOfficial_Cer1.Set_FamilyName(Value: UnicodeString);
begin
  ChildNodes['FamilyName'].NodeValue := Value;
end;

function TXMLOfficial_Cer1.Get_FirstName: UnicodeString;
begin
  Result := ChildNodes['FirstName'].Text;
end;

procedure TXMLOfficial_Cer1.Set_FirstName(Value: UnicodeString);
begin
  ChildNodes['FirstName'].NodeValue := Value;
end;

function TXMLOfficial_Cer1.Get_Patronymic: UnicodeString;
begin
  Result := ChildNodes['Patronymic'].Text;
end;

procedure TXMLOfficial_Cer1.Set_Patronymic(Value: UnicodeString);
begin
  ChildNodes['Patronymic'].NodeValue := Value;
end;

{ TXMLKPZU_Contractors }

procedure TXMLKPZU_Contractors.AfterConstruction;
begin
  RegisterChildNode('Contractor', TXMLKPZU_Contractors_Contractor);
  ItemTag := 'Contractor';
  ItemInterface := IXMLKPZU_Contractors_Contractor;
  inherited;
end;

function TXMLKPZU_Contractors.Get_Contractor(Index: Integer): IXMLKPZU_Contractors_Contractor;
begin
  Result := List[Index] as IXMLKPZU_Contractors_Contractor;
end;

function TXMLKPZU_Contractors.Add: IXMLKPZU_Contractors_Contractor;
begin
  Result := AddItem(-1) as IXMLKPZU_Contractors_Contractor;
end;

function TXMLKPZU_Contractors.Insert(const Index: Integer): IXMLKPZU_Contractors_Contractor;
begin
  Result := AddItem(Index) as IXMLKPZU_Contractors_Contractor;
end;

{ TXMLTEngineerOut }

procedure TXMLTEngineerOut.AfterConstruction;
begin
  RegisterChildNode('Organization', TXMLTOrganizationNameOut);
  inherited;
end;

function TXMLTEngineerOut.Get_FamilyName: UnicodeString;
begin
  Result := ChildNodes['FamilyName'].Text;
end;

procedure TXMLTEngineerOut.Set_FamilyName(Value: UnicodeString);
begin
  ChildNodes['FamilyName'].NodeValue := Value;
end;

function TXMLTEngineerOut.Get_FirstName: UnicodeString;
begin
  Result := ChildNodes['FirstName'].Text;
end;

procedure TXMLTEngineerOut.Set_FirstName(Value: UnicodeString);
begin
  ChildNodes['FirstName'].NodeValue := Value;
end;

function TXMLTEngineerOut.Get_Patronymic: UnicodeString;
begin
  Result := ChildNodes['Patronymic'].Text;
end;

procedure TXMLTEngineerOut.Set_Patronymic(Value: UnicodeString);
begin
  ChildNodes['Patronymic'].NodeValue := Value;
end;

function TXMLTEngineerOut.Get_NCertificate: UnicodeString;
begin
  Result := ChildNodes['NCertificate'].Text;
end;

procedure TXMLTEngineerOut.Set_NCertificate(Value: UnicodeString);
begin
  ChildNodes['NCertificate'].NodeValue := Value;
end;

function TXMLTEngineerOut.Get_Organization: IXMLTOrganizationNameOut;
begin
  Result := ChildNodes['Organization'] as IXMLTOrganizationNameOut;
end;

{ TXMLTOrganizationNameOut }

function TXMLTOrganizationNameOut.Get_Name: UnicodeString;
begin
  Result := ChildNodes['Name'].Text;
end;

procedure TXMLTOrganizationNameOut.Set_Name(Value: UnicodeString);
begin
  ChildNodes['Name'].NodeValue := Value;
end;

{ TXMLKPZU_Contractors_Contractor }

function TXMLKPZU_Contractors_Contractor.Get_Date: UnicodeString;
begin
  Result := AttributeNodes['Date'].Text;
end;

procedure TXMLKPZU_Contractors_Contractor.Set_Date(Value: UnicodeString);
begin
  SetAttribute('Date', Value);
end;

{ TXMLTCoordSystems_Spa2 }

procedure TXMLTCoordSystems_Spa2.AfterConstruction;
begin
  RegisterChildNode('CoordSystem', TXMLTCoordSystem_Spa2);
  ItemTag := 'CoordSystem';
  ItemInterface := IXMLTCoordSystem_Spa2;
  inherited;
end;

function TXMLTCoordSystems_Spa2.Get_CoordSystem(Index: Integer): IXMLTCoordSystem_Spa2;
begin
  Result := List[Index] as IXMLTCoordSystem_Spa2;
end;

function TXMLTCoordSystems_Spa2.Add: IXMLTCoordSystem_Spa2;
begin
  Result := AddItem(-1) as IXMLTCoordSystem_Spa2;
end;

function TXMLTCoordSystems_Spa2.Insert(const Index: Integer): IXMLTCoordSystem_Spa2;
begin
  Result := AddItem(Index) as IXMLTCoordSystem_Spa2;
end;

{ TXMLTCoordSystem_Spa2 }

function TXMLTCoordSystem_Spa2.Get_Name: UnicodeString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLTCoordSystem_Spa2.Set_Name(Value: UnicodeString);
begin
  SetAttribute('Name', Value);
end;

function TXMLTCoordSystem_Spa2.Get_CsId: UnicodeString;
begin
  Result := AttributeNodes['CsId'].Text;
end;

procedure TXMLTCoordSystem_Spa2.Set_CsId(Value: UnicodeString);
begin
  SetAttribute('CsId', Value);
end;

end.