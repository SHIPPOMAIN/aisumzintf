unit readerXML.tTestReader;

interface
uses
 KPZU_v05_,
 AISUMZDB.Types,
 System.SysUtils ,
 System.Variants,
 Xml.XMLIntf;

 const
   cNS = 'urn://x-artefacts-rosreestr-ru/commons/complex-types/address-output/3.0.1';

 type
 tTestReader = record
  class function doRead(const aPathFile: string): tParcel; static;
 end;

 KP05Parser = record
 private
  fXML: IXMLKPZU;
 public
  procedure OpenDocument(const pPath: string);
  procedure closeDocument;
  function ParseParcel: tParcel;
  function ParseRight: TArray<TParcelRight>;
  function ParseArenda: TArray<TParcelRight>;
 end;

implementation

{ tTestReader }


class function tTestReader.doRead(const aPathFile: string): tParcel;
var
 _xml: IXMLKPZU;
 _add: IXMLTAddressOut_adrOut3;
begin
 {_xml:= LoadKPZU(aPathFile)   ;
 _xml.DeclareNamespace('ns3','urn://x-artefacts-rosreestr-ru/commons/complex-types/address-output/3.0.1');
 Result.Clear;
 Result.zu_cad_num:= _xml.Parcel.CadastralNumber;
 Result.Area:= tArea.Construct(_xml.Parcel.Area.Area,0, _xml.Parcel.Area.Unit_);
 Result.CadCost := tCadCost.Construct(strtofloat(_xml.Parcel.CadastralCost.Value),0,_xml.Parcel.CadastralCost.Unit_);
 Result.Category := tCategory.Construct(0,_xml.Parcel.Category);
 _add:=_xml.Parcel.Location.Address;
 _add.SetAttributeNS('xmlns','ns3','urn://x-artefacts-rosreestr-ru/commons/complex-types/address-output/3.0.1');
 Result.Address := tAddress.Construct(0,_add.KLADR,_add.Note);
 Result.USe_ := tUse.Construct(0,_xml.Parcel.Utilization.Utilization, _xml.Parcel.Utilization.ByDoc);

 _xml:= nil;  }
end;

{ KP05Parser }

procedure KP05Parser.closeDocument;
begin
  Self.fXML := nil;
end;

procedure KP05Parser.OpenDocument(const pPath: string);
begin
//  try
    self.fXML:= LoadKPZU(pPath)   ;
//  except  on E:Exception do

//  end;
end;

function MsXml_ReadChildNodeValue(const Node: IXMLNode;  ChildName: string; cNS:string=''): string;
var
  _node: IXMLNode;
begin
  _node := Node.ChildNodes.FindNode(ChildName, cNS);
  if (_node = nil) or (_node.NodeValue = Null) then
    Result := ''
  else
    Result := _node.NodeValue
end;

function MsXml_ReadChildAttributeValue(const Node: IXMLNode;  ChildName: string; cNS:string=''): string;
var
  _node: IXMLNode;
begin
 { _node := Node.AttributeNodes.FindNode[ChildName];
  if (_node = nil) or (_node.NodeValue = Null) then
    Result := ''
  else
    Result := _node.NodeValue        }
end;

function changeDate(dd:string):string;
var s1,s2,s3: string;
begin
  Result := copy(dd,9,2)+'.'+copy(dd,6,2)+'.'+Copy(dd,1,4);
end;

function KP05Parser.ParseArenda: TArray<TParcelRight>;
var i : Integer;
_bUfrs : Boolean;
_doc : TDoc;
_owner : tOwner;
_owCount : Integer;
_owName : string;
_owType : ShortInt;
_sNum, _sDenum : Integer;
_ss:string;
_Node : IXMLNode;
_ar : Double;
_d1 : tDate;
begin
  Result := nil;
  SetLength(Result,fXML.Parcel.Encumbrances.Count);
  for I := 0 to fXML.Parcel.Encumbrances.Count-1 do
    begin
//      Result[i].Clear;
      _owner.Clear;
      _doc.Clear;
     if fXML.Parcel.Encumbrances.Encumbrance[i].Type_ = '022006000000' then
     begin

        if fXML.Parcel.Encumbrances.Encumbrance[i].Registration.RegNumber <> '' then _bUfrs := True else _bUfrs := false;
        _owCount := fXML.Parcel.Encumbrances.Encumbrance[i].OwnersRestrictionInFavorem.Count;
        if _owCount =1 then
          begin
            if fXML.Parcel.Encumbrances.Encumbrance[i].OwnersRestrictionInFavorem.OwnerRestrictionInFavorem[0].Governance.Name <> ''
              then
                begin
                  _owName := fXML.Parcel.Encumbrances.Encumbrance[i].OwnersRestrictionInFavorem.OwnerRestrictionInFavorem[0].Governance.Name;
                  _owType := 3;
                end
            else
            if fXML.Parcel.Encumbrances.Encumbrance[i].OwnersRestrictionInFavorem.OwnerRestrictionInFavorem[0].Organization.Name <> ''
              then
                begin
                  _owName := fXML.Parcel.Encumbrances.Encumbrance[i].OwnersRestrictionInFavorem.OwnerRestrictionInFavorem[0].Organization.Name;
                  _owType := 2;
                end
            else
            if fXML.Parcel.Encumbrances.Encumbrance[i].OwnersRestrictionInFavorem.OwnerRestrictionInFavorem[0].Person.FirstName <> ''
              then
                begin
                 _owName := fXML.Parcel.Encumbrances.Encumbrance[i].OwnersRestrictionInFavorem.OwnerRestrictionInFavorem[0].Person.FamilyName + ' ' + fXML.Parcel.Encumbrances.Encumbrance[i].OwnersRestrictionInFavorem.OwnerRestrictionInFavorem[0].Person.FirstName + ' ' +  fXML.Parcel.Encumbrances.Encumbrance[i].OwnersRestrictionInFavorem.OwnerRestrictionInFavorem[0].Person.Patronymic;
                 _owType := 1;
                end;
          end
        else
          if _owCount > 1 then
            begin
              _owType := 1;  //  ����� ������� ��������� � ������������ ���������� �������������
            end;
       // _ss :=  MsXml_ReadChildNodeValue(fXML.Parcel.Encumbrances.Encumbrance[i].Share,'Numerator');
       // if _ss <> ''
       //    then  _sNum := StrToInt(_ss)
       //    else  _sNum := 1;
        //_ss :=  MsXml_ReadChildNodeValue(fXML.Parcel.Rights.Right[i].Share,'Denominator');
       // if _ss <> ''
       //     then  _sDeNum := StrToInt(_ss)
       //     else  _sDeNum := 1;
        _d1 := StrToDate(changeDate(MsXml_ReadChildNodeValue(fXML.Parcel.Encumbrances.Encumbrance[i].Registration,'RegDate')));   //strtodate(MsXml_ReadChildNodeValue(fXML.Parcel.Rights.Right[i].Registration,'RegDate'));

        _owner := tOwner.Construct(_owName, _owCount,_owType, 0, 0, '',1, 1 );
        if (fXML.Parcel.Encumbrances.Encumbrance[i].Document.CodeDocument <> '')
        then
          _Doc := tDoc.construct(strtoInt(fXML.Parcel.Encumbrances.Encumbrance[i].Document.Number), fXML.Parcel.Encumbrances.Encumbrance[i].Document.Series, StrToDate(fXML.Parcel.Encumbrances.Encumbrance[i].Document.Date), 0,fXML.Parcel.Encumbrances.Encumbrance[i].Document.CodeDocument, fXML.Parcel.Encumbrances.Encumbrance[i].Document.Name);

        Result[i] := TParcelRight.Construct(0,0,0,fXML.Parcel.Encumbrances.Encumbrance[i].Type_, 0, True, _bUfrs, _d1,_Doc, _Owner);
      end;
    end;

end;

function KP05Parser.ParseParcel: tParcel;
var _kladr, _note:string;

begin
 Result.Clear;
 Result.zu_cad_num:= fxml.Parcel.CadastralNumber;
 //ii := 0;//FindUnitbyCode(fxml.Parcel.Area.Unit_);
 Result.Area:= tArea.Construct(fxml.Parcel.Area.Area,0, fxml.Parcel.Area.Unit_);
 Result.CadCost := tCadCost.Construct(strtofloat(fxml.Parcel.CadastralCost.Value),0,fxml.Parcel.CadastralCost.Unit_);
 Result.Category := tCategory.Construct(0,fxml.Parcel.Category);
 _kladr := MsXml_ReadChildNodeValue(fxml.Parcel.Location.Address,'KLADR',cNS);
 _note  := MsXml_ReadChildNodeValue(fxml.Parcel.Location.Address,'Note',CNS);

 Result.Address := tAddress.Construct(0,_kladr ,_note);
 Result.USe_ := tUse.Construct(0,fxml.Parcel.Utilization.Utilization, fxml.Parcel.Utilization.ByDoc);
end;



function KP05Parser.ParseRight: TArray<TParcelRight>;
var i : Integer;
_bUfrs : Boolean;
_doc : TDoc;
_owner : tOwner;
_owCount : Integer;
_owName : string;
_owType : ShortInt;
_sNum, _sDenum : Integer;
_ss:string;
_Node : IXMLNode;
_ar : Double;
_d1 : tDate;
begin
  Result := nil;
  SetLength(Result,fXML.Parcel.Rights.Count);
  for I := 0 to fXML.Parcel.Rights.Count-1 do
    begin
//      Result[i].Clear;
      _owner.Clear;
      _doc.Clear;

      if fXML.Parcel.Rights.Right[i].Registration.RegNumber <> '' then _bUfrs := True else _bUfrs := false;
      _owCount := fXML.Parcel.Rights.Count;//fXML.Parcel.Rights.Right[i].Owners.Count;
      if _owCount =1 then
        begin
          if fXML.Parcel.Rights.Right[i].Owners.Owner[0].Governance.Name <> ''
            then
              begin
                _owName := fXML.Parcel.Rights.Right[i].Owners.Owner[0].Governance.Name;
                _owType := 3;
              end
          else
          if fXML.Parcel.Rights.Right[i].Owners.Owner[0].Organization.Name <> ''
            then
              begin
                _owName := fXML.Parcel.Rights.Right[i].Owners.Owner[0].Organization.Name;
                _owType := 2;
              end
          else
          if fXML.Parcel.Rights.Right[i].Owners.Owner[0].Person.FirstName <> ''
            then
              begin
               _owName := fXML.Parcel.Rights.Right[i].Owners.Owner[0].Person.FamilyName + ' ' + fXML.Parcel.Rights.Right[i].Owners.Owner[0].Person.FirstName + ' ' +  fXML.Parcel.Rights.Right[i].Owners.Owner[0].Person.Patronymic;
               _owType := 1;
              end;
        end
      else
        if _owCount > 1 then
          begin
            _owType := 1;  //  ����� ������� ��������� � ������������ ���������� �������������
          end;
      _ss :=  MsXml_ReadChildNodeValue(fXML.Parcel.Rights.Right[i].Share,'Numerator');
      if _ss <> ''
         then  _sNum := StrToInt(_ss)
         else  _sNum := 1;
      _ss :=  MsXml_ReadChildNodeValue(fXML.Parcel.Rights.Right[i].Share,'Denominator');
      if _ss <> ''
          then  _sDeNum := StrToInt(_ss)
          else  _sDeNum := 1;
      _d1 := StrToDate(changeDate(MsXml_ReadChildNodeValue(fXML.Parcel.Rights.Right[i].Registration,'RegDate')));   //strtodate(MsXml_ReadChildNodeValue(fXML.Parcel.Rights.Right[i].Registration,'RegDate'));

      _owner := tOwner.Construct(_owName, _owCount,_owType, 0, 0, '',_sNum, _sDeNum );
      if fXML.Parcel.Rights.Right[i].Documents.Count > 0
      then
        _Doc := tDoc.construct(strtoInt(fXML.Parcel.Rights.Right[i].Documents.Document[0].Number), fXML.Parcel.Rights.Right[i].Documents.Document[0].Series, StrToDate(fXML.Parcel.Rights.Right[i].Documents.Document[0].Date), 0,fXML.Parcel.Rights.Right[i].Documents.Document[0].CodeDocument, fXML.Parcel.Rights.Right[i].Documents.Document[0].Name);

      Result[i] := TParcelRight.Construct(0,0,0,fXML.Parcel.Rights.Right[i].Type_, 0, True, _bUfrs, _d1,_Doc, _Owner);
    end;
end;

end.
