object dm1: Tdm1
  OldCreateOrder = False
  Height = 242
  Width = 596
  object ibscrtysrvc1: TIBSecurityService
    ServerName = 'localhost'
    Protocol = TCP
    Params.Strings = (
      'user_name=sysdba'
      'password=superVlands')
    LoginPrompt = False
    TraceFlags = []
    ServerType = 'IBServer'
    UserName = 'SYSDBA'
    Left = 48
    Top = 32
  end
  object ibdtbs1: TIBDatabase
    DefaultTransaction = ibtrnsctn1
    ServerType = 'IBServer'
    Left = 128
    Top = 32
  end
  object ibtrnsctn1: TIBTransaction
    Left = 208
    Top = 32
  end
  object IBDS1: TIBDataSet
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    UniDirectional = False
    Left = 48
    Top = 104
  end
  object IBQUpdateGuid: TIBSQL
    Database = ibdtbs1
    SQL.Strings = (
      'Update T_land'
      'Set Guid = :pG'
      'where cad_num = :pkn')
    Transaction = ibtrnsctn1
    Left = 128
    Top = 104
  end
end
