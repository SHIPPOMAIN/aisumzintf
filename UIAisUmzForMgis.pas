unit UIAisUmzForMgis;

interface
uses IBDatabase;                     //6-4-16
 type
  IAisUMZForMgis = interface(IInterface)
   function GetDB:TIBDatabase ;
   function GetTrAc:TIBTransaction;
   property Mydb : TIBDATABase read GetDB;
   property MyTrAc : TIBTransaction read GetTRAc;
   function Connect(ServerPath,Login,Pass,Role: string; local_prefix : Integer): integer;
   function UpdateGuid(KN:string; GID: Tguid): integer;
   function DisConnect:boolean;
   //function GetMydb:tIBdatabase;     //6-4-16
  end;
implementation

end.
