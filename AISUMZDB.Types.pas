unit AISUMZDB.Types;

interface

type
   tArea = record
   private
    fValue : Double;
    fUnit: Double;
    fUnitCod : string;
    fisUseV: boolean;
    fisUseU: boolean;
    //function getUnit: Double;
    //function getUnitCod: string;
    //function getValue: Double;
    //function getUseV: boolean;
    //function getUseU: boolean;
    procedure setUnit(const Value: Double);
    procedure setUnitCod(const Value: string);
    procedure setValue(const Value: Double);
    procedure setuseV(const Value: boolean);
    procedure setuseU(const Value: boolean);
   public
    procedure Clear;
    property isUseV: boolean read fisUseV write setUseV;
    property isUseU: boolean read fisUseU write setUSEU;
    class function Construct(pValue: Double; pUnit: Double; pUnitCod: string): tArea; static;
    property Value: Double read fValue write setValue;
    property Unit_: Double read fUnit write setUnit;
    property UnitCod: string read fUnitCod write setUnitCod;
   end;

   tAddress = record
     private
     FValue : Double;
     Faddrkladr : string;
     FaddrNote : string;
     fisUseID  : boolean;
     fisUseNote: boolean;
     //function getKladr: string;
     //function getNote: string;
     //function getValue: Double;
    // function getUseId: boolean;
    // function getUseNote: boolean;
     procedure setKladr(const Value: string);
     procedure setNOte(const Value: string);
     procedure setValue(const Value: Double);
     procedure setUseId(const Value: boolean);
     procedure setUseNote(const Value: boolean);
     public
     procedure Clear;
     property isUseID:boolean read fisUseID write setUseId;
     property isUseNote:boolean read fisUseNote write setUseNote;
     class function Construct(pValue: Double; pKladr: string; pNote: string): taddress; static;
     property Value: Double read FValue write setValue;
     property AdrKladr: String read Faddrkladr write setKladr;
     property AdrNote: string read FaddrNote write setNOte;
   end;

   tUse = record
     private
     fValue : Double;
     fcode : string;
     ffact : string;
     fisUseID  : boolean;
     fisUseFact: boolean;

     //function getValue: Double;
     //function getCode: string;
     //function getfact: string;
     //function getUseID: boolean;
     //function getUseFact: boolean;
     procedure setValue(const Value: Double);
     procedure setCode(const Value: string);
     procedure setFact(const Value: string);
     procedure setUSeId(const Value: boolean);
     procedure setUseFact(const Value: boolean);
     public
     procedure Clear;
     property isUseID: boolean read fisUseID write setUSeId;
     property isUsefact: boolean read fisUseFact write setUSefact;
     class function Construct(pValue: Double; pCode: string; pFact: string): tuse; static;
     property Value: Double read fValue write setValue;
     property UseCode: String read fcode write setCode;
     property UseFact: string read ffact write setFact;

   end;

   tCategory = record
     private
     fcode : string;  // ��� �������������?
     FValue : Double;   // ��� �������������?
     fisUse: boolean;
     //function getValue: Double;
     procedure setValue(const Value: Double);
     //function getCode: string;
     procedure setCode(const Value: string);
     //function getUse: boolean;
     procedure setUse(const Value:boolean);
     public
     procedure Clear;
     property isUse: boolean read fisUse write setUse;
     class function Construct(pValue: Double; pCode: string): tcategory; static;
     property Value: Double read FValue write setValue;
     property Code: String read fcode write setCode;
   end;

   tCadCost = record
     private
     fValue : Double;
     fUnit  : Double;
     FUnitCode : string;
     fisUseV: boolean;
     fisUseU1: boolean;

     {function getUnit: Double;
     function getUnitCod: string;
     function getValue: Double;
     function getUseV: boolean;
     function getUseU1: boolean;    }

     procedure setUnit(const Value: Double);
     procedure setUnitCod(const Value: string);
     procedure setValue(const Value: Double);
     procedure setUseV(const Value: boolean);
     procedure setUseU1(const Value: boolean);

     public
     property isUseV: boolean read fisUseV write setUseV;
     property isUseU1: boolean read fisUseU1 write setUseU1;
     procedure Clear;
     class function Construct(pValue: Double; pUnit: Double; pUnitCod: string): tCadCost; static;
     property Value: Double read fValue write setValue;
     property Unit_: Double read fUnit write setUnit;
     property UnitCod: string read FUnitCode write setUnitCod;
   end;

   tParcel = record
     private
      fzu_ID : Double;
      fzu_pred_id: Double;
      fzu_local_prefix : Integer;  // �� ��� ������
      fzu_cad_num : string;

      {fArea: tArea;
      fAddres: tAddress;
      fUse: tUse;
      fCategory : tCategory;
      fCadCost : tCadCost;  }

      fifreadArea : Boolean;
      fIfreadChar : Boolean;
      fIfreadAdr : Boolean;
      fIfreadUse : Boolean;

      {function getArea: tArea;
      procedure setArea(const Value: tArea);
      function getAddress: tAddress;
      procedure setAddress(const Value: tAddress);
      function getUse: tUse;
      procedure setUse(const Value: tUse);
      function getcategory: tCategory;
      procedure setCategory(const Value: tCategory);
      function getCadcost: tCadCost;
      procedure setCadcost(const Value: tCadCost);      }

     // function getzu_id: double;
      procedure setzu_id(const Value: double);
    // function getzu_predid: double;
      procedure setzu_predid(const Value: double);
    //  function getlp: integer;
      procedure setlp(const Value: integer);
    //  function getzu_KN: string;
      procedure setzu_KN(const Value: string);

    //  function getreadArea: boolean;
      procedure setreadArea(const Value: boolean);
   //  function getreadChar: boolean;
      procedure setreadChar(const Value: boolean);
   //   function getreadAdr: boolean;
      procedure setreadAdr(const Value: boolean);
   //   function getreadUse: boolean;
      procedure setreadUse(const Value: boolean);

     public
     Area: tArea;
     Address: tAddress;
     USe_: tUse;
     Category: tCategory;
     CadCost: tCadCost;
     procedure Clear;
     property zu_ID : Double read fzu_ID write setzu_id;
     property zu_pred_id: Double read fzu_pred_id write setzu_predid;
     property zu_local_prefix : Integer read fzu_local_prefix write setlp;  // �� ��� ������
     property zu_cad_num : string read fzu_cad_num write setzu_Kn;

     property ifreadArea:boolean  read fifreadArea write setreadArea;
     property ifreadChar:boolean  read fIfreadChar write setreadChar;
     property ifreadAdr:boolean  read fIfreadAdr write setreadAdr;
     property ifreadUse:boolean  read fIfreadUse write setreadUSe;
     //zu_pred_cad_num : string;  // �� ��� ������

     //property Area: tArea read getArea write setArea;
    { property Address: tAddress read getAddress write setAddress;
     property USe_: tUse read getUse write setUse;
     property Category: tCategory read getcategory write setCategory;
     property CadCost: tCadCost read getCadCost write setCadCost;     }
  end;

   tOwner = record
   private
     fowner : string;
     fownercount : Integer;   // ���� ������������� ���������  - ������� ������ ������
     fowner_type : ShortInt;
     fowner_area : Double;
     fowner_area_unit : Double;
     fowner_area_unit_code : string;
     fowner_drob1 : Integer;
     fowner_drob2 : Integer;

     fisuseowner : Boolean;
  //   fisuseowner_type : Boolean;
     fisuseowner_area : Boolean;
     fisuseowner_area_unit : Boolean;
     fisuseowner_drob : Boolean;

    // function getowner:string;
     procedure setOwner(const Value:string);
    // function getownerCount:integer;
     procedure setOwnerCount(const Value:integer);
   //  function getowner_type:shortint;
     procedure setOwner_type(const Value:shortint);
   //  function getowner_area:double;
     procedure setOwner_area(const Value:double);
   //  function getowner_areaunit:Double;
     procedure setOwner_areaunit(const Value:double);
   //  function getowner_d1:integer;
     procedure setOwner_d1(const Value:integer);
   //  function getowner_d2:Integer;
     procedure setOwner_d2(const Value:integer);
     procedure setarea_unit_code(const Value:string);

     procedure setuseowner(const Value:Boolean);
   //  procedure setuseowner_type(const Value:Boolean);
     procedure setuseowner_area(const Value:Boolean);
     procedure setuseowner_area_unit(const Value:Boolean);
     procedure setuseowner_drob(const Value:Boolean);
   public
     procedure Clear;
     class function Construct(pOwner: string; pOwnerCount: Integer ; powner_type: ShortInt; powner_area : Double; powner_area_unit : Double; powner_area_unit_code:string; powner_drob1 : Integer; powner_drob2 : Integer): tOwner; static;
     property owner                :string    read fowner                write setOwner;
     property ownerCount           :integer   read fownercount           write setOwnerCount;
     property owner_type           :ShortInt  read fowner_type           write setOwner_type;
     property owner_area           :double    read fowner_area           write setOwner_area;
     property owner_area_unit      :double    read fowner_area_unit      write setOwner_areaunit;
     property owner_area_unit_code :string    read fowner_area_unit_code write setarea_unit_code;
     property owner_drob1          :Integer   read fowner_drob1          write setOwner_d1;
     property owner_drob2          :Integer   read fowner_drob2          write setOwner_d2;

     property isuseowner           :Boolean  read fisuseowner           write setuseowner;
 //    property isuseowner_type      :Boolean  read fisuseowner_type      write setuseowner_type;
     property isuseowner_area      :Boolean  read fisuseowner_area      write setuseowner_area;
     property isuseowner_area_unit :Boolean  read fisuseowner_area_unit write setuseowner_area_unit;
     property isuseowner_drob      :Boolean  read fisuseowner_drob      write setuseowner_drob;

  end;

   TDoc = record
    private
      fdoc_num : integer;
      fdoc_seria : string;
      fdoc_date : TDate;
      fdoc_type : double;
      fdoc_code : string;
      fDoc_name : string;

      fisusednum :Boolean;
      fisusedseria :Boolean;
      fisusedndate :Boolean;
      fisusedtype :Boolean;

    //  function getDoc_num: integer;
      procedure setDoc_num(const Value: integer);
    //  function getDoc_Seria: string;
      procedure setDoc_seria(const Value: string);
    //  function getDoc_date: tDate;
      procedure setDoc_date(const Value: tDate);
    //  function getdoc_type: double;
      procedure setdoc_type(const Value: double);
    //  function getdoc_code: string;
      procedure setdoc_code(const Value: string);
    //  function getdoc_name: string;
      procedure setdoc_name(const Value: string);

    //  function getisusednum: Boolean;
      procedure setisusednum(const Value: boolean);
    //  function getisusedseria: Boolean;
      procedure setisusedSeria(const Value: boolean);
    //  function getisusedndate: Boolean;
      procedure setisusednDate(const Value: boolean);
    //  function getisusedtype: Boolean;
      procedure setisusedtype(const Value: boolean);

    public
      procedure Clear;
      class function construct(pNUm : Integer; pseria:string; pdate : TDate; ptype:double; pDoc_code : string; pDoc_name : string):tDoc; static;

      property doc_num  :integer  read fdoc_num   write setDoc_num;
      property doc_seria:string   read fdoc_seria write setDoc_seria;
      property doc_date :tDate    read fdoc_date  write setDoc_date;
      property doc_type :double   read fdoc_type  write setdoc_type;
      property doc_code :string   read fdoc_code  write setdoc_code;
      property doc_name :string   read fDoc_name  write setdoc_name;

      property isusednum  :Boolean  read fisusednum   write setisusednum;
      property isusedseria:Boolean  read fisusedseria write setisusedSeria;
      property isusedndate:Boolean  read fisusedndate write setisusednDate;
      property isusedtype :Boolean  read fisusedtype  write setisusedtype;
  end;

  TParcelRight =record
  private
     fland_id   : Double;
     flright_id : Double;   // ���� ��� �������
     fRight_ID  : Double; // ���� �����

     fright_cod        :string;
     fright_type_id    :Double;
     fright_cad_uchet  :Boolean; // ������ ����� ������� ���������.
     fright_ufrs       :Boolean ;  // �� ������� ����������?
     fdatebegin        :TDate;
     //fowner : tOwner;
     //fDoc : TDoc;

     fisuse_cod        :Boolean;
     fisuse_cad_uchet  :Boolean; // ������ ����� ������� ���������.
     fisuse_ufrs       :Boolean;

     // function getland_id: double;
      procedure setland_id(const Value: double);
     // function getlright_id: double;
      procedure setflright_id(const Value: double);
     // function getright_id: double;
      procedure setright_id(const Value: double);
     // function getright_cod: string;
      procedure setright_cod(const Value: string);
     // function getright_type_id: double;
      procedure setright_type_id(const Value: double);
     // function getright_cad_uchet: Boolean;
      procedure setright_cad_uchet(const Value: Boolean);
     // function getright_ufrs: Boolean;
      procedure setright_ufrs(const Value: Boolean);
      procedure setdatebegin(const Value: TDate);

      procedure setisUse_ufrs(const Value: Boolean);
      procedure setIsuseCaduchet(const Value: Boolean);
      procedure setisUseCode(const Value: Boolean);
     { function getOwner: tOwner;
      procedure setOwner(const Value: tOwner);
      function getDoc: tDoc;
      procedure setDoc(const Value: tDoc);
                                                     }
  public
     owner:towner;
     Doc:tDoc ;
     procedure Clear;
     class function Construct(plid : Double; plrid:Double; prid:Double;prCod:string;prtype:Double;prcaduch:Boolean; prufrs:Boolean;pdatebegin: Tdate; pDoc:TDoc; pOwner:tOwner):TParcelRight; static;
     property land_id         : Double  read fland_id         write setland_id;
     property lright_id       : Double  read flright_id       write setflright_id;
     property Right_ID        : Double  read fRight_ID        write setright_id;
     property right_cod       : string  read fright_cod       write setright_cod;
     property right_type_id   : double  read fright_type_id   write setright_type_id;
     property right_cad_uchet : boolean read fright_cad_uchet write setright_cad_uchet;
     property right_ufrs      : boolean read fright_ufrs      write setright_ufrs;
     property datebegin       : tdate   read fdatebegin       write setdatebegin;

     property isUse_ufrs      : boolean read fisuse_ufrs      write setisUse_ufrs;
     property isUse_cadUch    : boolean read fisuse_cad_uchet write setIsuseCaduchet;
     property isUse_rtype     : boolean read fisuse_cod       write setisUseCode;
    // property owner:towner  read getOwner write setOwner;
    // property Doc:tDoc  read getDoc write setDoc;
  end;

implementation

{ tArea }

procedure tArea.Clear;
begin
 self.fValue:= 0;
 self.fUnit:= 0;
 self.fUnitCod:= '';
 self.isUseU :=false;
 Self.isUseV := false;
end;

class function tArea.Construct(pValue, pUnit: Double; pUnitCod: string): tArea;
begin
 Result.fValue:= pValue;
 Result.fUnit:= pUnit;
 Result.fUnitCod:= pUnitCod;
 if pUnitCod <>'' then Result.fisUseU:= true;
 if pValue <> 0 then Result.fisUseV:= true;
end;

{function tArea.getUnit: Double;
begin
 Result:= self.fUnit;
end;    }

{function tArea.getUnitCod: string;
begin
 Result:= self.fUnitCod;
end;    }

{function tArea.getUseU: boolean;
begin
 Result:= self.fisUseU;
end;  }

{function tArea.getUseV: boolean;
begin
 Result:= self.fisUseV;
end; }

{function tArea.getValue: Double;
begin
 Result:= self.fValue;
end;  }

procedure tArea.setUnit(const Value: Double);
begin
  Self.fUnit:= Value;
  //self.fisUseU:= true;
end;

procedure tArea.setUnitCod(const Value: string);
begin
 self.fUnitCod:= Value;
 if Value <>'' then self.fisUseU:= true;

end;

procedure tArea.setuseU(const Value: boolean);
begin
 self.fisUseU:= Value;
end;

procedure tArea.setuseV(const Value: boolean);
begin
 self.fisUseV:= Value;
end;

procedure tArea.setValue(const Value: Double);
begin
 self.fValue:= Value;
 if Value <> 0 then self.fisUseV:= true;
end;

{ tParcel }

procedure tParcel.Clear;
begin
   self.zu_cad_num :='';
   self.zu_ID := 0;
   self.zu_local_prefix := 0;
   self.zu_pred_id := 0;

   self.ifreadAdr := False;
   self.ifreadArea := False;
   self.ifreadUse := False;
   self.ifreadChar := False;

   self.Area.Clear;
   self.Address.Clear;
   self.CadCost.Clear;
   self.USe_.Clear;
   self.Category.Clear;
end;

{function tParcel.getAddress: tAddress;
begin
 Result:= self.fAddres;
end;

function tParcel.getArea: tArea;
begin
 Result:= self.fArea;
end;

function tParcel.getCadcost: tCadCost;
begin
 Result:= self.fCadCost;
end;

function tParcel.getcategory: tCategory;
begin
 Result:= self.fCategory;
end;    }

{function tParcel.getlp: integer;
begin
 Result := Self.fzu_local_prefix;
end;

function tParcel.getreadAdr: boolean;
begin
 Result := Self.fIfreadAdr;
end;

function tParcel.getreadArea: boolean;
begin
 Result := Self.fifreadArea;
end;

function tParcel.getreadChar: boolean;
begin
 Result := Self.fIfreadChar;
end;

function tParcel.getreadUse: boolean;
begin
 Result := Self.fIfreadUse;
end;     }

{function tParcel.getUse: tUse;
begin
 Result:= self.fUse;
end;     }

{function tParcel.getzu_id: double;
begin
 Result := Self.fzu_ID;
end;

function tParcel.getzu_KN: string;
begin
 Result := Self.fzu_cad_num;
end;

function tParcel.getzu_predid: double;
begin
 Result := Self.fzu_pred_id;
end; }

{procedure tParcel.setAddress(const Value: tAddress);
begin
 self.fAddres:= Value;
end;

procedure tParcel.setArea(const Value: tArea);
begin
 self.fArea:= Value;
end;

procedure tParcel.setCadcost(const Value: tCadCost);
begin
  self.fCadCost:= Value;
end;

procedure tParcel.setCategory(const Value: tCategory);
begin
 self.fCategory:= Value;
end;     }

procedure tParcel.setlp(const Value: integer);
begin
  Self.fzu_local_prefix := Value;
end;

procedure tParcel.setreadAdr(const Value: boolean);
begin
  Self.fIfreadAdr := Value;
end;

procedure tParcel.setreadArea(const Value: boolean);
begin
  Self.fIfreadArea := Value;
end;

procedure tParcel.setreadChar(const Value: boolean);
begin
  Self.fIfreadchar := Value;
end;

procedure tParcel.setreadUse(const Value: boolean);
begin
   Self.fIfreadUse := Value;
end;

{procedure tParcel.setUse(const Value: tUse);
begin
 Self.fUse := Value;
end;  }

procedure tParcel.setzu_id(const Value: double);
begin
  Self.fzu_ID := Value;
end;

procedure tParcel.setzu_KN(const Value: string);
begin
  Self.fzu_cad_num := Value;
end;

procedure tParcel.setzu_predid(const Value: double);
begin
  Self.fzu_pred_id := Value;;
end;

{ tAddress }

procedure tAddress.Clear;
begin
   self.fValue      := 0;
   self.Faddrkladr  := '';
   self.FaddrNote   := '';
   Self.fisUseID    := false;
   Self.fisUseNote  := False;
end;

class function tAddress.Construct(pValue: Double; pKladr, pNote: string): taddress;
begin
   Result.fValue      := pValue;
   Result.Faddrkladr  := pKladr;
   Result.FaddrNote   := pNote;
   if pKladr <> '' then Result.fisUseID :=True;
   if pNote <> '' then Result.fisUseNote :=True;
end;

{function tAddress.getKladr: string;
begin
  Result:= self.Faddrkladr;
end;

function tAddress.getNote: string;
begin
  Result:= self.FaddrNote;
end;

function tAddress.getUseId: boolean;
begin
 Result := Self.fisUseID;
end;

function tAddress.getUseNote: boolean;
begin
 Result := Self.fisUseNote;
end;

function tAddress.getValue: Double;
begin
  Result:= self.FValue;
end;   }

procedure tAddress.setKladr(const Value: string);
begin
 self.Faddrkladr:= Value;
 if Value <> '' then Self.fisUseID :=True;
end;

procedure tAddress.setNOte(const Value: string);
begin
 self.FaddrNote:= Value;
 if Value <> '' then Self.fisUseNote :=True;
end;

procedure tAddress.setUseId(const Value: boolean);
begin
 Self.fisUseID := Value;
end;

procedure tAddress.setUseNote(const Value: boolean);
begin
 Self.fisUseNote := Value;
end;

procedure tAddress.setValue(const Value: Double);
begin
  self.fValue:= Value;
end;

{ tUse }

procedure tUse.Clear;
begin
  self.fValue:= 0;
  self.Fcode:= '';
  self.ffact:= '';
  Self.fisUseID := False;
  Self.fisUsefact :=False;

end;

class function tUse.Construct(pValue: Double; pCode, pFact: string): tuse;
begin
  Result.fValue:= pValue;
  Result.Fcode:= pcode;
  Result.ffact:= pfact;

  if pFact <> '' then Result.fisUseID :=True;
  if pFact <> '' then Result.fisUseFact :=True;
end;

{function tUse.getCode: string;
begin
 Result:= self.fcode;
end;

function tUse.getfact: string;
begin
 Result:= self.ffact;
end;

function tUse.getUseFact: boolean;
begin
 Result := Self.fisUsefact;
end;

function tUse.getUseID: boolean;
begin
 Result := Self.fisUseID;
end;

function tUse.getValue: Double;
begin
 Result:= self.FValue;
end;   }

procedure tUse.setCode(const Value: string);
begin
 self.fcode:= Value;
 if Value <> '' then Self.fisUseID :=True;
end;

procedure tUse.setFact(const Value: string);
begin
 self.ffact:= Value;
  if Value <> '' then Self.fisUseFact :=True;
end;


procedure tUse.setUseFact(const Value: boolean);
begin
 Self.fisUseFact := Value;
end;

procedure tUse.setUSeId(const Value: boolean);
begin
 Self.fisUseID := Value;
end;

procedure tUse.setValue(const Value: Double);
begin
  self.fValue:= Value;
end;

{ tCategory }

procedure tCategory.Clear;
begin
  self.FValue := 0;
  self.fcode  := '';
  self.fisUse :=False;
end;

class function tCategory.Construct(pValue: Double; pCode: string): tcategory;
begin
  Result.FValue := pValue;
  Result.fcode  := pCode;
  if pCode <> '' then Result.fisUse := True;
end;

{function tCategory.getCode: string;
begin
  Result:= self.fcode;
end;

function tCategory.getUse: boolean;
begin
 Result := Self.fisUse;
end;

function tCategory.getValue: Double;
begin
 Result:= self.FValue;
end;        }

procedure tCategory.setCode(const Value: string);
begin
  self.fcode:= Value;
  if Value <> '' then Self.fisUse := True;
end;

procedure tCategory.setUse(const Value: boolean);
begin
 Self.fisUse := Value;
end;

procedure tCategory.setValue(const Value: Double);
begin
  self.fValue:= Value;
end;

{ tCadCost }

procedure tCadCost.Clear;
begin
  self.fValue := 0;
  self.fUnit := 0;
  self.FUnitCode := '';
  self.fisUseU1 := False;
  self.fisUseV :=False;
end;

class function tCadCost.Construct(pValue, pUnit: Double; pUnitCod: string): tCadCost;
begin
  Result.fValue := pValue;
  Result.fUnit := pUnit;
  Result.FUnitCode := pUnitCod;
  if pUnitCod <> '' then Result.fisUseU1 := True;
  if pValue <> 0 then Result.fisUseV := True;
end;

{function tCadCost.getUnit: Double;
begin
 Result := Self.fUnit;
end;

function tCadCost.getUnitCod: string;
begin
 Result:= self.FUnitCode;
end;

function tCadCost.getUseU1: boolean;
begin
 Result:= self.fisUseU1;
end;


function tCadCost.getUseV: boolean;
begin
  Result:= self.fisUseV;
end;

function tCadCost.getValue: Double;
begin
 Result:= self.FValue;
end;       }

procedure tCadCost.setUnit(const Value: Double);
begin
  self.FUnit:= Value;
end;

procedure tCadCost.setUnitCod(const Value: string);
begin
  self.FUnitCode:= Value;
  if Value <> '' then Self.fisUseU1 := True;
end;

procedure tCadCost.setUseU1(const Value: boolean);
begin
 self.fisUseU1:= Value;
end;

procedure tCadCost.setUseV(const Value: boolean);
begin
 self.fisUseU1:= Value;
end;

procedure tCadCost.setValue(const Value: Double);
begin
 self.fValue:= Value;
 if Value <> 0 then Self.fisUseV := True;
end;

{ TParselRight }

procedure TParcelRight.Clear;
begin
 self.fland_id :=0;
 self.flright_id :=0;
 self.fRight_ID  :=0;
 self.fright_cod :='';
 self.fright_type_id :=0;
 self.fright_cad_uchet :=False;
 self.fright_ufrs      :=False;
 self.fdatebegin := 0;
 self.owner.Clear;
 self.Doc.Clear;
 Self.fisuse_cad_uchet := False;
 Self.fisuse_ufrs   := False;
 Self.fisuse_cod    := False;
end;

class function TParcelRight.Construct(plid ,plrid, prid: Double; prCod: string;  prtype: Double; prcaduch, prufrs: Boolean; pdatebegin: Tdate ;pDoc:TDoc; pOwner:tOwner): TParcelRight;
begin
 Result.fland_id :=plid;
 Result.flright_id :=plrid;
 Result.fRight_ID  :=prid;
 Result.fright_cod :=prCod;
 Result.fright_type_id :=prtype;
 Result.fright_cad_uchet :=prcaduch;
 Result.fright_ufrs      :=prufrs;
 Result.fdatebegin   :=pdatebegin;

 Result.owner:=pOwner;
 Result.Doc:=pDoc;

 if prufrs   then  Result.fisuse_ufrs :=True;
 if prcaduch then  Result.fisuse_cad_uchet :=True;
 if prCod <> '' then  Result.fisuse_cod :=True;
end;

{function TParcelRight.getDoc: tDoc;
begin
  Result:= self.fDoc;
end;

function TParcelRight.getland_id: double;
begin
  Result := Self.fland_id;
end;

function TParcelRight.getlright_id: double;
begin
  Result:= self.flright_id;
end;

function TParcelRight.getOwner: tOwner;
begin
 Result := Self.fowner;
end;

function TParcelRight.getright_cad_uchet: Boolean;
begin
  Result:= self.fright_cad_uchet;
end;

function TParcelRight.getright_cod: string;
begin
 Result:= self.fright_cod;
end;

function TParcelRight.getright_id: double;
begin
 Result:= self.fRight_ID;
end;

function TParcelRight.getright_type_id: double;
begin
 Result:= self.fright_type_id;
end;

function TParcelRight.getright_ufrs: Boolean;
begin
 Result:= self.fright_ufrs;
end;                                               }

{procedure TParcelRight.setDoc(const Value: tDoc);
begin
 Self.fDoc := Value;
end;                                           }

procedure TParcelRight.setdatebegin(const Value: TDate);
begin
 Self.fdatebegin := Value;
end;

procedure TParcelRight.setflright_id(const Value: double);
begin
 Self.flright_id := Value;
end;

procedure TParcelRight.setIsuseCaduchet(const Value: Boolean);
begin
 Self.fisuse_cad_uchet := Value;
end;

procedure TParcelRight.setisUseCode(const Value: Boolean);
begin
Self.fisuse_cod := Value;
end;

procedure TParcelRight.setisUse_ufrs(const Value: Boolean);
begin
Self.fisuse_ufrs := Value;
end;

procedure TParcelRight.setland_id(const Value: double);
begin
 Self.fland_id := Value;
end;

{procedure TParcelRight.setOwner(const Value: tOwner);
begin
 Self.fowner := Value;
end;                                              }

procedure TParcelRight.setright_cad_uchet(const Value: Boolean);
begin
 Self.fright_cad_uchet := Value;
end;

procedure TParcelRight.setright_cod(const Value: string);
begin
 Self.fright_cod := Value;
end;

procedure TParcelRight.setright_id(const Value: double);
begin
 Self.fRight_ID := Value;
end;

procedure TParcelRight.setright_type_id(const Value: double);
begin
 Self.fright_type_id := Value;
end;

procedure TParcelRight.setright_ufrs(const Value: Boolean);
begin
 Self.fright_ufrs := Value;
end;

{ tOwner }

procedure tOwner.Clear;
begin
  self.fowner := '';
  self.fownercount := 0;
  self.fowner_area := 0;
  self.fowner_area_unit := 0;
  self.fowner_area_unit_code := '';
  self.fowner_type := 0;
  self.owner_drob1 := 0;
  self.owner_drob2 := 0;
end;

class function tOwner.Construct(pOwner: string; pOwnerCount: Integer ;powner_type: ShortInt;  powner_area, powner_area_unit: Double; powner_area_unit_code:string; powner_drob1,  powner_drob2: Integer): tOwner;
begin
  Result.fowner                := pOwner;
  Result.fownercount           := pOwnerCount;
  Result.fowner_area           := powner_area;
  Result.fowner_area_unit      := powner_area_unit;
  Result.fowner_area_unit_code := powner_area_unit_code;
  Result.fowner_type           := powner_type;
  Result.owner_drob1           := powner_drob1;
  Result.owner_drob2           := powner_drob2;
  if pOwner <> ''                then Result.fisuseowner := true;
  if powner_area <> 0            then Result.fisuseowner_area := true;
  if powner_area_unit_code <> '' then Result.fisuseowner_area_unit := true;
  if powner_drob1 <> 0           then Result.fisuseowner_drob := true;

end;

{function tOwner.getowner: string;
begin
 Result := Self.fowner;
end;

function tOwner.getownerCount: integer;
begin
 Result := Self.fownercount;
end;

function tOwner.getowner_area: double;
begin
 Result := Self.fowner_area;
end;

function tOwner.getowner_areaunit: Double;
begin
 Result := Self.fowner_area_unit;
end;

function tOwner.getowner_d1: integer;
begin
 Result := Self.fowner_drob1;
end;

function tOwner.getowner_d2: Integer;
begin
 Result := Self.fowner_drob2;
end;

function tOwner.getowner_type: shortint;
begin
 Result := Self.fowner_type;
end;                                   }

procedure tOwner.setarea_unit_code(const Value: string);
begin
 Self.fowner_area_unit_code := Value;
end;

procedure tOwner.setOwner(const Value: string);
begin
 Self.fowner := Value;
 if  Value <> '' then Self.fisuseowner := True;
end;

procedure tOwner.setOwnerCount(const Value: integer);
begin
 Self.fownercount := Value;
end;

procedure tOwner.setOwner_area(const Value: double);
begin
 Self.fowner_area := Value;
  if  Value <> 0 then Self.fisuseowner_area := True;
end;

procedure tOwner.setOwner_areaunit(const Value: double);
begin
 Self.fowner_area_unit := Value;
  if  Value <> 0 then Self.fisuseowner_area_unit := True;
end;

procedure tOwner.setOwner_d1(const Value: integer);
begin
 Self.fowner_drob1 := Value;
  if  Value <> 0 then Self.fisuseowner_drob := True;
end;

procedure tOwner.setOwner_d2(const Value: integer);
begin
 Self.fowner_drob2 := Value;
end;

procedure tOwner.setOwner_type(const Value: shortint);
begin
 Self.fowner_type := Value;
end;

procedure tOwner.setuseowner(const Value: Boolean);
begin
 Self.fisuseowner := Value;
end;

procedure tOwner.setuseowner_area(const Value: Boolean);
begin
  Self.fisuseowner_area  := Value;
end;

procedure tOwner.setuseowner_area_unit(const Value: Boolean);
begin
  Self.fisuseowner_area_unit := Value;
end;

procedure tOwner.setuseowner_drob(const Value: Boolean);
begin
  Self.fisuseowner_drob := Value;
end;

{procedure tOwner.setuseowner_type(const Value: Boolean);
begin
 Self.fisuseowner_type := Value;
end;     }

{ TDoc }

procedure TDoc.Clear;
begin
  Self.fdoc_date :=0;
  Self.fdoc_num := 0;
  Self.fdoc_seria := '';
  Self.fdoc_type := 0;
  Self.fdoc_code := '';
  Self.fdoc_name := '';

  Self.fisusedtype := False;
  Self.fisusedndate := False;
  Self.fisusednum := False;
  Self.fisusedseria := False;
end;

class function TDoc.construct(pNUm: Integer; pseria: string; pdate: TDate;  ptype: double; pDoc_code : string; pDoc_name : string): tDoc;
begin
  Result.fdoc_num := pNUm;
  Result.fdoc_date := pdate;
  Result.fdoc_seria := pSeria;
  Result.fdoc_type :=ptype;
  Result.fdoc_code :=pDoc_code;
  Result.fDoc_name :=pDoc_name;
  if pNUm <> 0    then Result.fisusednum   :=True;
  if pseria <> '' then Result.fisusedseria :=True;
  if pdate <> 0   then Result.fisusedndate :=True;
  if pDoc_code <> '' then Result.fisusedtype :=True;
end;

{function TDoc.getdoc_code: string;
begin
  Result := Self.doc_code;
end;

function TDoc.getDoc_date: tDate;
begin
  Result := Self.doc_date;
end;

function TDoc.getdoc_name: string;
begin
  Result := Self.doc_name;
end;

function TDoc.getdoc_type: double;
begin
  Result := Self.doc_type;
end;

function TDoc.getDoc_num: integer;
begin
  Result := Self.doc_num;
end;

function TDoc.getDoc_Seria: string;
begin
  Result := Self.doc_seria;
end;

function TDoc.getisusedtype: Boolean;
begin
  Result := Self.isusedtype;
end;

function TDoc.getisusedndate: Boolean;
begin
  Result := Self.isusedndate;
end;

function TDoc.getisusednum: Boolean;
begin
  Result := Self.isusednum;
end;

function TDoc.getisusedseria: Boolean;
begin
  Result := Self.isusedseria;
end;                                       }

procedure TDoc.setdoc_code(const Value: string);
begin
  Self.fdoc_code := Value;
end;

procedure TDoc.setDoc_date(const Value: tDate);
begin
 Self.fdoc_date := Value;
 if Value <> 0 then self.isusedndate := True;
end;

procedure TDoc.setdoc_name(const Value: string);
begin
  Self.fdoc_name := Value;
end;

procedure TDoc.setdoc_type(const Value: double);
begin
 Self.fdoc_type := Value;
 if Value <> 0 then self.isusedtype := True;
end;

procedure TDoc.setisusedtype(const Value: boolean);
begin
  Self.fisusedtype := Value;
end;

procedure TDoc.setisusednDate(const Value: boolean);
begin
  Self.fisusedndate := Value;
end;

procedure TDoc.setisusednum(const Value: boolean);
begin
 Self.fisusednum := Value;
end;

procedure TDoc.setisusedSeria(const Value: boolean);
begin
 Self.fisusedseria := Value;
end;

procedure TDoc.setDoc_num(const Value: integer);
begin
 Self.fdoc_num := Value;
 if Value <> 0 then self.isusednum := True;
end;

procedure TDoc.setDoc_seria(const Value: string);
begin
 Self.fdoc_seria := Value;
 if Value <> '' then self.isusedseria := True;
end;

end.
