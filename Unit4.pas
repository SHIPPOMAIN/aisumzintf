unit Unit4;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  UIAisUmzForMgis,  UAisUmzforMgisClass ,  MYcLASS3, uRemontRight,
  Vcl.StdCtrls, Vcl.Imaging.jpeg, Vcl.ExtCtrls ;

type

  TForm4 = class(TForm)
    Button1: TButton;
    edt1: TEdit;
    edt2: TEdit;
    edt3: TEdit;
    edt4: TEdit;
    edt5: TEdit;
    edt6: TEdit;
    lbl1: TLabel;
    lbl2: TLabel;
    btn1: TButton;
    btn2: TButton;
    edt7: TEdit;
    edt8: TEdit;
    mmo1: TMemo;
    btnTestReadXML: TButton;
    Label1: TLabel;
    img1: TImage;
    btn4: TButton;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn1Click(Sender: TObject);
    procedure btnTestReadXMLClick(Sender: TObject);
    procedure btn4Click(Sender: TObject);

  private
    { Private declarations }
  public
     procedure WriteTofile(var ff:TextFile; msg:string; res: string);
    { Public declarations }
  end;
var
  Form4: TForm4;
  myI: IAisUMZForMgis;
  Res : Integer;

  myfun : TFunctional;
  chbase : tRemont;
  ZemUch : TZemUch;
  ZemRight : TZemRight;

  ff: TextFile;
implementation

{$R *.dfm}

uses readerXML.tTestReader, AISUMZDB.Types;

{ TForm4 }

procedure TForm4.btn1Click(Sender: TObject);
begin
  Res := 0;
  Res :=  myI.UpdateGuid(Trim(edt7.Text),StringToGUID(edt8.Text));
  edt6.Text := IntToStr(Res);
end;

procedure TForm4.btn2Click(Sender: TObject);
begin
 if myI.DisConnect
 then
  begin
    edt6.Text := '���������� ������ �������';
    myI := nil;

    mmo1.Visible := False;
    Label1.Visible :=False;
    img1.Visible := True;
  end
 else
  edt6.Text := '��� ��� ���������� ���� ��� ��� -�� ���!!!!';
end;

procedure TForm4.btn4Click(Sender: TObject);
begin
  if Res = 0 then begin
    ShowMessage('�� �� ������������ � ����!');
    Exit;
   end;

  if edt5.Text = '' then  begin
   ShowMessage('�� �� ����� ��������� ������� ������ ��� ��������!');
    Exit;
  end;
  assignfile(ff,ExtractFilePath(Application.ExeName)+'resultRemont.txt');
  Rewrite(FF);
  img1.Visible := False;
  mmo1.Visible := True;
  Label1.Visible := True;
  chbase := TRemont.Create;
  chbase.MYINterface := MyI;
  if chbase.MYINterface.MyTrAc.Active = false
   then  chbase.MYINterface.MyTrAc.StartTransaction;
  chbase.CheckBase(StrToInt(edt5.Text));
  chbase.MYINterface.MyTrAc.CommitRetaining;
  WriteTofile(ff,'��������� ���������','');
  CloseFile(FF);
  chbase := nil;
end;

procedure TForm4.btnTestReadXMLClick(Sender: TObject);
var
 _od: TOpenDialog;
 _file: string;
 _parcel: tParcel;
 _parcelRight : TArray<TParcelRight>;
 _parcelArenda : TArray<TParcelRight>;
 _parser : KP05Parser;
 i, ii:Integer;

begin

Form4.btn4.Click;

if Res = 0 then begin
  // ShowMessage('�� �� ������������ � ����!');
   Exit;
end;

   myfun := TFunctional.Create;
   myfun.MYINterface := MyI;
   if myfun.MYINterface.MyTrAc.Active = false
   then  myfun.MYINterface.MyTrAc.StartTransaction;

  ii:=0;
 _od:= TOpenDialog.Create(nil);
 try
   _od.Options:= _od.Options + [ofAllowMultiSelect] ;
   _od.Filter := '����� ���|*.xml' ;
   _od.FilterIndex := 0;
   assignfile(ff,ExtractFilePath(Application.ExeName)+'result'+DateToStr(Date()) +'.txt');
   Rewrite(FF);
   mmo1.Lines.Clear;
   
   if _od.Execute then
   begin
    mmo1.Visible := True;
    Label1.Visible := True;
    img1.Visible := False;

   for _file in _od.Files do begin
     //_parcel:= tTestReader.doRead(_file);
     try
       _Parser.OpenDocument(_file);
       ii := ii+1;
       _parcel := _Parser.ParseParcel;
         if _parcel.Category.Code = '003001000000' then    // only selhoz naznachenie
          begin
             _parcelRight := _parser.ParseRight;
             _parcelArenda := _parser.ParseArenda;
             _Parser.closeDocument;

             WriteTofile(ff, ' ','');
             WriteTofile(ff,IntToStr(ii),' ');
             WriteTofile(ff,ExtractFileName(_file),'');
             WriteTofile(ff,_parcel.zu_cad_num,'');

             myfun.WhatInsertOrUpdate(_parcel);

             if High(_parcelRight) <> -1 then
                myfun.WhatInsertOrUpdate(_parcelRight, _parcel, 1);
             if High(_parcelRight) = -1
               then WriteTofile(ff,'����� �� ������� ����������� � ���','');

             for I := Low(_parcelArenda) to High(_parcelArenda) do       // ��������� ������ ������ ������ �� ����� ��� �������� ���������� ������ ������ �������.
              begin                                                      // ������ ��������� �� ���������� �����������, � ���� ��� ������� ������ - �� ��� ������ ����� ������
               if (_parcelArenda[I].right_cod <> '')  then
                 myfun.WhatInsertOrUpdate(_parcelArenda[I], _parcel, 2);
              end;
             if High(_parcelArenda) = -1
               then WriteTofile(ff,'������ �� ������� ����������� � ���','');
          end
         else
          begin
           WriteTofile(ff, ' ','');
           WriteTofile(ff,IntToStr(ii),' ');
           WriteTofile(ff,ExtractFileName(_file),'');
           WriteTofile(ff,_parcel.zu_cad_num + '  ������� �� ��������� � ������ ������� ���������� � �� ����� ��������������','');
          end;
     except
       _Parser.closeDocument;
       WriteTofile(ff,'','');
       WriteTofile(ff,IntToStr(ii),'');
       WriteTofile(ff,ExtractFileName(_file),'');
       WriteTofile(ff,'���� ���� �� ��������� � ����','');
     end;

     //edt6.Text := IntToStr(ii)+ '  '+ ExtractFileName(_file);
     Application.ProcessMessages;
   end;
   end;

 finally
   WriteTofile(ff,'��������� ���������','');
   CloseFile(FF);
   _od.Free;
   edt6.Text := '��������� ���������';
 end;
end;

procedure TForm4.Button1Click(Sender: TObject);
begin

  if edt1.Text = '' then begin
    ShowMessage('�� �� ��������� �����!');
    Exit;
   end;

  if edt2.Text = '' then begin
    ShowMessage('�� �� ��������� ������!');
    Exit;
   end;

  if edt5.Text = '' then begin
    ShowMessage('�� �� ��������� ��������� ������� ������ ������!');
    Exit;
  end;

 myI := TAisUMZForMgis.Create;
 Res := 0;
 Res := myI.Connect(edt4.Text,edt1.Text,edt2.Text,edt3.Text,StrToInt(edt5.Text));
 edt6.Text := IntToStr(Res);

 if Res <> 100 then
   begin
     img1.Visible := False;
     mmo1.Visible := True;
     mmo1.Lines.Clear;
     mmo1.Lines.Add(lbl2.Caption)
   end
 else
   begin
     img1.Visible := true;
     mmo1.Visible := false;
//     mmo1.Lines.Clear;
   end;
 //  //06-04-16     - ��� ��������
 { myfun := TFunctional.Create;
  myfun.MYINterface := MyI;
  myfun.MYINterface.MyTrAc.StartTransaction;     }

end;

procedure TForm4.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //myI := nil;
end;

procedure TForm4.FormShow(Sender: TObject);
begin
 //myI := TAisUMZForMgis.Create;
 // Self.ClientHeight := 552;
 // Self.ClientWidth  := 444;
 // Self.Position := poDesktopCenter;
end;

procedure TForm4.WriteTofile(var ff: TextFile; msg, res: string);
begin
  Writeln(ff,msg + '   '+ res);
  mmo1.Lines.Add(msg+ '  '+ res);
end;

end.
