unit UAisUmzforMgisClass;

interface

uses System.SysUtils, System.StrUtils,
     UIAisUmzForMgis, uData, {Vcl.Forms,} uERC, IBDatabase;

type
  TAisUMZForMgis = class(TInterfacedObject,IAisUMZForMgis)
    private
     Server_name  : String;
     Base_path    : string;
     vlocal_prefix : Integer;
     vServerPath   : string;
     vLogin        : string;
     vPass         : string;
     vRole         : string;

     dm1 : Tdm1;
     function GetDB:TIBDatabase;       //6-4-16
     function GetTrAc:TIBTransaction;       //6-4-16
    public
     property Mydb : TIBDATABase read GetDB;    //6-4-16
     property MyTrAc : TIBTransaction read GetTRAc;
     constructor Create;
     destructor destroy;  override;
     function Connect(ServerPath,Login,Pass,Role: string; local_prefix : Integer):integer;
     function UpdateGuid(KN:string; GID: Tguid): integer;
     function DisConnect:boolean;
     //function GetMydb:tIBdatabase;
     //property Fdm1 : Tdm1 read dm1;
  end;


implementation



  constructor TAisUMZForMgis.Create;
  begin
    dm1 := Tdm1.Create(nil);
    vlocal_prefix :=0;
    vServerPath   :='';
    vLogin        :='';
    vPass         :='';
    vRole         :='';
//    ERCode :=
  end;

  destructor TAisUMZForMgis.destroy;
  begin
    //dm1.DisConnectBase(vServerPath);
    self.dm1.free;
    inherited;
  end;

  function TAisUMZForMgis.GetDB():TIBDATAbase;     //6-4-16
  begin
    result :=  dm1.ibdtbs1;
  end;

function TAisUMZForMgis.GetTrAc: TIBTransaction;
begin
 Result := dm1.ibtrnsctn1;
end;

{ function TAisUMZForMgis.GetMydb: tIBdatabase;   //6-4-16
  begin
   Result :=  dm1.ibdtbs1;
  end;         }

procedure ReadServerPath(SPath:string; var Server:string; var Path:string);
  var port1,ss : string;
      count,i : Integer;
  begin
    // ��������� ���� �� ������ � ���� �� ����
    SS := trim(SPath);
    port1 := '';

    count := 0;
    for i:=1 to length(SS) do
    begin
     if SS[i]=':' then count:=count+1;
    end;

    if (Count=2) or (pos('/',SS)<>0)
    then
      begin
        Server := copy(SS,1,pos(':',SS)-1);
        Path   := copy(SS,pos(':',SS)+1,length(SS));
      end
    else //����� �������� ������� ���� ����
      begin
       Server := copy(SS,1,posEx(':',SS,pos(':',SS)+1)-1);
       Path := copy(SS,posEx(':',SS,pos(':',SS)+1)+1,length(SS));
      end;

   if length(Server) >9 // �� ���� ���� ���� ����� �������  {localhost = 9}
   then
    port1 := copy(Server,10,length(Server));  // ���� ������������ ��������

  end;


  function TAisUMZForMgis.Connect(ServerPath,Login,Pass, Role: string; local_prefix:integer): integer;
  // 100 - ������� ������ �������
  // 101 - �� ������� ������ / �� �������� ������-������
  // 102 - ��� ���� �� ���������� ����
  // 103 - �� �������� ������ �����/������/����
  // 104 - �� �������� ������-������

  // 108 - ������ ���������� �����- ����� ���� � ���� ���
  // 109 - ������ �� ���� ������ �� ������. �����-�� ������
  // 110 - ������ ������ ������ ���� �� ������ ����� ������ �� ������. �����-�� ������
  var RCode : Integer;
  begin

    ReadServerPath(ServerPath,Server_name,Base_path);
    // 1- �������� �������
    RCode := dm1.ServerTest(Server_name);
    // 2- �������� ���� �� �������������
    if RCode = ERC_Ok
    then
    begin
        if FileExists(Base_path)
        then RCode := ERC_Ok
        else RCode := ERC_NOBase;

        //3- ��� ������� � ����
        if RCode = ERC_Ok
        then
         begin
           RCode := dm1.ConnectBase(Serverpath,Role,Login, Pass,local_prefix) ;
           if RCode = ERC_Ok then
            begin
              vlocal_prefix :=local_prefix;
              vServerPath   :=ServerPath;
              vLogin        :=Login;
              vPass         :=Pass;
              vRole         :=Role;
            end;
           Result := RCode;
         end
        else
         Result := RCode;
    end
    else
     Result := RCode;
  end;

  function TAisUMZForMgis.UpdateGuid(KN:string; GID: Tguid): integer;
  // 105 - ���� � ������ read-only
  // 106 - �� �������� local_prefix
  // 107 - ������ ������� �� ���� ��� ��������
  // 111 - ������ � ������� �������� � ���� �� ������
  // 112 - ������� � ����� ����������� ��� � ����
  // 113 - ������ � ������� T_LAND �� ������
  // 114 - update ������ �� �� ������
  var RCode : Integer;
  begin
    // 1 - �������� ���� �� ���-����
    RCode :=  dm1.CheckBase();
    if RCode = ERC_Ok then
      begin
       // 2 - �������� �� ������������� �������, �������� �� ��������� �������, �������� �� ������
       RCode := dm1.CheckLand(kn, vlocal_prefix);
       // ������
       if RCode = ERC_Ok
         then
           begin
             if dm1.UpdateGuidbyCadnum(kn, GID)
             then Result := ERC_Ok
             else Result := ERC_NOUpdate
           end
         else Result := RCode;

      end
    else
     Result := RCode;
  end;

  // ������������ �� �������
  function TAisUMZForMgis.DisConnect:boolean;
  begin
    //
    if dm1.DisConnectBase(vServerPath)
    then
      begin
        Result := True;
      end
    else
     Result := False;

  end;
end.
