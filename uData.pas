unit uData;

interface

uses
  System.SysUtils, System.Classes, IBDatabase, Data.DB, IBServices,    Vcl.Dialogs,
  IBCustomDataSet, IBSQL, uERC;

type
  Tdm1 = class(TDataModule)
    ibscrtysrvc1: TIBSecurityService;
    ibdtbs1: TIBDatabase;
    ibtrnsctn1: TIBTransaction;
    IBDS1: TIBDataSet;
    IBQUpdateGuid: TIBSQL;

  private
    { Private declarations }
  public
    { Public declarations }
    function ServerTest(Server : string): Integer;
    function ConnectAdmin(Server_Path:string):Boolean;
    function ReadUserData(RoleName: string; Role_id: double; UserName : string; Password: string; local_pr: integer; var vUser_Name: string; var vPassword :String;var vUser_ID :Double; var vLocal_Prefix: Integer;var vRole : string): Boolean; stdcall;
    function ReadRoleCurrentPerson(RoleName : string): double;
    function ConnectBase(Server_path:string; RoleName: string; UserName : string; Password: string; local_pr: integer):Integer; stdcall;
    function DisConnectBase(ServerPath:string): boolean;
    function UpdateGuidbyCadnum(var kn:string; var newG:TGuid):boolean;
    function CheckBase():integer;
    function CheckLand(kn: string; Local_prefix: integer): Integer;
  end;


implementation
 uses UAisUmzforMgisClass;
{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

 // ���� � �������
 //????????? �������� �������� ����������
  function Tdm1.ServerTest(Server : string): Integer;
  begin
   with Self do
   begin
     // ibscrtysrvc1.Params.Clear;
      ibscrtysrvc1.ServerName := trim(Server);
    //  ibscrtysrvc1.Protocol := Local;

    //  ibscrtysrvc1.Params.Add('user_name=SYSDBA');
    //  ibscrtysrvc1.Params.Add('password=superVlands');
    //  ibscrtysrvc1.Password := 'superVlands';
      try
          // ������� ���������������
          ibscrtysrvc1.Active := True;
          ibscrtysrvc1.Active := false;
          Result := ERC_Ok;
      except
          Result := ERC_NoServer;
      end;
     end;
  end;

   // ������� � ������-�������
  function Tdm1.ConnectAdmin(Server_Path:string):Boolean;
  begin
   with Self do
   begin
       try
        ibdtbs1.Params.Clear;
        ibdtbs1.LoginPrompt:=False;
        ibdtbs1.DatabaseName:=Trim(Server_path);
        ibdtbs1.Params.Add('user_name=SYSDBA');
        ibdtbs1.Params.Add('password=superVlands');
        ibdtbs1.Params.Add('lc_ctype=win1251');
        ibdtbs1.Connected:=True;
        ibtrnsctn1.StartTransaction;

        Result := True;
       except
        Result := False;

        ibtrnsctn1.Active := false;
        ibdtbs1.Connected:=False;
       end;
   end;
  end;

  // ������ ������ ���� �� ������ �����
  // RCode = 110 - ������ ������ ������ ���� �� ������ ����� ������ �� ������. �����-�� ������
  function Tdm1.ReadUserData(RoleName: string; Role_id: double; UserName : string; Password: string; local_pr: integer; var vUser_Name: string; var vPassword :String;var vUser_ID :Double; var vLocal_Prefix: Integer;var vRole : string): Boolean;
  //var IBDS1 : TIBDataSet;                              // prog                            prog               prog              prog                   db                       db                   db                    db                          db
  begin
   with Self do
   begin
       try
        if IBDS1.Active then IBDS1.Close;
        IBDS1.SelectSQL.Clear;
        IBDS1.SelectSQL.Add('Select u.ID, u.DB_USER, u.DB_PASSWORD, u.Login, u.Family, u.Name, u.LOCAL_PREFIX, NULL as DB_ROLE   ' );
        IBDS1.SelectSQL.Add(' from T_User u  ');
        IBDS1.SelectSQL.Add(' where :p = 0   ');
        IBDS1.SelectSQL.Add(' and UPPER(u.LOGIN) like UPPER(:pLogin)                 ');
        IBDS1.SelectSQL.Add(' and UPPER(u.USER_PASSWORD) like UPPER(:pPassword)      ');
        IBDS1.SelectSQL.Add(' and (((u.BEGIN_DATE is Null) and (u.END_DATE is Null)) ');
        IBDS1.SelectSQL.Add(' or ((u.BEGIN_DATE is Null) and (:pDate <= u.END_DATE)) ');
        IBDS1.SelectSQL.Add(' or ((:pDate >= u.BEGIN_DATE) and (u.END_DATE is Null)) ');
        IBDS1.SelectSQL.Add(' or (:pDate between u.BEGIN_DATE and u.END_DATE) )      ');
        IBDS1.SelectSQL.Add(' union                                                  ');
        IBDS1.SelectSQL.Add(' Select u.ID, u.DB_USER, u.DB_PASSWORD, u.Login,u.Family, u.Name, u.LOCAL_PREFIX, r.DB_ROLE ');
        IBDS1.SelectSQL.Add(' from T_User u, T_ROLE r                                ');
        IBDS1.SelectSQL.Add(' where :p = 1                                           ');
        IBDS1.SelectSQL.Add(' and r.ID = :pRole_ID                                   ');
        IBDS1.SelectSQL.Add(' and u.ROLE_ID = r.ID                                   ');
        IBDS1.SelectSQL.Add(' and UPPER(u.LOGIN) like UPPER(:pLogin)                 ');
        IBDS1.SelectSQL.Add(' and UPPER(u.USER_PASSWORD) like UPPER(:pPassword)      ');
        IBDS1.SelectSQL.Add(' and (((u.BEGIN_DATE is Null) and (u.END_DATE is Null)) ');
        IBDS1.SelectSQL.Add(' or ((u.BEGIN_DATE is Null) and (:pDate <= u.END_DATE)) ');
        IBDS1.SelectSQL.Add(' or ((:pDate >= u.BEGIN_DATE) and (u.END_DATE is Null)) ');
        IBDS1.SelectSQL.Add(' or (:pDate between u.BEGIN_DATE and u.END_DATE) )      ');

       if  ((RoleName = '') and (UpperCase(userName) = 'SYSDBA'))
       then IBDS1.ParamByName('p').Value :=Role_id//0
       else
          begin
            IBDS1.ParamByName('p').Value :=1;
            IBDS1.ParamByName('pRole_ID').AsDouble :=Role_id;
          end;
       IBDS1.ParamByName('pDate').Value := date;
       IBDS1.ParamByName('pLogin').AsString := UserName;
       IBDS1.ParamByName('pPassword').AsString := Password;
       IBDS1.Open;

       vUser_Name    := IBDS1.FieldByName('DB_user').AsString;
       vPassword     := IBDS1.FieldByName('DB_PASSWORD').AsString;
       vUser_ID      := IBDS1.FieldByName('ID').Value;

       if UpperCase(UserName) = UpperCase('sysdba')
        then vLocal_Prefix := 363400
        else vLocal_Prefix := IBDS1.FieldByName('LOCAL_PREFIX').Value;

       if IBDS1.FieldByName('DB_ROLE').IsNull
         then vRole := ''
         else vRole := IBDS1.FieldByName('DB_ROLE').AsString;

       IBDS1.Close;

        Result := True;
       except
        Result := false;
       end;
   end;

  end;

  // ������� ������ ���� �� ���������� ����������
  // RCode = 108 - ������ ���������� �����- ����� ���� � ���� ���
  // RCode = 109 - ������ �� ���� ������ �� ������. �����-�� ������
  function Tdm1.ReadRoleCurrentPerson(RoleName : string): double;
  var IBDS1 : TIBDataSet;
  begin
   with Self do
   begin
       try
        if IBDS1.Active then IBDS1.Close;
        IBDS1.SelectSQL.Clear;
        IBDS1.SelectSQL.Add('select r.ID from t_role r where UPPER(r.Name) like UPPER(:pName) ');
        IBDS1.Params[0].AsString := RoleName;
        IBDS1.Open;

        if not IBDS1.IsEmpty then Result := IBDS1.FieldByName('ID').Value
                             Else Result := ERC_Empty;
        IBDS1.Close;
       except
        Result := ERC_NoRole;
       end;
   end;
  end;

   // ������� � ����
  function Tdm1.ConnectBase(Server_path:string; RoleName: string; UserName : string; Password: string; local_pr: integer):Integer;
  var  dbRoleID : Double;
       wUser_Name, wPassword, wRole :String;
       wUser_ID :Double;
       wLocal_Prefix: Integer;
  begin
   with Self do
   begin
        // ������� � ������-�������
        if ibtrnsctn1.Active then ibtrnsctn1.Active := False;
        if ibdtbs1.Connected then ibdtbs1.Connected := False;

        if ConnectAdmin(Server_Path)  // ����������� ��������
        then
         begin
          // ������ ������� ������ �� ���������� ����������
          if RoleName <> ''
           then dbRoleID := ReadRoleCurrentPerson({ibdtbs1,}RoleName)
           else dbRoleID := 0;

          if (dbRoleID <> ERC_Empty) and (dbRoleID <> ERC_NoRole)
          then
            begin
              // ������� � ����������� �������.
              wUser_Name := '';
              wPassword  := '';
              wUser_ID   := 0;
              wLocal_Prefix := 0;
              wRole := '';

              if ReadUserData(RoleName, DBRoleid, UserName, Password, local_pr, wUser_Name, wPassword ,wUser_ID ,wLocal_Prefix, wRole)
                then
                  begin
                    try
                 //   IBT1.Active := False;
                    ibdtbs1.Connected:=False;
                    ibdtbs1.Params.Clear;
                    ibdtbs1.LoginPrompt:=False;
                    ibdtbs1.DatabaseName:=Trim(Server_path);
                    ibdtbs1.Params.Add('user_name='+trim(wUser_Name));
                    ibdtbs1.Params.Add('password='+trim(wPassword));
                    if (wRole <> '') then ibdtbs1.Params.Add('sql_role_name='+trim(wRole));
                    ibdtbs1.Params.Add('lc_ctype=win1251');
                    ibdtbs1.Connected:=True;

                     Result := ERC_Ok;    // ���������� �����������
                    except
                     Result := ERC_NOLogin;
                    end;
                  end
                else
                  Result := ERC_NOUser; //
            end
          else
           begin //����� �� ��������� � ������ ������ - �� ������� ��������� ��������� ���� �� ����������
             if dbRoleID = ERC_Empty then Result := ERC_Empty;
             if dbRoleID = ERC_NoRole then Result := ERC_NoRole;
           end;
         end
        else
          Result := ERC_NOmaster; // ������-������ �� �������
   end;
  end;

  // ������������ �� �������
  // true  - �����
  // false -  ������ �� �� �������
  function Tdm1.DisConnectBase(ServerPath:string): boolean;
  begin
    if Self <> nil
    then

        with Self do
        begin
          if not ibdtbs1.Connected
          then
            begin
              Result := False;
              Exit;
            end
          else
          try
          if ibtrnsctn1.Active
            then
              begin
               ibtrnsctn1.Commit;
               ibtrnsctn1.Active := False;
              end;
           if ibdtbs1.Connected
           then ibdtbs1.Connected := False;

           Result := True;
          except
           Result := False;
          end;
        end
    else
     Exit;
  end;

 // ������� �������������� ����� �� ����������� ������
  function Tdm1.UpdateGuidbyCadnum(var kn:string; var newG:TGuid):boolean;
  begin
    with Self do
    begin
     //ibtrnsctn1.StartTransaction;
     try
      //if IBQUpdateGuid.Active then IBQUpdateGuid.Close;
      IBQUpdateGuid.ParamByName('pG').AsString  := GUIDToString(newG);
      IBQUpdateGuid.ParamByName('pKN').AsString := kn;
      IBQUpdateGuid.ExecQuery;
      ibtrnsctn1.CommitRetaining;
      Result := true;
     except
      ibtrnsctn1.RollbackRetaining;
      result :=false;
     end;
    end;
  end;

  // �������� ���� �� ���� ����
  function Tdm1.CheckBase():integer;
  // 111 - ������ � ������� �������� � ���� �� ������
  begin
   if Self <> nil
   then
      with Self do
      begin
        if IBDS1.Active then IBDS1.Close;
        IBDS1.SelectSQL.Clear;
        IBDS1.SelectSQL.Add('select FL_READ_ONLY from T_IMP_EXP');
        IBDS1.Open;
        if IBDS1.FieldByName('fl_read_only').Value=0
          then Result := ERC_Ok  // ����� ���������
          else Result := ERC_Readonly;  // ����� ���-����
        IBDS1.Close;
      end
   else
    begin
      Result := ERC_NOImpExp;
      Exit;
    end;
  end;

  // �������� ������ � �������
  function Tdm1.CheckLand(kn: string; Local_prefix: integer): Integer;
 // 106 - �� �������� local_prefix
 // 107 - ������ ������� �� ���� ��� ��������
 // 112 - ������� � ����� ����������� ��� � ����
 // 113 - ������ � ������� T_LAND �� ������
  begin
    with Self do
      begin
       try
         if IBDS1.Active then IBDS1.Close;
         IBDS1.SelectSQL.Clear;
         IBDS1.SelectSQL.Add('select ID,Local_prefix, status  from T_Land where cad_num = :vkn');
         IBDS1.ParamByName('vkn').AsString := Trim(kn);
         IBDS1.Open;
         if IBDS1.IsEmpty
           then Result := ERC_NOCadNum
           else
           begin
           // if IBDS1.RecordCount > 1 then ??????

             if (IBDS1.FieldByName('Status').value = 0)
               then Result := ERC_Ok
               else Result := ERC_NOStatus;

             if (IBDS1.FieldByName('Local_prefix').AsInteger = Local_prefix) or (Local_prefix = 363400)
               then Result := ERC_Ok
               else Result := ERC_NOLocpref;// ��������� ���� �� �� ����� ������ � �� ����

           end;

        IBDS1.Close;
       except
         Result := ERC_NOTland;
       end;
      end
  end;

end.
