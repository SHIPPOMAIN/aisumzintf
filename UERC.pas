unit UERC;

interface
uses
  System.SysUtils, System.Classes;

     const ERC_Ok        :integer = 100;       // 100 - ������� ������ �������                                     //TAisUMZForMgis.Connect
     const ERC_NoServer  :integer = 101;       // 101 - �� ������� ������ / �� �������� ������-������              //TAisUMZForMgis.Connect
     const ERC_NOBase    :integer = 102;       // 102 - ��� ���� �� ���������� ����                                //TAisUMZForMgis.Connect
     const ERC_NOLogin   :integer = 103;       // 103 - �� �������� ������ �����/������/����                       //TAisUMZForMgis.Connect
     const ERC_NOmaster  :integer = 104;       // 104 - �� �������� ������-������                                  //TAisUMZForMgis.Connect

     const ERC_Empty     :integer = 108;       // 108 - ������ ���������� �����- ����� ���� � ���� ���             //TAisUMZForMgis.Connect
     const ERC_NoRole    :integer = 109;       // 109 - ������ �� ���� ������ �� ������. �����-�� ������           //TAisUMZForMgis.Connect
     const ERC_NOUser    :integer = 110;       // 110 - ������ ������ ������ ���� �� ������ ����� ������ �� ������. �����-�� ������  //TAisUMZForMgis.Connect

     const ERC_Readonly  :integer = 105;       // 105 - ���� � ������ read-only                                    //TAisUMZForMgis.UpdateGuid
     const ERC_NOLocpref :integer = 106;       // 106 - �� �������� local_prefix                                   //TAisUMZForMgis.UpdateGuid
     const ERC_NOStatus  :integer = 107;       // 107 - ������ ������� �� ���� ��� ��������                        //TAisUMZForMgis.UpdateGuid
     const ERC_NOImpExp  :integer = 111;       // 111 - ������ � ������� �������� � ���� �� ������                 //TAisUMZForMgis.UpdateGuid
     const ERC_NOCadNum  :integer = 112;       // 112 - ������� � ����� ����������� ��� � ����                     //TAisUMZForMgis.UpdateGuid
     const ERC_NOTland   :integer = 113;       // 113 - ������ � ������� T_LAND �� ������                          //TAisUMZForMgis.UpdateGuid
     const ERC_NOUpdate  :integer = 114;       // 114 - update ������ �� �� ������                                 //TAisUMZForMgis.UpdateGuid

//type
  // TErrorCodeConst = class//(integer)
// private
    { Private declarations }
//  public

    { Public declarations }
//  end;
implementation

end.
