unit uData2;

interface

uses
  System.SysUtils, System.Classes, Data.DB, IBCustomDataSet, IBQuery;

type
  Tdm2 = class(TDataModule)
    ibqryUpdate: TIBQuery;
    ibqryGenid: TIBQuery;
    ibqryInsert: TIBQuery;
    ibqryFind: TIBQuery;
    ibqryCheck: TIBQuery;
    ibqrySelect: TIBQuery;
    ibqryInsert2: TIBQuery;
    ibqrySelect2: TIBQuery;
    ibqrySelect3: TIBQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dm2: Tdm2;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
