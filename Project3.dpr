program Project3;

uses
  Vcl.Forms,
  Unit4 in 'Unit4.pas' {Form4},
  UIAisUmzForMgis in 'UIAisUmzForMgis.pas',
  UAisUmzforMgisClass in 'UAisUmzforMgisClass.pas',
  uData in 'uData.pas' {dm1: TDataModule},
  UERC in 'UERC.pas',
  uILoadZU in 'uILoadZU.pas',
  uLoadZuClass in 'uLoadZuClass.pas',
  MYcLASS3 in 'MYcLASS3.pas',
  uData2 in 'uData2.pas' {dm2: TDataModule},
  AISUMZDB.Types in 'AISUMZDB.Types.pas',
  AISUMZDB.SQLTEXTS in 'AISUMZDB.SQLTEXTS.pas',
  readerXML.tTestReader in 'readerXML\readerXML.tTestReader.pas',
  KPZU_v05_ in 'readerXML\KPZU_v05_.pas',
  uRemontRight in 'uRemontRight.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm4, Form4);
  Application.CreateForm(Tdm2, dm2);
  // Application.CreateForm(Tdm1, dm1);
  Application.Run;
end.
