unit MYcLASS3;

interface
uses classes, System.SysUtils,  System.Generics.Collections , uData2,UIAisUmzForMgis,
     UAisUmzforMgisClass , Data.DB, IBCustomDataSet, IBQuery ,System.Variants,

      AISUMZDB.Types, AISUMZDB.SQLTEXTS;

type

  TZemUch = tParcel;
  TZemRight = TParcelRight;

  tFunctional = class
    private
    myI : IAisUMZForMgis;

    function FindKN(vKn:string):Double;
    function ReadLocal_prefix(vKn:string):Integer;
    function FindParent_id(vKn : string):double;

    function InsertKvartal(vKn:string):Double;
    function InsertZemUch(var zemUch: TZemUch):double;
    function insertLand(var zemUch: TZemUch):Boolean;
    function insertLandChar(var zemUch: TZemUch):Boolean;
    function insertLandAdr(var zemUch: TZemUch):Boolean;
    function insertLandUse(var zemUch: TZemUch):Boolean;
    function InsertNewUse(zemUch:TZemUch):double;

    function LandGenId(vKN: string):Double;
    function LandAdrGenId(vKN: string):Double;
    function LandcharGenId(vKN: string):Double;
    function LanduseGenId(vKN: string):Double;

    function LandGenRightId(vKN: string):Double;
    function LandGenRightRegId(vKN: string):Double;
    function LandGenLandRightId(vKN: string):Double;
    function LandGenRightOwnerId(vKN: string):Double;
    function LandGenDocId(vKN: string):Double;
    function LandGenRightDocId(vKN: string):Double;
    function GenPersonId(vKN: string):Double;
    function GenEnterpriseId(vKN: string):Double;
    function GenuseId(vKN: string):Double;

    function FindAdrbyKladr(vkladr:string): Double;
    function FindUseIDbyCode(vCode:string):Double;
    function FindUnitbyCode(vUCode:string):Double;
    function FindCategorybyCode(vUCode:string):Double;
    function FindRightTypeID(right_cod:string; owner_type:integer; owner_name:string):Double;
    function FinfDocTYpeByCode(vCode:string):double;
    function FindPersonbyName(name1 : string): Double;
    function FindEnterprisebyName(name1 : string): Double;
    function FindLandRightID(lID : double):double;
    function findRightRegBYID(rId:Double; type_reg:integer): boolean;
    function FindRightID(lright_id:double):Double;
    function FindLandRightIDArenda(lID: double): double;

    function createPerson(name : string; ow_count:integer; vKN:string): Double;
    function createEnterprise(Ename : string; vKn : string): Double;

    function UpdateZemUch(var ZemUch:TZemUch):double;
    function UpdateLandArea(ZemUch:TZemUch):boolean;
    function UpdateLandAreaUNIT(ZemUch:TZemUch):boolean;

    function UpdateLandCharCategory(ZemUch:TZemUch):boolean;
    function UpdateLandCharCadCost(ZemUch:TZemUch):boolean;
    function UpdateLandCharUnit(ZemUch:TZemUch):boolean;

    function UpdateLandAdrID(ZemUch:TZemUch): Boolean;
    function UpdateLandAdrNote(ZemUch:TZemUch): Boolean;

    function UpdateLandUseValue(ZemUch:TZemUch):Boolean;
    function UpdateLandUseFact(ZemUch:TZemUch):Boolean;

    function  ChecklandChar(ZemUch:tzemuch) : Boolean;
    function  ChecklandAdr(ZemUch:tzemuch) : Boolean;
    function  ChecklandUse(ZemUch:tzemuch) : Boolean;
    function  CheckCountRihtOwners(ZemRight:TZemRight):integer;

    function ReadLand(var ZemUch:tzemuch):Boolean;
    function ReadLandAdr(var ZemUch:tzemuch):Boolean;
    function ReadLandChar(var ZemUch:tzemuch):Boolean;
    function ReadLandUse(var ZemUch:tzemuch):Boolean;

    function InsertZemRight(var ZemRight:TZemRight; vKn:string):Boolean;
    function InsertRight(var ZemRight:TZemRight; vKn:string):Boolean;
    function InsertRightReg(ZemRight:TZemRight; vKn:string; pregtype:integer):Boolean;
    function InsertDoc(var ZemRight:TZemRight; vKn:string):Boolean;
    function InsertLandRight(var ZemRight:TZemRight; vKn:string):double;
    function InsertRightOwner(var ZemRight:TZemRight; vKn:string; lrID : double):Boolean;

    function UpdateZemRight(var ZemRight:TZemRight; vKn:string):Boolean;
    function UpdateRight(var ZemRight:TZemRight):Boolean;
    function UpdateRightReg(ZemRight:TZemRight; vKn:string):Boolean;
    function UpdateDoc(ZemRight:TZemRight):Boolean;
    function UpdateLandRight(ZemRight:TZemRight):Boolean;
    function UpdateRightOwner(ZemRight:TZemRight; vKn :string):Boolean;

    public
    constructor Create();
    destructor Destroy; override;
    property MYINterface:IAisUMZForMgis  read MyI write MyI;

   // function FindUnitbyCode(vUCode:string):Double;
   // function FindCategorybyCode(vUCode:string):Double;
    procedure ClearZemUch(var ZemUch: TZemUch);
    procedure ClearZemright(var ZemRight: tzemRight);

    function  WhatInsertOrUpdate(zemUch : TZemUch):Boolean; overload;
    function  WhatInsertOrUpdate(var zemRight : TArray<TzemRight>; zemUch : TZemUch; rightOrarenda:integer):Boolean; overload;  // ��� �����
    function  WhatInsertOrUpdate(var zemRight : TZemRight; zemUch : TZemUch; rightOrarenda:integer):Boolean; overload;          // ��� ������
    procedure GetZu(var ZemUch:TzemUch; vKN:string);overload;
    procedure GetZu(var ZemRight:TzemRight; vKN:string); overload;
  end;

//var
//   ZemUch : TZemUch;
//   ZemRight : TZemRight;


implementation
uses Unit4;


constructor tFunctional.Create;
begin
  inherited;

end;

destructor tFunctional.Destroy;
begin

  inherited;
end;

function tFunctional.FindKN(vKn:string):Double;
 begin
   // 0 - ���� ������� ���
   // ID - ���� ����
   with dm2 do
   begin
   ibqryFind.Database :=  myI.Mydb;  //(myI as TAisUMZForMgis).GetMyDB;
   ibqryFind.sql.clear;
   ibqryFind.sql.add(_sqlFind_KN);
   ibqryFind.Params[0].AsString := vKn;
   //ibqryFind.Params[1].AsDate := date();
   ibqryFind.Open;
   if ibqryFind.IsEmpty then  Result :=0 else Result := ibqryFind.Fields[0].AsFloat;
   ibqryFind.Close;
   end;
 end;

function tFunctional.FindLandRightID(lID: double): double;
begin
 with dm2 do
   begin
   ibqryFind.Database :=  myI.Mydb;  //(myI as TAisUMZForMgis).GetMyDB;
   ibqryFind.sql.clear;
   ibqryFind.sql.add(_sqlFindLandRightID);
   ibqryFind.Params[0].value := lID;
   //ibqryFind.Params[1].AsDate := date();
   ibqryFind.Open;
   if ibqryFind.IsEmpty then  Result :=0 else Result := ibqryFind.Fields[0].AsFloat;
   ibqryFind.Close;
   end;
end;

function tFunctional.FindLandRightIDArenda(lID: double): double;
begin
 with dm2 do
   begin
   ibqryFind.Database :=  myI.Mydb;  //(myI as TAisUMZForMgis).GetMyDB;
   ibqryFind.sql.clear;
   ibqryFind.sql.add(_sqlFindLandArendaID);
   ibqryFind.Params[0].value := lID;
   //ibqryFind.Params[1].AsDate := date();
   ibqryFind.Open;
   if ibqryFind.IsEmpty then  Result :=0 else Result := ibqryFind.Fields[0].AsFloat;
   ibqryFind.Close;
   end;
end;

function tFunctional.ReadLand(var ZemUch:TZemuch): Boolean;
begin
 with dm2 do
 begin
  ibqryselect.Database :=  myI.Mydb;
  ibqryselect.sql.clear;
  ibqryselect.sql.add(_sqlRead_land);
  ibqryselect.Params[0].Value := ZemUch.zu_id;
  ibqryselect.open;
  if not ibqryselect.IsEmpty then
   begin
     if ibqryselect.Fields[0].Value <> null 
      then ZemUch.Area.Value := ibqryselect.Fields[0].Value;
     if ibqryselect.Fields[1].Value <> null 
      then ZemUch.Area.Unit_ := ibqryselect.Fields[1].Value;
    Result := True;
   end
  else 
    Result :=false ;
  ibqryselect.Close;  
 end;
end;

function tFunctional.ReadLandAdr(var ZemUch:tzemuch): Boolean;
begin
 with dm2 do
 begin
  ibqryselect.Database :=  myI.Mydb;
  ibqryselect.sql.clear;
  ibqryselect.sql.add(_sqlRead_landAdr);
  ibqryselect.Params[0].Value := ZemUch.zu_id;
  ibqryselect.open;
  if not ibqryselect.IsEmpty then
   begin
     if ibqryselect.Fields[0].Value <> null 
      then ZemUch.Address.Value := ibqryselect.Fields[0].Value;
     if ibqryselect.Fields[1].Value <> null 
      then ZemUch.Address.AdrNote := ibqryselect.Fields[1].Value;
    Result := True;
   end
  else 
    Result :=false ;
  ibqryselect.Close;  
 end;
end;

function tFunctional.ReadLandChar(var ZemUch:tzemuch): Boolean;
begin
 with dm2 do
 begin
  ibqryselect.Database :=  myI.Mydb;
  ibqryselect.sql.clear;
  ibqryselect.sql.add(_sqlRead_landChar);
  ibqryselect.Params[0].Value := ZemUch.zu_id;
  ibqryselect.open;
  if not ibqryselect.IsEmpty then
   begin
     if ibqryselect.Fields[0].Value <> null 
      then ZemUch.Category.Value := ibqryselect.Fields[0].Value;
     if ibqryselect.Fields[1].Value <> null 
      then ZemUch.CadCost.Value := ibqryselect.Fields[1].Value;
     if ibqryselect.Fields[2].Value <> null 
      then ZemUch.CadCost.Unit_ := ibqryselect.Fields[2].Value; 
    Result := True;
   end
  else 
    Result :=false ;
  ibqryselect.Close;  
 end;
end;

function tFunctional.ReadLandUse(var ZemUch:tzemuch): Boolean;
begin
 with dm2 do
 begin
  ibqryselect.Database :=  myI.Mydb;
  ibqryselect.sql.clear;
  ibqryselect.sql.add(_sqlRead_landuse);
  ibqryselect.Params[0].Value := ZemUch.zu_id;
  ibqryselect.open;
  if not ibqryselect.IsEmpty then
   begin
     if ibqryselect.Fields[0].Value <> null 
      then ZemUch.USe_.Value := ibqryselect.Fields[0].Value;
     if ibqryselect.Fields[1].Value <> null 
      then ZemUch.USe_.UseFact := ibqryselect.Fields[1].Value;    
    Result := True;
   end
  else 
    Result :=false ;
  ibqryselect.Close;  
 end;
end;

function tFunctional.ReadLocal_prefix(vKn:string):Integer;
 begin
   // ������ lp
   ReadLocal_prefix := StrToInt(Copy(vKn,1,2)+copy(vKn,4,2)+'00');
 end;

 function tFunctional.FindParent_id(vKn : string):double;
 var vKnPred : string;
 begin
   // �� ������������ ������ ����� ������� � ���� ID
   vKnPred := Copy(vKn,1,13);
   with dm2 do
   begin
   ibqryFind.Database :=  myI.Mydb;  //(myI as TAisUMZForMgis).GetMyDB;
   ibqryFind.sql.clear;
   ibqryFind.sql.add(_sqlFind_KN);
   ibqryFind.Params[0].AsString := vKnPred;
//   ibqryFind.Params[1].AsDate := date();
   ibqryFind.Open;
   if ibqryFind.IsEmpty then  Result :=0 else Result := ibqryFind.Fields[0].AsFloat;
   ibqryFind.Close;
   end;
   // ���������� 0 - ����
   //            ID - ����
 end;

function tFunctional.FindRightTypeID(right_cod: string;   owner_type: integer; owner_name:string): Double;
begin
 if right_cod = '001001000000' then
   begin
    if owner_type = 1 then Result := 43 // ��� ��� ���
    else
    if owner_type = 2 then Result := 44
    else
    if owner_type = 3 then
      begin
        if Pos(UpperCase('���������'), UpperCase(owner_name)) <> 0 then Result := 48
        else
        if (Pos(UpperCase('�������������'), uppercase(owner_name)) <> 0) or (Pos(UpperCase('�����'), uppercase(owner_name)) <> 0) or (Pos(UpperCase('�����������'), uppercase(owner_name)) <> 0) then Result := 14
        else
        if Pos(UpperCase('���������� ���������'), UpperCase(owner_name)) <> 0 then Result := 12
        else
        if Pos(UpperCase('�������'), UpperCase(owner_name)) <> 0 then Result := 13;
      end;
   end
 else
 if right_cod = '001002000000' then    Result := 45
 else
 if right_cod = '001003000000' then    Result := 46
 else
 if right_cod = '001006000000' then    Result := 3
 else
 if right_cod = '001007000000' then    Result := 4
 else
 if right_cod = '022006000000' then    Result := 7
 else
 if right_cod = '022009000000' then    Result := 8
 else
    Result := 0;
end;

function tFunctional.LandGenDocId(vKN: string): Double;
var ii:Double;
 begin
  with dm2 do
   begin
    ii := StrTofloat(Copy(vKn,1,2) + Copy(vKn,4,2)+'00000000000');

    ibqryGenId.Database :=  myI.Mydb;
    ibqryGenId.sql.clear;
    ibqryGenId.sql.add(_sqlGenDocId);
    ibqryGenId.Open;
    Result := ii + ibqryGenId.Fields[0].AsFloat;
    ibqryGenId.Close;
   end;

end;

function tFunctional.LandGenId(vKN: string):Double;
 var ii:Double;
 begin
  with dm2 do
   begin
    ii := StrTofloat(Copy(vKn,1,2) + Copy(vKn,4,2)+'00000000000');

    ibqryGenId.Database :=  myI.Mydb;
    ibqryGenId.sql.clear;
    ibqryGenId.sql.add(_sqlGenLandId);
    ibqryGenId.Open;
    Result := ii + ibqryGenId.Fields[0].AsFloat;
    ibqryGenId.Close;
   end;
 end;


function tFunctional.LandGenLandRightId(vKN: string): Double;
var ii:Double;
 begin
  with dm2 do
   begin
    ii := StrTofloat(Copy(vKn,1,2) + Copy(vKn,4,2)+'00000000000');

    ibqryGenId.Database :=  myI.Mydb;
    ibqryGenId.sql.clear;
    ibqryGenId.sql.add(_sqlGenLandRightId);
    ibqryGenId.Open;
    Result := ii + ibqryGenId.Fields[0].AsFloat;
    ibqryGenId.Close;
   end;

end;

function tFunctional.LandGenRightDocId(vKN: string): Double;
var ii:Double;
 begin
  with dm2 do
   begin
    ii := StrTofloat(Copy(vKn,1,2) + Copy(vKn,4,2)+'00000000000');

    ibqryGenId.Database :=  myI.Mydb;
    ibqryGenId.sql.clear;
    ibqryGenId.sql.add(_sqlGenrightDocId);
    ibqryGenId.Open;
    Result := ii + ibqryGenId.Fields[0].AsFloat;
    ibqryGenId.Close;
   end;
end;

function tFunctional.LandGenRightId(vKN: string): Double;
var ii:Double;
 begin
  with dm2 do
   begin
    ii := StrTofloat(Copy(vKn,1,2) + Copy(vKn,4,2)+'00000000000');

    ibqryGenId.Database :=  myI.Mydb;
    ibqryGenId.sql.clear;
    ibqryGenId.sql.add(_sqlGenRightId);
    ibqryGenId.Open;
    Result := ii + ibqryGenId.Fields[0].AsFloat;
    ibqryGenId.Close;
   end;

end;

function tFunctional.LandGenRightOwnerId(vKN: string): Double;
var ii:Double;
 begin
  with dm2 do
   begin
    ii := StrTofloat(Copy(vKn,1,2) + Copy(vKn,4,2)+'00000000000');

    ibqryGenId.Database :=  myI.Mydb;
    ibqryGenId.sql.clear;
    ibqryGenId.sql.add(_sqlGenRightOwnerId);
    ibqryGenId.Open;
    Result := ii + ibqryGenId.Fields[0].AsFloat;
    ibqryGenId.Close;
   end;

end;

function tFunctional.LandGenRightRegId(vKN: string): Double;
var ii:Double;
 begin
  with dm2 do
   begin
    ii := StrTofloat(Copy(vKn,1,2) + Copy(vKn,4,2)+'00000000000');

    ibqryGenId.Database :=  myI.Mydb;
    ibqryGenId.sql.clear;
    ibqryGenId.sql.add(_sqlGenryteregId);
    ibqryGenId.Open;
    Result := ii + ibqryGenId.Fields[0].AsFloat;
    ibqryGenId.Close;
   end;

end;

function tFunctional.FinfDocTYpeByCode(vCode:string):double;
begin
  if vCode = '558501030300' then Result := 1767
  else
  if ((vCode = '558403090000') or (vCode = '558403110000') or (vCode = '558403100000') )then Result := 1769
  else
  if vCode = '558401070100' then Result := 1771
  else
  if vCode = '558401010103' then Result := 1772
  else
  if vCode = '558401010101' then Result := 1773
  else
  if vCode = '558401010102' then Result := 1774
  else
  if vCode = '558401010216' then Result := 1777
  else
  if vCode = '558301040000' then Result := 1787
  else
  if vCode = '558401050400' then Result := 1790
  else
  if vCode = '558403010000' then Result := 1791
  else
  if ((vCode = '558401030100') or (vCode = '558401030200')) then Result := 1792
  else
  if vCode = '558213010000' then Result := 1794
  else
  if vCode = '558301130000' then Result := 1796
  else
  if vCode = '558401010209' then Result := 3
  else
   Result := 0;
end;

{function tFunctional.InsertDocArenda(var ZemRight: TZemRight; vKn:string): Boolean;
begin

end;  }


function tFunctional.InsertDoc(var ZemRight: TZemRight; vKn:string): Boolean;
var docId : Double;
begin
 with dm2 do
 begin
   ZemRight.Doc.doc_type := FinfDocTYpeByCode(ZemRight.Doc.doc_code);
   docId :=LandGenDocId(vKn) ;
   try
   ibqryInsert.Database := myI.Mydb;
   ibqryInsert.sql.clear;
   ibqryInsert.sql.add(_sqlInsertDoc);
   ibqryInsert.Params[0].AsFloat := docId;
   if ZemRight.Doc.doc_type <> 0
       then  ibqryInsert.Params[1].Value := ZemRight.Doc.doc_type
       else  ibqryInsert.Params[1].Value := null;
   ibqryInsert.Params[2].Value := ZemRight.Doc.doc_seria;
   ibqryInsert.Params[3].Value := ZemRight.Doc.doc_num;
   ibqryInsert.Params[4].Value := ZemRight.Doc.doc_date;
   ibqryInsert.Params[5].Value := null;
   ibqryInsert.Params[6].Value := ZemRight.Doc.doc_name;
   ibqryInsert.Params[7].AsDate := Date();
   ibqryInsert.Params[8].AsInteger := ReadLocal_prefix(vKn);
   ibqryInsert.ExecSQL;
   ibqryInsert.Close;
   Result := True;
   except
    result := False;
   end;
 end;
// if Result =False then form4.mmo1.Lines.Add('insertDoc false') else form4.mmo1.Lines.Add('insertDoc true');
 if Result =False then Form4.WriteTofile(ff,'������� ���������', 'false') else Form4.WriteTofile(ff,'������� ���������', '��');

 with dm2 do
 begin
   try
   ibqryInsert.Database := myI.Mydb;
   ibqryInsert.sql.clear;
   ibqryInsert.sql.add(_sqlInsertRightDoc);
   ibqryInsert.Params[0].Value := LandGenRightDocId(vKn);
   ibqryInsert.Params[1].Value := ZemRight.Right_ID;
   ibqryInsert.Params[2].Value := docId;
   ibqryInsert.Params[3].AsInteger := zemUch.zu_local_prefix;
   ibqryInsert.Params[4].AsDate :=  Date();
   ibqryInsert.ExecSQL;
   ibqryInsert.Close;
   Result := True;
   except
    result := False;
   end;
 end;
// if Result =False then form4.mmo1.Lines.Add('insertDocright false') else form4.mmo1.Lines.Add('insertDocRight true');
  if Result =False then form4.WriteTofile(ff,'������� ����� �������� � �����', '���') else form4.WriteTofile(ff,'������� ����� �������� � �����', '��');
end;

function tFunctional.InsertKvartal(vKn:string):Double;
 var vKnPred, vKnraion : string;
     vId, vPredID : Double;
 begin
   // ��������� ������� � ���������� ID
   vKnPred  :=Copy(vKn,1,13);
   vKnraion :=Copy(vKn,1,5);

   with dm2 do
   begin
    ibqryFind.Database :=  myI.Mydb;  //(myI as TAisUMZForMgis).GetMyDB;
    ibqryFind.sql.clear;
    ibqryFind.sql.add(_sqlFind_KN);
    ibqryFind.Params[0].AsString := vKnRaion;
    ibqryFind.Params[1].AsDate := date();
    ibqryFind.Open;
    if ibqryFind.IsEmpty then  vPredID :=0 else vPredID := ibqryFind.Fields[0].AsFloat;
    ibqryFind.Close;

    vId := LandGenId(vKN);

    try
    ibqryInsert.Database := myI.Mydb;
    ibqryInsert.sql.clear;
    ibqryInsert.sql.add(_sqlInsertPredok);
    ibqryInsert.Params[0].Value := vId;
    ibqryInsert.Params[1].Value := vPredID;
    ibqryInsert.Params[2].Value := vKnpred;
    ibqryInsert.Params[3].Value := ReadLocal_prefix(vKn);
    ibqryInsert.Params[4].Value := Date();
    ibqryInsert.ExecSQL;
    ibqryInsert.Close;
    Result := vId;
    except
     Result := 0;
    end;
   end;
 //  if Result =0 then form4.mmo1.Lines.Add('InsertKvartal false') else form4.mmo1.Lines.Add('InsertKvartal true');
   if Result =0 then form4.WriteTofile(ff,'������� ��������', '���') else form4.WriteTofile(ff,'������� ��������',' ��');
 end;

function tFunctional.insertLand(var zemUch: TZemUch): Boolean;
begin
 with dm2 do
 begin
   zemUch.zu_ID := LandGenId(zemUch.zu_cad_num);
   ZemUch.Area.Unit_:= FindUnitbyCode(zemUch.Area.UnitCod);
   try
   ibqryInsert.Database := myI.Mydb;
   ibqryInsert.sql.clear;
   ibqryInsert.sql.add(_sqlInsertLand);
   ibqryInsert.Params[0].Value := zemUch.zu_ID;
   ibqryInsert.Params[1].Value := ZemUch.zu_pred_id;
   ibqryInsert.Params[2].Value := zemUch.zu_cad_num;
   ibqryInsert.Params[3].Value := zemUch.Area.Value;
   ibqryInsert.Params[4].Value := ZemUch.Area.Unit_;
   ibqryInsert.Params[5].Value := zemUch.zu_local_prefix;
   ibqryInsert.Params[6].Value := Date();
   ibqryInsert.ExecSQL;
   ibqryInsert.Close;
   Result := True;
   except
    result := False;
   end;
 end;
 //if Result =False then form4.mmo1.Lines.Add('insertLand false') else form4.mmo1.Lines.Add('insertLand true');
 if Result =False then form4.WriteTofile(ff,'������� ��������� �������� ��',' ���') else form4.WriteTofile(ff,'������� ��������� �������� ��',' ��');
end;

function tFunctional.LandAdrGenId(vKN: string):Double;
 var ii:Double;
 begin
  with dm2 do
   begin
    ii := StrTofloat(Copy(vKn,1,2) + Copy(vKn,4,2)+'00000000000');

    ibqryGenid.Database :=  myI.Mydb;
    ibqryGenid.sql.clear;
    ibqryGenid.sql.add(_sqlGenLandAdrId);
    ibqryGenid.Open;
    Result := ii + ibqryGenid.Fields[0].AsFloat;
    ibqryGenid.Close;
   end;
 end;

function  tFunctional.FindAdrbyKladr(vkladr:string): Double;
begin
  with dm2 do
  begin
    ibqryFind.Database := myI.Mydb;
    ibqryFind.sql.clear;
    ibqryFind.sql.add(_sqlFind_AdrbyKladr);
    ibqryFind.Params[0].Value := vkladr;
    ibqryFind.Open;
    if ibqryFind.IsEmpty then result := 0 else Result := ibqryFind.Fields[0].AsFloat;
    ibqryFind.Close;
  end;
end;


function tFunctional.FindCategorybyCode(vUCode: string): Double;
begin
  if vUCode = '003001000000' then Result := 1
  else
  if vUCode = '003002000000' then Result := 2
  else
  if vUCode = '003003000000' then Result := 3
  else
  if vUCode = '003004000000' then Result := 4
  else
  if vUCode = '003005000000' then Result := 5
  else
  if vUCode = '003006000000' then Result := 6
  else
  if vUCode = '003007000000' then Result := 7
  else
  if vUCode = '003008000000' then Result := 10
  else
    Result := 0;
end;

function tFunctional.insertLandAdr(var zemUch: TZemUch): Boolean;
var ii : Double; vKLadr:string;
begin
 with dm2 do
 begin
   ii := LandAdrGenId(zemUch.zu_cad_num);   // id
   ZemUch.Address.VAlue := FindAdrbyKladr(zemUch.address.Adrkladr);
   if ZemUch.Address.VAlue =0 then  ZemUch.Address.VAlue  := FindAdrbyKladr(copy(zemUch.address.Adrkladr,1,5)+'00000000'); // ���� ������ �� ������ �� ����������� �����

   try
   ibqryInsert.Database := myI.Mydb;
   ibqryInsert.sql.clear;
   ibqryInsert.sql.add(_sqlInsertLandAdr);
   ibqryInsert.Params[0].Value := ii;
   ibqryInsert.Params[1].Value := zemUch.zu_ID;
   if ZemUch.Address.VAlue <> 0
     then
       ibqryInsert.Params[2].Value := ZemUch.Address.VAlue
     else
       ibqryInsert.Params[2].Value := null ;
   ibqryInsert.Params[3].Value := zemUch.address.AdrNote;
   ibqryInsert.Params[4].Value := zemUch.zu_local_prefix;
   ibqryInsert.Params[5].Value := Date();
   ibqryInsert.ExecSQL;
   ibqryInsert.Close;
   Result := True;
   except
   result := False;
   end;
 end;
// if Result =False then form4.mmo1.Lines.Add('insertLandAdr false') else form4.mmo1.Lines.Add('insertLandAdr true');
  if Result =False then form4.WriteTofile(ff,'������� ������ ��',' ���') else form4.WriteTofile(ff,'������� ������ ��',' ��');
end;

function tFunctional.LandcharGenId(vKN: string):Double;
 var ii:Double;
 begin
  with dm2 do
   begin
    ii := StrTofloat(Copy(vKn,1,2) + Copy(vKn,4,2)+'00000000000');

    ibqryGenid.Database :=  myI.Mydb;
    ibqryGenid.sql.clear;
    ibqryGenid.sql.add(_sqlGenLandCharid);
    ibqryGenid.Open;
    Result := ii + ibqryGenid.Fields[0].AsFloat;
    ibqryGenid.Close;
   end;
 end;

function tFunctional.insertLandChar(var zemUch: TZemUch): Boolean;
begin
  zemUch.cadcost.unit_ := FindUnitbyCode(zemUch.cadcost.unitcod);
  with dm2 do
  begin
    try
       ibqryInsert.Database := myI.Mydb;
       ibqryInsert.sql.clear;
       ibqryInsert.sql.add(_sqlInsertLandChar);
       ibqryInsert.Params[0].Value := LandcharGenId(zemUch.zu_cad_num);
       ibqryInsert.Params[1].Value := zemUch.zu_ID;
       ibqryInsert.Params[2].Value := Date();
       if  zemUch.cadCost.value <> 0 then ibqryInsert.Params[3].Value := zemUch.cadCost.value
                                     else ibqryInsert.Params[3].Value := null;
       if zemUch.cadCost.unit_ <> 0 then  ibqryInsert.Params[4].Value := zemUch.cadCost.unit_ 
                                    else  ibqryInsert.Params[4].Value := null;
       ibqryInsert.Params[5].Value := zemUch.zu_local_prefix;
       ibqryInsert.ExecSQL;
       ibqryInsert.Close;
       Result := True;
       except
       result := False;
       end;
  end;
//  if Result =False then form4.mmo1.Lines.Add('insertLandChar false') else form4.mmo1.Lines.Add('insertLandChar true');
  if Result =False then form4.WriteTofile(ff,'������� ������������� ��',' ���') else form4.WriteTofile(ff,'������� ������������� ��',' ��');
end;

function tFunctional.InsertLandRight(var ZemRight: TZemRight; vKn:string): double;
var lrId : Double;
begin
 with dm2 do
 begin
   try
   lrId := LandGenLandRightId(vKn);
   ibqryInsert.Database := myI.Mydb;
   ibqryInsert.sql.clear;
   ibqryInsert.sql.add(_sqlInsertLandRight);
   ibqryInsert.Params[0].Value := lrId;
   ibqryInsert.Params[1].Value := ZemRight.Right_ID;
   ibqryInsert.Params[2].Value := ZemRight.land_id;
   ibqryInsert.Params[3].Value := ReadLocal_prefix(vKn);
   ibqryInsert.Params[4].Value := Date();
   ibqryInsert.ExecSQL;
   ibqryInsert.Close;
   Result := lrId;
   except
   result := 0;
   end;
 end;
 //if Result =0 then form4.mmo1.Lines.Add('insertLandRight false') else form4.mmo1.Lines.Add('insertLandright true');
  if Result =0 then form4.WriteTofile(ff,'������� ����� �� � �����',' ���') else form4.WriteTofile(ff,'������� ����� �� � �����',' ��');
end;

function tFunctional.LandUseGenId(vKN: string):Double;
 var ii:Double;
 begin
  with dm2 do
   begin
    ii := StrTofloat(Copy(vKn,1,2) + Copy(vKn,4,2)+'00000000000');

    ibqryGenid.Database :=  myI.Mydb;
    ibqryGenid.sql.clear;
    ibqryGenid.sql.add(_sqlGenLandUseID);
    ibqryGenid.Open;
    Result := ii + ibqryGenid.Fields[0].AsFloat;
    ibqryGenid.Close;
   end;
 end;


function tFunctional.FindUnitbyCode(vUCode: string): Double;
begin
  if ((vUCode = '012002001000') or (vUCode = '055')) then Result := 19  //��.�.
  else
  if  (vUCode = '058') then Result := 20  //��
  else
  if ((vUCode = '012002002000') or (vUCode = '059')) then Result := 21  //��
  else
  if ((vUCode = '012003000000') or (vUCode = '061')) then Result := 22  // ��.��
  else
  if ((vUCode = '012004001000') or (vUCode = '383')) then Result := 257  //���
  else
  if ((vUCode = '012004002000') or (vUCode = '384'))then Result := 258  //��� ���
  else
  if ((vUCode = '012004003000') or (vUCode = '385')) then Result := 259  //��� ���
  else
    Result := 0;
end;

function tFunctional.FindUseIDbyCode(vCode:string):Double;
 begin
  with dm2 do
  begin
    ibqryFind.Database := myI.Mydb;
    ibqryFind.sql.clear;
    ibqryFind.sql.add(_sqlFindUseIdbyCode);
    ibqryFind.Params[0].Value := vCode;
    ibqryFind.Open;
    if ibqryFind.IsEmpty then result := 0 else Result := ibqryFind.Fields[0].AsFloat;
    ibqryFind.Close;
  end;
 end;

 function tFunctional.insertLandUSE(var zemUch: TZemUch): Boolean;
  begin
    with dm2 do
    begin

      zemUch.use_.value := FindUseIDbyCode(zemUch.Use_.Usecode);
      if zemUch.use_.value = 0 then zemUch.use_.value := InsertNewUse(zemUch);


      try
       ibqryInsert.Database := myI.Mydb;
       ibqryInsert.sql.clear;
       ibqryInsert.sql.add(_sqlInsertLandUse);
       ibqryInsert.Params[0].Value := LandUseGenId(zemUch.zu_cad_num);
       ibqryInsert.Params[1].Value := zemUch.zu_ID;
       if zemUch.use_.value <> 0
        then
         ibqryInsert.Params[2].Value := zemUch.use_.value
        else
         ibqryInsert.Params[2].Value := null ;
       ibqryInsert.Params[3].Value := zemUch.use_.usefact;
       ibqryInsert.Params[4].Value := Date();
       ibqryInsert.Params[5].Value := zemUch.zu_local_prefix;
       ibqryInsert.ExecSQL;
       ibqryInsert.Close;
       Result := True;
       except
       result := False;
       end;
    end;
    //if Result =False then form4.mmo1.Lines.Add('insertLandUSE false') else form4.mmo1.Lines.Add('insertLandUSE true');
    if Result =False then form4.WriteTofile(ff,'������� ���� ������������� ��',' ���') else form4.WriteTofile(ff,'������� ���� ������������� ��',' ��');
  end;


function  tFunctional.InsertZemUch(var zemUch: TZemUch):double;     // ���������� ID ������� ��� �����
var i:Integer;
 begin
   try
   i:=0;
   // ���������� ��������� �������
   zemUch.zu_local_prefix := ReadLocal_prefix(zemUch.zu_cad_num);
   // ���������� ��������
   zemUch.zu_pred_id := FindParent_id(zemUch.zu_cad_num);
   if zemUch.zu_pred_id = 0 then zemUch.zu_pred_id:= InsertKvartal(ZemUch.zu_cad_num);
   //��������� ��������� �������

      if insertLand(zemUch)
      then
        begin
         i:=i+1;
         if insertLandAdr(zemUch) then  i:=i+1;
         if insertLandChar(zemUch) then i:=i+1;
         if insertLandUse(zemuch) then  i:=i+1;
     // commitratainig
         Result := zemuch.zu_id
        end;
     myI.MyTrAc.CommitRetaining;
   except
     myI.MyTrAc.RollbackRetaining;
     Result := 0; // - �� �����!
   end;
  // if Result =0 then form4.mmo1.Lines.Add('InsertZemUch false'+ ' '+ inttostr(i)) else form4.mmo1.Lines.Add('InsertZemUch true'+ ' '+ inttostr(i));
     if Result =0 then form4.WriteTofile(ff,'������� �� �����:'+ ' '+ inttostr(i),'') else form4.WriteTofile(ff,'������� �� �����:'+ ' '+ inttostr(i),'');
 end;


function tFunctional.UpdateLandArea(ZemUch:TZemUch):Boolean;
begin
  with dm2 do
  begin
    try
     ibqryUpdate.Database := myI.Mydb;
     ibqryUpdate.Sql.clear;
     ibqryUpdate.Sql.Add(_sqlUPDATE_LANDAREA);
     ibqryUpdate.ParamByName('p1').Value := ZemUch.Area.Value;
     ibqryUpdate.ParamByName('p2').Value := ZemUch.zu_id;
     ibqryUpdate.ExecSQL;
     ibqryUpdate.Close;
     Result := True;
    except
     Result := False;
    end;
  end;
  //if Result =False then form4.mmo1.Lines.Add('UpdateLandArea false') else form4.mmo1.Lines.Add('UpdateLandArea true');
  if Result =False then form4.WriteTofile(ff,'���������� ������� ��',' ���') else form4.WriteTofile(ff,'���������� ������� ��',' ��');
end;

function tFunctional.UpdateLandAreaUnit(ZemUch:TZemUch): Boolean;
begin

  ZemUch.Area.Unit_ := FindUnitbyCode(ZemUch.Area.UnitCod);

  with dm2 do
  begin
    try
     ibqryUpdate.Database := myI.Mydb;
     ibqryUpdate.Sql.clear;
     ibqryUpdate.Sql.Add(_sqlUPDATE_LANDAREAunit);
     ibqryUpdate.ParamByName('p1').Value := ZemUch.Area.Unit_;
     ibqryUpdate.ParamByName('p2').Value := ZemUch.zu_id;
     ibqryUpdate.ExecSQL;
     ibqryUpdate.Close;
     Result := True;
    except
     Result := False;
    end;
  end;
  //if Result =False then form4.mmo1.Lines.Add('UpdateLandAreaUnit false') else form4.mmo1.Lines.Add('UpdateLandAreaUnit true');
  if Result =False then form4.WriteTofile(ff,'���������� ���� ������� ��',' ���') else form4.WriteTofile(ff,'���������� ���� ������� ��',' ��');

end;


function tFunctional.UpdateLandRight(ZemRight: TZemRight): Boolean;
begin
 { with dm2 do
  begin
    try
     ibqryUpdate.Database := myI.Mydb;
     ibqryUpdate.Sql.clear;
     ibqryUpdate.Sql.Add();
     ibqryUpdate.ParamByName('p1').Value := ;
     ibqryUpdate.ParamByName('p2').Value := ;
     ibqryUpdate.ExecSQL;
     ibqryUpdate.Close;
     Result := True;
    except
     Result := False;
    end;
  end;
  //if Result =False then form4.mmo1.Lines.Add('UpdateLandAreaUnit false') else form4.mmo1.Lines.Add('UpdateLandAreaUnit true');
  if Result =False then form4.WriteTofile(ff,'',' ���') else form4.WriteTofile(ff,'',' ��');     }
end;



function  tFunctional.CheckCountRihtOwners(ZemRight:TZemRight):integer;
begin
  with dm2 do
   begin
   ibqryCheck.Database :=  myI.Mydb;  //(myI as TAisUMZForMgis).GetMyDB;
   ibqryCheck.sql.clear;
   ibqryCheck.sql.add(_sqlCheckContRightOwners);
   ibqryCheck.Params[0].AsFloat := ZemRight.lright_id;
   ibqryCheck.Open;
   if ibqryCheck.IsEmpty then  Result :=0 else Result := ibqryCheck.Fields[0].Value;
   ibqryCheck.Close;
   end;
end;


function tFunctional.UpdateRightOwner(ZemRight: TZemRight; vKn :string): Boolean;
var ow_id : Double;
begin
  with dm2 do
  begin
    try
     ibqryUpdate.Database := myI.Mydb;
     ibqryUpdate.Sql.clear;
     ibqryUpdate.Sql.Add(_sqlUpdateRightOwner1);
     if (ZemRight.owner.ownerCount >1)
     then
      begin
        ibqryUpdate.ParamByName('p1').Value := createPerson(ZemRight.owner.owner,ZemRight.owner.ownerCount,vKn);
        ibqryUpdate.ParamByName('p2').Value := null;
      end
     else
       begin
           if (ZemRight.owner.owner_type = 1) and (ZemRight.owner.ownerCount =1)
           then
             begin
                ow_id := FindPersonbyName(ZemRight.owner.owner);
                if ow_id <> 0
                 then
                   ibqryUpdate.ParamByName('p1').Value := ow_id
                 else
                   ibqryUpdate.ParamByName('p1').Value := createPerson(ZemRight.owner.owner,ZemRight.owner.ownerCount,vKn);
                ibqryUpdate.ParamByName('p2').Value := null;
             end
           else
           if (ZemRight.owner.owner_type <>1) and (ZemRight.owner.ownerCount =1)
           then
             begin
                ow_id := FindEnterprisebyName(ZemRight.owner.owner);
                ibqryUpdate.ParamByName('p1').Value := null;
                if ow_id <> 0
                 then
                   ibqryUpdate.ParamByName('p2').Value := ow_id
                 else
                   ibqryUpdate.ParamByName('p2').Value := createEnterprise(ZemRight.owner.owner,vKn) ;
             end;
       end;
     if ZemRight.owner.owner_type = 1
       then ibqryUpdate.ParamByName('p3').Value := '���������� ����'
       else ibqryUpdate.ParamByName('p3').Value := '����������� ����';

     ibqryUpdate.ParamByName('p4').Value := null;
     ibqryUpdate.ParamByName('p5').Value := null;
     if (ZemRight.owner.ownerCount >1)
     then
      begin
        ibqryUpdate.ParamByName('p6').Value := 1;
        ibqryUpdate.ParamByName('p7').Value := 1;
      end
     else
       begin
         ibqryUpdate.ParamByName('p6').Value := ZemRight.owner.owner_drob1;
         ibqryUpdate.ParamByName('p7').Value := ZemRight.owner.owner_drob2;
       end ;
     ibqryUpdate.ParamByName('p8').Value := ZemRight.lright_id;
     ibqryUpdate.ExecSQL;
     ibqryUpdate.Close;
     Result := True;
    except
     Result := False;
    end;
  end;
 // if Result =False then form4.mmo1.Lines.Add('UpadteRightOwner false') else form4.mmo1.Lines.Add('UpadteRightOwner true');
    if Result =False then form4.WriteTofile(ff,'���������� ����� ������������ � �����',' ���') else form4.WriteTofile(ff,'���������� ����� ������������ � �����',' ��');
end;

function  tFunctional.findRightRegBYID(rId:Double; type_reg:integer): boolean;
begin
 with dm2 do
   begin
   ibqryFind.Database :=  myI.Mydb;  //(myI as TAisUMZForMgis).GetMyDB;
   ibqryFind.sql.clear;
   ibqryFind.sql.add(_sqlFindRightregistrationbyID);
   ibqryFind.Params[0].AsFloat := rID;
   ibqryFind.Params[1].Asinteger := type_reg;
   ibqryFind.Open;
   if ibqryFind.IsEmpty then  Result :=false else Result := true;
   ibqryFind.Close;
   end;
end;

function tFunctional.UpdateRightReg(ZemRight: TZemRight; vKn:string): Boolean;
var type_reg:integer;
begin
 // �������� �� �������������
 if ZemRight.right_cad_uchet then
   begin
      type_reg := 1;
      if not findRightRegBYID(ZemRight.Right_ID, type_reg)
        then InsertRightReg(ZemRight, vKn,type_reg);
   end;
 if ZemRight.right_ufrs then
   begin
      type_reg := 2;
      if not findRightRegBYID(ZemRight.Right_ID, type_reg)
        then InsertRightReg(ZemRight, vKn,type_reg);
   end;

end;

function tFunctional.UpdateDoc(ZemRight: TZemRight): Boolean;
begin
  // ����� ���� �������
end;

function tFunctional.UpdateLandAdrID(ZemUch:TZemUch): Boolean;
var ii : Double;
begin
  ZemUch.Address.Value := FindAdrbyKladr(ZemUch.Address.AdrKladr);
  if  ZemUch.Address.Value = 0 then   ZemUch.Address.Value := FindAdrbyKladr(copy(zemUch.address.Adrkladr,1,5)+'00000000');
  
  with dm2 do
  begin
    try
     ibqryUpdate.Database := myI.Mydb;
     ibqryUpdate.Sql.clear;
     ibqryUpdate.Sql.Add(_sqlUPDATE_LANDAdr);
     if ZemUch.Address.Value <> 0 then  ibqryUpdate.ParamByName('p1').Value := ZemUch.Address.Value
                                  else  ibqryUpdate.ParamByName('p1').Value := null  ;
     ibqryUpdate.ParamByName('p2').Value := ZemUch.zu_id;
     ibqryUpdate.ExecSQL;
     ibqryUpdate.Close;
     Result := True;
    except
     Result := False;
    end;
  end;
  //if Result =False then form4.mmo1.Lines.Add('UpdateLandAdrID false') else form4.mmo1.Lines.Add('UpdateLandAdrID true');
  if Result =False then form4.WriteTofile(ff,'���������� ������ ��',' ���') else form4.WriteTofile(ff,'���������� ������ ��',' ��');
end;

function tFunctional.UpdateLandAdrNote(ZemUch:TZemUch): Boolean;
var ii : Double;
begin
   with dm2 do
  begin
    try
     ibqryUpdate.Database := myI.Mydb;
     ibqryUpdate.Sql.clear;
     ibqryUpdate.Sql.Add(_sqlUPDATE_LANDAdrnote);
     ibqryUpdate.ParamByName('p1').Value := ZemUch.Address.AdrNote;
     ibqryUpdate.ParamByName('p2').Value := ZemUch.zu_id;
     ibqryUpdate.ExecSQL;
     ibqryUpdate.Close;
     Result := True;
    except
     Result := False;
    end;
  end;
  // if Result =False then form4.mmo1.Lines.Add('UpdateLandAdrNote false') else form4.mmo1.Lines.Add('UpdateLandAdrNote true');
   if Result =False then form4.WriteTofile(ff,'���������� ���������� ������ ��',' ���') else form4.WriteTofile(ff,'���������� ���������� ������ ��',' ��');
end;


function tFunctional.UpdateLandCharCadCost(ZemUch: TZemUch): boolean;
begin
  with dm2 do
  begin
    try
     ibqryUpdate.Database := myI.Mydb;
     ibqryUpdate.Sql.clear;
     ibqryUpdate.Sql.Add(_sqlUPDATE_LANDCadCost);
     ibqryUpdate.ParamByName('p1').Value := ZemUch.CadCost.Value;
     ibqryUpdate.ParamByName('p2').Value := ZemUch.zu_id;
     ibqryUpdate.ExecSQL;
     ibqryUpdate.Close;
     Result := True;
    except
     Result := False;
    end;
  end;
  //if Result =False then form4.mmo1.Lines.Add('UpdateLandCharCadCost false') else form4.mmo1.Lines.Add('UpdateLandCharCadCost true');
  if Result =False then form4.WriteTofile(ff,'���������� ����������� ��������� ��',' ���') else form4.WriteTofile(ff,'���������� ����������� ��������� ��',' ��');
end;

function tFunctional.UpdateLandCharCategory(ZemUch: TZemUch): boolean;
begin
  with dm2 do
  begin
    try
     ibqryUpdate.Database := myI.Mydb;
     ibqryUpdate.Sql.clear;
     ibqryUpdate.Sql.Add(_sqlUPDATE_LANDCategory);
     ibqryUpdate.ParamByName('p1').Value := FindCategorybyCode(ZemUch.Category.Code);
     ibqryUpdate.ParamByName('p2').Value := ZemUch.zu_id;
     ibqryUpdate.ExecSQL;
     ibqryUpdate.Close;
     Result := True;
    except
     Result := False;
    end;
  end;
 // if Result =False then form4.mmo1.Lines.Add('UpdateLandCharCategory false') else form4.mmo1.Lines.Add('UpdateLandCharCategory true');
 if Result =False then form4.WriteTofile(ff,'���������� ��������� ��',' ���') else form4.WriteTofile(ff,'���������� ��������� ��',' ��');
end;

function tFunctional.UpdateLandCharUnit(ZemUch: TZemUch): boolean;
begin
  with dm2 do
  begin
    try
     ibqryUpdate.Database := myI.Mydb;
     ibqryUpdate.Sql.clear;
     ibqryUpdate.Sql.Add(_sqlUPDATE_LANDCadCostUnit);
     if FindUnitbyCode(ZemUch.CadCost.UnitCod)  <> 0 then   ibqryUpdate.ParamByName('p1').Value := FindUnitbyCode(ZemUch.CadCost.UnitCod)
                                                     else   ibqryUpdate.ParamByName('p1').Value := null;
     ibqryUpdate.ParamByName('p2').Value := ZemUch.zu_id;
     ibqryUpdate.ExecSQL;
     ibqryUpdate.Close;
     Result := True;
    except
     Result := False;
    end;
  end;
  // if Result =False then form4.mmo1.Lines.Add('UpdateLandCharUnit false') else form4.mmo1.Lines.Add('UpdateLandCharUnit true');
   if Result =False then form4.WriteTofile(ff,'���������� ���� ���.��������� ��',' ���') else form4.WriteTofile(ff,'���������� ���� ���. ��������� ��',' ��');
end;


function tFunctional.UpdateLandUseValue(ZemUch:TZemUch): Boolean;
var useID :Double;
begin
  with dm2 do
  begin
    try
     useId := FindUseIDbyCode(ZemUch.USe_.UseCode);
     ibqryUpdate.Database := myI.Mydb;
     ibqryUpdate.SQL.Clear;
     ibqryUpdate.SQL.Add(_sqlUPDATE_LANDUse);
     if useID <> 0 then ibqryUpdate.Parambyname('p1').Value := useId
                   else ibqryUpdate.Parambyname('p1').Value := InsertNewUse(zemUch);
     ibqryUpdate.ParamByName('p2').AsFloat := ZemUch.zu_ID;
     ibqryUpdate.ExecSQL;
     ibqryUpdate.Close;
     Result := True;
    except
     Result := False;
    end;
  end;
 // if Result =False then form4.mmo1.Lines.Add('UpdateLandUseValue false') else form4.mmo1.Lines.Add('UpdateLandUseValue true');
  if Result =False then form4.WriteTofile(ff,'���������� ���� ������������� ��',' ���') else form4.WriteTofile(ff,'���������� ���� ������������� ��',' ��');
end;

function  tFunctional.FindRightID(lright_id:double):Double;
begin
  with dm2 do
   begin
   ibqryFind.Database :=  myI.Mydb;  //(myI as TAisUMZForMgis).GetMyDB;
   ibqryFind.sql.clear;
   ibqryFind.sql.add(_sqlFindRightID);
   ibqryFind.Params[0].AsFloat := lright_id;
   ibqryFind.Open;
   if ibqryFind.IsEmpty then  Result :=0 else Result := ibqryFind.Fields[0].Value;
   ibqryFind.Close;
   end;
end;

function tFunctional.UpdateRight(var ZemRight: TZemRight): Boolean;
begin
   ZemRight.Right_ID := FindRightID(ZemRight.lright_id);

   with dm2 do
   begin
   try
   ibqryUpdate.Database :=  myI.Mydb;  //(myI as TAisUMZForMgis).GetMyDB;
   ibqryUpdate.sql.clear;
   ibqryUpdate.sql.add(_sqlUpdateRight);

   ibqryUpdate.Parambyname('p1').Value := FindRightTypeID(ZemRight.right_cod, ZemRight.owner.owner_type,ZemRight.owner.owner);
 //  ibqryUpdate.Parambyname[2].Value := ZemRight.datebegin;
 //  ibqryUpdate.Parambyname[3].Value := null;
 //  ibqryUpdate.Parambyname[4].Value := null;
 //  ibqryUpdate.Parambyname[5].Value := null;
   ibqryUpdate.Parambyname('p6').AsFloat := ZemRight.Right_ID;
   ibqryUpdate.Open;
   ibqryUpdate.Close;
   Result := True;
   except
   Result := False;
   end;
   end;

  //if Result =False then form4.mmo1.Lines.Add('UpdateRight false') else form4.mmo1.Lines.Add('UpdateRight true');
  if Result =False then form4.WriteTofile(ff,'���������� �����',' ���') else form4.WriteTofile(ff,'���������� �����',' ��');
end;

function tFunctional.UpdateLandUsefact(ZemUch:TZemUch): Boolean;
begin
  with dm2 do
  begin
    try
     ibqryUpdate.Database := myI.Mydb;
     ibqryUpdate.SQL.Clear;
     ibqryUpdate.SQL.Add(_sqlUPDATE_LANDUseFact);
     ibqryUpdate.Parambyname('p1').Value := ZemUch.USe_.UseFact;
     ibqryUpdate.ParamByName('p2').AsFloat := ZemUch.zu_ID;
     ibqryUpdate.ExecSQL;
     ibqryUpdate.Close;
     Result := True;
    except
     Result := False;
    end;
  end;
  //if Result =False then form4.mmo1.Lines.Add('UpdateLandUsefact false') else form4.mmo1.Lines.Add('UpdateLandUsefact true');
  if Result =False then form4.WriteTofile(ff,'���������� ������.������������� ��',' ���') else form4.WriteTofile(ff,'���������� ������.������������� ��',' ��');
end;


function tFunctional.UpdateZemUch(var ZemUch:TZemUch):double;
 begin
   try
      ZemUch.zu_local_prefix := ReadLocal_prefix(ZemUch.zu_cad_num);

      if zemuch.area.isuseV then  UpdateLandArea(ZemUch);
      if zemuch.area.isuseU then  UpdateLandAreaUNIT(ZemUch);

     if ChecklandChar(ZemUch)
         then
           begin
             if zemuch.Category.isuse  then UpdateLandCharCategory(ZemUch);
             if zemuch.CadCost.isUseV  then UpdateLandCharCadCost(ZemUch);
             if zemuch.CadCost.isUseU1 then UpdateLandCharUnit(ZemUch);
           end
         else  insertLandChar(zemuch);
      if ChecklandAdr(zemuch)
         then
           begin
            if ZemUch.Address.isUseID  then  UpdateLandAdrID(ZemUch);
            if ZemUch.Address.isUseNote  then UpdateLandAdrNote(ZemUch);
           end
         else  insertLandAdr(zemUch);
      if ChecklandUse(zemuch)
         then
           begin
             if ZemUch.USe_.isUseID then UpdateLandUseValue(ZemUch);
             if ZemUch.USe_.isUsefact then UpdateLandUsefact(ZemUch);
           end
         else  insertLandUse(zemuch);
      Result := ZemUch.zu_ID;
      myI.MyTrAc.CommitRetaining;
   except
      Result := 0;
      myI.MyTrAc.RollbackRetaining;
   end;
  // if Result =0 then form4.mmo1.Lines.Add('UpdateZemUch ���') else form4.mmo1.Lines.Add('UpdateZemUch true');
   if Result =0 then form4.WriteTofile(ff,'���������� �� �����:',' ���') else form4.WriteTofile(ff,'���������� �� �����:',' ��');
 end;

function tFunctional.InsertNewUse(zemUch: TZemUch): double;
var UseId : Double;
begin
 with dm2 do
 begin
   UseId := GEnUseId(zemUch.zu_cad_num);
   if ibqryINsert2.Active then ibqryINsert2.Close;

   ibqryINsert2.Database := myI.Mydb;
   ibqryINsert2.SQL.Clear;
   ibqryINsert2.SQL.Add(_sqlIsertTOT_use);
   ibqryINsert2.Parambyname('p1').AsFloat := UseId;
   ibqryINsert2.ParamByName('p2').AsString := zemUch.USe_.UseCode;
   ibqryINsert2.ParamByName('p3').AsString  := zemUch.USe_.UseFact;
   ibqryINsert2.ExecSQL;
   ibqryINsert2.Close;
   Result:=UseId;
 end;
end;

function tFunctional.InsertRight(var ZemRight: TZemRight; vKn:string): Boolean;
begin
 with dm2 do
  begin
    ZemRight.Right_ID := LandGenRightId(vKn);
    ZemRight.right_type_id := FindRightTypeID(ZemRight.right_cod, ZemRight.owner.owner_type,ZemRight.owner.owner);

    try
     ibqryINsert.Database := myI.Mydb;
     ibqryINsert.SQL.Clear;
     ibqryINsert.SQL.Add(_sqlInsertRight1);
     ibqryINsert.SQL.Add(_sqlInsertRight2);
     ibqryINsert.Parambyname('p1').AsFloat := ZemRight.Right_ID;
     ibqryINsert.ParamByName('p3').AsFloat := ZemRight.right_type_id;
     ibqryINsert.ParamByName('p4').AsDate := Date();
     ibqryINsert.ParamByName('p5').AsDate := ZemRight.datebegin;
     ibqryINsert.ParamByName('p6').Value := null;
     ibqryINsert.ParamByName('p7').Value := null;
     ibqryINsert.ParamByName('p8').Value := null;
     ibqryINsert.ParamByName('p9').AsInteger := ReadLocal_prefix(vKn);
     ibqryINsert.ExecSQL;
     ibqryINsert.Close;
     Result := True;
    except
     Result := False;
    end;
  end;
  //if Result =False then form4.mmo1.Lines.Add('InsertRight false') else form4.mmo1.Lines.Add('InsertRight true');
  if Result =False then form4.WriteTofile(ff,'������� �����',' ���') else form4.WriteTofile(ff,'������� �����',' ��');
end;


function tFunctional.FindPersonbyName(name1 : string): Double;
var ss1,ss2,ss3 : string; i,i2 : Integer;
begin
  i := Pos(' ',name1);
  ss1 := copy(name1,1,i-1);
  i2:=Pos(' ',name1,i+1);
  ss2 := Copy(name1, i+1, i2-i-1);
  ss3 := Copy(name1, i2+1, Length(name1));
  with dm2 do
  begin
    try
     ibqryFind.Database := myI.Mydb;
     ibqryFind.SQL.Clear;
     ibqryFind.SQL.Add(_sqlFindPersonbyName);
     ibqryFind.Parambyname('p1').Value := ss1;
     ibqryFind.ParamByName('p2').Value := ss2;
     ibqryFind.ParamByName('p3').Value := ss3;
     ibqryFind.Open;
     if not ibqryFind.IsEmpty
       then
         Result := ibqryFind.Fields[0].Value
       else
         Result := 0;
     ibqryFind.Close;
    except
     Result := 0;
    end;
  end;
end;

function tFunctional.FindEnterprisebyName(name1 : string): Double;
var ss,ss1 : string;   ic,ic2:Integer;
begin
  with dm2 do
  begin
    try
     ibqryFind.Database := myI.Mydb;
     ibqryFind.SQL.Clear;
     ibqryFind.SQL.Add(_sqlFindEnterprisebyName);
  {   ss1 := name1;
     ic:= Pos('"', ss1);
     if ic <>0 then
       begin
          ic2 := Pos('"',ss1, ic);
          ss:= Copy(name,1,ic-1)+ Copy(name,ic+1,ic2-Pos('"', ss1)-1)+ Copy(ss1,ic2+1,Length(ss1));
       //   name := ss;
       end;         }

     ibqryFind.Parambyname('p1').Value := name1;
     ibqryFind.Open;
     if not ibqryFind.IsEmpty
       then
         Result := ibqryFind.Fields[0].Value
       else
         Result := 0;
     ibqryFind.Close;
    except
     Result := 0;
    end;
  end;
end;

function tFunctional.createPerson(name : string; ow_count:integer; vKN:string): Double;
var ss1,ss2,ss3 : string; i,i2 : Integer;  id:Double;
begin
 if ow_count = 1  then
   begin
     i := Pos(' ',name);
     ss1 := copy(name,1,i-1);
     i2:=Pos(' ',name,i+1);
     ss2 := Copy(name, i+1, i2-i-1);
     ss3 := Copy(name, i2+1, Length(name));
   end
 else
  if ow_count >1 then
   begin
     ss1 := IntToStr(ow_count) + ' ������������(��)/��������(��) �������';
     ss2 := '';
     ss3 := '';
   end;

  id := GenPersonId(vKn);

  with dm2 do
  begin
    try
    if ibqryINsert2.Active then ibqryINsert2.Close;
     ibqryInsert2.Database := myI.Mydb;
     ibqryInsert2.SQL.Clear;
     ibqryInsert2.SQL.Add(_sqlInsertPerson);
     ibqryInsert2.Parambyname('p1').AsFloat := id;
     ibqryInsert2.Parambyname('p2').Value := ss1;
     ibqryInsert2.ParamByName('p3').Value := ss2;
     ibqryInsert2.ParamByName('p4').Value := ss3;
     ibqryInsert2.ParamByName('p5').AsInteger := ReadLocal_prefix(vKn);
     ibqryInsert2.ParamByName('p6').AsDate := Date();
     ibqryInsert2.ExecSQL;
     ibqryInsert2.Close;
     Result := id;
    except
     Result := 0;
    end;
  end;
  if Result =0 then form4.WriteTofile(ff,'������� ������ ������������',' ���') else form4.WriteTofile(ff,'������� ������ ������������',' ��');
end;


function tFunctional.createEnterprise(Ename : string; vKn : string): Double;
var id : Double;
begin
  with dm2 do
  begin
    try
    if ibqryINsert2.Active then ibqryINsert2.Close;
     id := GenEnterpriseId(vKn);
     ibqryInsert2.Database := myI.Mydb;
     ibqryInsert2.SQL.Clear;
     ibqryInsert2.SQL.Add(_sqlInsertenterprise);
     ibqryInsert2.Parambyname('p1').AsFloat := id;
     ibqryInsert2.Parambyname('p2').AsString := eName;
     ibqryInsert2.ParamByName('p3').asstring := Ename;
     ibqryInsert2.ParamByName('p4').AsInteger := ReadLocal_prefix(vKn);
     ibqryInsert2.ParamByName('p5').AsDate := Date();
     ibqryInsert2.SQL.CommaText;
     ibqryInsert2.ExecSQL;
     ibqryInsert2.Close;
     Result := id;
    except
     Result := 0;
    end;
  end;
  if Result =0 then form4.WriteTofile(ff,'������� ������ ������������',' ���') else form4.WriteTofile(ff,'������� ������ ������������',' ��');
end;

function tFunctional.InsertRightOwner(var ZemRight: TZemRight; vKn:string; lrID : double): Boolean;
var ow_id : Double;
begin
 with dm2 do
  begin
    try
     ibqryINsert.Database := myI.Mydb;
     ibqryINsert.SQL.Clear;
     ibqryINsert.SQL.Add(_sqlInsertRightOwner1);
     ibqryINsert.SQL.Add(_sqlInsertRightOwner2);
     ibqryINsert.Parambyname('p1').AsFloat := LandGenRightOwnerId(vKn);
     ibqryINsert.ParamByName('p2').AsFloat := lrID;
     if ZemRight.owner.ownerCount =1
     then
     begin
       if ZemRight.owner.owner_type =1
        then
          begin
            ow_id := FindPersonbyName(ZemRight.owner.owner);
            if ow_id = 0 then ow_id := createPerson(ZemRight.owner.owner, ZemRight.owner.ownerCount, vKn);

            ibqryINsert.ParamByName('p3').AsFloat := Ow_id;
            ibqryINsert.ParamByName('p4').Value := null;
          end
        else      //2,3
          begin
            ow_id := FindEnterprisebyName(ZemRight.owner.owner);
            if ow_id = 0 then ow_id := createEnterprise(ZemRight.owner.owner, vKn);

            ibqryINsert.ParamByName('p3').Value := null;
            ibqryINsert.ParamByName('p4').AsFloat := ow_id;
          end  ;
     end
     else
     if ZemRight.owner.ownerCount > 1
     then
     begin
       ow_id := FindPersonbyName(ZemRight.owner.owner);
       if ow_id = 0 then ow_id := createPerson(ZemRight.owner.owner, ZemRight.owner.ownerCount, vKn);

       ibqryINsert.ParamByName('p3').AsFloat := Ow_id;
       ibqryINsert.ParamByName('p4').Value := null;
     end;

     if ZemRight.owner.owner_type = 1
      then  ibqryINsert.ParamByName('p5').AsString := '���������� ����'
      else  ibqryINsert.ParamByName('p5').AsString := '����������� ����';

     ibqryINsert.ParamByName('p66').value := null;
     ibqryINsert.ParamByName('p77').value := null;
     ibqryINsert.ParamByName('p8').AsInteger := ReadLocal_prefix(vKn);
     ibqryINsert.ParamByName('p9').AsDate := Date();
     if ZemRight.owner.ownerCount > 1
     then
     begin
     ibqryINsert.ParamByName('p10').Value := 1;
     ibqryINsert.ParamByName('p11').Value := 1;
     end
     else
     begin
     ibqryINsert.ParamByName('p10').Value := ZemRight.owner.owner_drob1;
     ibqryINsert.ParamByName('p11').Value := ZemRight.owner.owner_drob2;
     end;
     ibqryINsert.ExecSQL;
     ibqryINsert.Close;
     Result := True;
    except
     Result := False;
    end;
  end;
 // if Result =False then form4.mmo1.Lines.Add('InsertRight false') else form4.mmo1.Lines.Add('InsertRight true');
  if Result =False then form4.WriteTofile(ff,'������� ����� ������������ � �����',' ���') else form4.WriteTofile(ff,'������� ����� ������������ � �����',' ��');
end;

function tFunctional.InsertRightReg(ZemRight: TZemRight; vKn:string; pregtype:integer): Boolean;
begin
  with dm2 do
  begin
    try
     ibqryINsert.Database := myI.Mydb;
     ibqryINsert.SQL.Clear;
     ibqryINsert.SQL.Add(_sqlInsertRightregistration);
     ibqryINsert.Parambyname('p1').AsFloat := LandGenRightRegId(vKn);
     ibqryINsert.ParamByName('p2').AsFloat := pregtype;
     ibqryINsert.ParamByName('p3').AsFloat := ZemRight.Right_ID;
     ibqryINsert.ParamByName('p4').AsInteger := ReadLocal_prefix(vKn);
     ibqryINsert.ParamByName('p5').AsDate := Date();
     ibqryINsert.ExecSQL;
     ibqryINsert.Close;
     Result := True;
    except
     Result := False;
    end;
  end;
  //if Result =False then form4.mmo1.Lines.Add('InsertRight false') else form4.mmo1.Lines.Add('InsertRight true');
  if Result =False then form4.WriteTofile(ff,'������� �����/����',' ���') else form4.WriteTofile(ff,'������� �����/����',' ��');
end;

function  tFunctional.InsertZemRight(var zemRight: TZemRight; vKn:string):boolean;
var lrId :Double;
 begin
   try
      InsertRight(zemRight,vKn);
      InsertRightReg(zemRight,vKn,1);
      if zemRight.right_ufrs
        then InsertRightReg(zemRight,vKn,2);
      if (ZemRight.Doc.doc_code <>'')and (ZemRight.Doc.doc_name <> '')
        then InsertDoc(zemRight,vKn);
      lrId := InsertLandRight(zemRight,vKn);
      if lrId <> 0
        then InsertRightOwner(zemRight,vKn, lrId);
      InsertZemRight := true;
      myI.MyTrAc.CommitRetaining;
   except
      InsertZemRight := false;
      myI.MyTrAc.RollbackRetaining;
   end;
 end;

 function  tFunctional.UpdateZemRight(var ZemRight:TZemRight; vKn : string):Boolean;
 var ii , ll: Integer;
 begin
   // �������� �� ������������ �����
   // ���� ���� ��������� �� ���������� �� ���� �� ������ �� ���� ������������
   // ���� ������������ �� ������� - ��������� �����? ��� ��������� ������������??
   ll:=0;
   try
      if UpdateRight(ZemRight) then ll:=ll+1;
      if UpdateRightReg(ZemRight, vKn) then ll := ll+1;
   //   UpdateDoc(ZemRight);
   //   UpadateLandRight(ZemRight);
     if ZemRight.owner.ownerCount <>0
     then
      begin
         ii:= CheckCountRihtOwners(ZemRight) ;
         if  ii = 1
          then
          begin
            if UpdateRightOwner(ZemRight, vKn) then ll:= ll+1
          end
          else
          begin
            if ii > 1
              then form4.WriteTofile(ff,'��������� �������������. �������������� ���������� ���� ����������','')//form4.mmo1.Lines.Add('��������� ���c���������. �������������� ���������� ���� ����������')
              else InsertRightOwner(ZemRight,vKn,ZemRight.lright_id) ;
          end;
      end;
       myI.MyTrAc.CommitRetaining;
      UpdateZemRight := true;
   except
      UpdateZemRight := false;
      myI.MyTrAc.RollbackRetaining;
   end;
 end;

 function  tFunctional.WhatInsertOrUpdate(zemUch : TZemUch):boolean;
 begin
   // ���������� ����� �� �������
   zemUch.zu_ID :=  FindKN(zemUch.zu_cad_num);

   if zemUch.zu_ID =0
   then
     begin
      if InsertZemUch(zemUch)<>0
       then Result := True
       else Result := False;
     end
   else
      begin
       UpdateZemUch(ZemUch);
       Result := True;
      end;
 end;


 function  tFunctional.WhatInsertOrUpdate(var zemRight : Tarray<TZemright>; zemUch:TZemUch; rightOrarenda:integer):boolean;
 //var ok :Boolean;
 begin
//   // ���������� ����� �� �������
//   if  zemuch.zu_ID = 0
//     then  zemRight.land_id :=  FindKN(zemUch.zu_cad_num);
//
//   if zemRight.land_id <> 0
//    then
//      if FindLandRightID(zemRight.land_id)=0  then   // �� ������� ����� �� �������
//         begin
//           zemRight.land_id := InsertZemUch(zemUch);      // zemuch ���������???????
//           // �������� tlandright
//
//           if  zemRight.land_id <> 0  // ������� �������
//            then begin
//                   InsertZemRight(zemRight, zemUch.zu_cad_num);  // ����� �����
//                   Result := True;
//                 end
//            else
//               Result := False;  // �� ���������� ��������
//         end
//       else
//         begin
//          UpdateZemright(zemRight);
//          Result := True;
//         end
//    else
//     Form4.mmo1.Lines.Add('�� �������. �� � ���� �����������');

 //  if zemRight[0].owner.ownerCount = 1 then
 //  begin
      // ���������� ����� �� �������
      if  zemuch.zu_ID = 0
        then  zemRight[0].land_id :=  FindKN(zemUch.zu_cad_num);
      if zemRight[0].land_id <> 0 // ������� �����
      then
        begin
         // �������� ���� �� ����� �� ���� �������
          if rightOrarenda = 1 then    zemRight[0].lright_id := FindLandRightID(zemRight[0].land_id)
                               else    zemRight[0].lright_id := FindLandRightIDArenda(zemRight[0].land_id);
          if zemRight[0].lright_id =0  then   // �� ������� ����� �� �������
            begin
              InsertZemRight(zemRight[0], zemUch.zu_cad_num);  // ����� �����
              Result := True;
            end
          else
            begin
              UpdateZemright(zemRight[0],zemUch.zu_cad_num );
              Result := True;
            end;
        end
      else
     //Form4.mmo1.Lines.Add('��� �������. �� � ���� �����������'+ zemUch.zu_cad_num);
       form4.WriteTofile(ff,'��� �������. �� � ���� �����������'+ zemUch.zu_cad_num ,'');
//   end
//    else
//    if  High(zemRights) > 1
//    then
//      begin
//      end ;
 end;


 function  tFunctional.WhatInsertOrUpdate(var zemRight : TZemright; zemUch:TZemUch; rightOrarenda:integer):boolean;
 begin
     // ���������� ����� �� �������
      if  zemuch.zu_ID = 0
        then  zemRight.land_id :=  FindKN(zemUch.zu_cad_num);
      if zemRight.land_id <> 0 // ������� �����
      then
        begin
         // �������� ���� �� ����� �� ���� �������
          if rightOrarenda = 1 then    zemRight.lright_id := FindLandRightID(zemRight.land_id)
                               else    zemRight.lright_id := FindLandRightIDArenda(zemRight.land_id);
          if zemRight.lright_id =0  then   // �� ������� ����� �� �������
            begin
              InsertZemRight(zemRight, zemUch.zu_cad_num);  // ����� �����
              Result := True;
            end
          else
            begin
              UpdateZemright(zemRight,zemUch.zu_cad_num );
              Result := True;
            end;
        end
      else
     //Form4.mmo1.Lines.Add('��� �������. �� � ���� �����������'+ zemUch.zu_cad_num);
       form4.WriteTofile(ff,'��� �������. �� � ���� �����������'+ zemUch.zu_cad_num, '');
 end;

 procedure tFunctional.ClearZemUch(var ZemUch: TZemUch);
 begin
   ZemUch.Clear;
 end;

  procedure tFunctional.GetZu(var ZemUch:TzemUch; vKN:string);
 begin
    zemUch.zu_cad_num := vKN;
    zemUch.zu_ID :=  FindKN(vKn);
   if zemUch.zu_ID =0
   then
     begin
       ClearZemUch(ZemUch);       
       //GetZu := False;
     end
   else
     begin
       if ZemUch.ifreadArea then ReadLand(ZemUch);
       if ZemUch.ifreadChar then ReadLandChar(ZemUch);
       if ZemUch.ifreadAdr  then ReadLandAdr(ZemUch);
       if ZemUch.ifreadUse  then ReadLandUse(zemUch);
     end;
 end;


function tFunctional.ChecklandAdr(ZemUch: tzemuch): Boolean;
begin
   with dm2 do
   begin
     ibqryCheck.Database := myI.Mydb;
     ibqryCheck.sql.clear;
     ibqryCheck.sql.add(_sqlCheck_LANDAdr);
     ibqryCheck.Params[0].Value := ZemUch.zu_ID;
     ibqryCheck.Open;
     if ibqryCheck.IsEmpty then Result := false else Result := True;
     ibqryCheck.Close;
   end;
end;

function tFunctional.ChecklandChar(ZemUch: tzemuch): Boolean;
begin
   with dm2 do
   begin
     ibqryCheck.Database := myI.Mydb;
     ibqryCheck.sql.clear;
     ibqryCheck.sql.add(_sqlCheck_LANDchar);
     ibqryCheck.Params[0].Value := ZemUch.zu_ID;
     ibqryCheck.Open;
     if ibqryCheck.IsEmpty then Result := false else Result := True;
     ibqryCheck.Close;
   end;
end;

function tFunctional.ChecklandUse(ZemUch: tzemuch): Boolean;
begin
  with dm2 do
   begin
     ibqryCheck.Database := myI.Mydb;
     ibqryCheck.sql.clear;
     ibqryCheck.sql.add(_sqlCheck_LANDUse);
     ibqryCheck.Params[0].Value := ZemUch.zu_ID;
     ibqryCheck.Open;
     if ibqryCheck.IsEmpty then Result := false else Result := True;
     ibqryCheck.Close;
   end;
end;

procedure tFunctional.ClearZemright(var ZemRight: tzemRight);
 begin
   ZemRight.Clear;
 end;

function tFunctional.GenEnterpriseId(vKN: string): Double;
var ii:Double;
 begin
  with dm2 do
   begin
    ii := StrTofloat(Copy(vKn,1,2) + Copy(vKn,4,2)+'00000000000');

    ibqryGenId.Database :=  myI.Mydb;
    ibqryGenId.sql.clear;
    ibqryGenId.sql.add(_sqlGenEntrpriseId);
    ibqryGenId.Open;
    Result := ii + ibqryGenId.Fields[0].AsFloat;
    ibqryGenId.Close;
   end;
end;

function tFunctional.GenPersonId(vKN: string): Double;
var ii:Double;
 begin
  with dm2 do
   begin
    ii := StrTofloat(Copy(vKn,1,2) + Copy(vKn,4,2)+'00000000000');

    ibqryGenId.Database :=  myI.Mydb;
    ibqryGenId.sql.clear;
    ibqryGenId.sql.add(_sqlGenPersonId);
    ibqryGenId.Open;
    Result := ii + ibqryGenId.Fields[0].AsFloat;
    ibqryGenId.Close;
   end;
end;

function tFunctional.GenuseId(vKN: string): Double;
var ii:Double;
 begin
  with dm2 do
   begin
    ii := StrTofloat(Copy(vKn,1,2) + Copy(vKn,4,2)+'00000000000');

    ibqryGenId.Database :=  myI.Mydb;
    ibqryGenId.sql.clear;
    ibqryGenId.sql.add(_sqlGenUseID);
    ibqryGenId.Open;
    Result := ii + ibqryGenId.Fields[0].AsFloat;
    ibqryGenId.Close;
   end;
end;

procedure tFunctional.GetZu(var ZemRight:TzemRight; vKN:string);
 begin
    zemRight.land_id :=  FindKN(vKN);
   if ZemRight.land_id =0
   then
     begin
       ClearZemright(ZemRight);
      // GetZu := False;
     end
   else
     begin
      { ReadLandRight
       ReadRight
       ReadRightReg
       ReadRightDoc
       ReadRightOwner
       GetZuRight := true; }
     end;
 end;






end.
