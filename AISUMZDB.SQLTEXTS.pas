unit AISUMZDB.SQLTEXTS;

interface
uses  System.SysUtils, System.Classes;

     {UDDATE T_LAND}
     const _sqlUPDATE_LANDAREA         :string = 'Update T_land Set area = :p1, identify = 2 where (id = :p2)  and (status <>3) and (identify <> 3)';
     const _sqlUPDATE_LANDAREAunit     :string = 'Update T_land Set unit_type_id = :p1, identify = 2 where (id = :p2) and (status <>3) and (identify <> 3)';
     {Update T_Land_characteristic}
     const _sqlUPDATE_LANDCategory     :string = 'Update T_land_Characteristic Set Land_Category_id = :p1, identify = 2 where (Land_id = :p2) and (status <>3) and (identify <> 3)';
     const _sqlUPDATE_LANDCadCost      :string = 'Update T_land_Characteristic Set cad_price = :p1, identify = 2 where (Land_id = :p2) and (status <>3) and (identify <> 3)';
     const _sqlUPDATE_LANDCadCostUnit  :string = 'Update T_land_Characteristic Set unit_type_id = :p1, identify = 2 where (Land_id = :p2) and (status <>3) and (identify <> 3)';
     {update t_land_location}
     const _sqlUPDATE_LANDAdr          :string = 'Update T_land_Location Set address_plan_id = :p1, identify = 2 where (Land_id = :p2) and (status <>3) and (identify <> 3)';
     const _sqlUPDATE_LANDAdrnote      :string = 'Update T_land_Location Set notes = :p1, identify = 2 where (Land_id = :p2) and (status <>3) and (identify <> 3)';
     {update t_land_use}
     const _sqlUPDATE_LANDUse          :string = 'Update T_land_use Set PERMITTED_USE_TYPE_id = :p1, identify = 2 where (Land_id = :p2) and (status <>3) and (identify <> 3)';
     const _sqlUPDATE_LANDUseFact      :string = 'Update T_land_use Set FACTUAL_USE = :p1, identify = 2 where (Land_id = :p2) and (status <>3) and (identify <> 3)';

     {CHECK}
     const _sqlCheck_LANDchar          :string = 'Select ID from T_LAND_characteristic where (land_id = :p1) and (status <> 3) and (identify <>3) ';
     const _sqlCheck_LANDAdr           :string = 'Select ID from T_LAND_Location where (land_id = :p1) and (status <> 3) and (identify <>3)';
     const _sqlCheck_LANDUse           :string = 'Select ID from T_LAND_Use where (land_id = :p1) and (status <> 3) and (identify <>3) ';
     const _sqlCheckContRightOwners    :string = 'Select count(id) from t_right_owner where land_right_id = :p1';

     {Find}
     const _sqlFind_KN                 :string = 'Select ID from T_LAND where (Cad_num = :p1) and (status <> 3) and (identify <>3)';
     const _sqlFind_AdrbyKladr         :string = 'select id from t_Address_plan where code = :p1 ';
     const _sqlFindUseIdbyCode         :string = 'select id from T_PERMITTED_USE_TYPE where code = :p1';
     const _sqlFindPersonbyName        :string = 'select id from T_Person where (family = :p1) and (name = :p2) and (Last_name = :p3) and (status <> 3) and (identify <> 3)';
     const _sqlFindEnterprisebyName    :string = 'select id from T_Enterprise where (full_name = :p1) and (status <> 3) and (identify <> 3)';
     const _sqlFindLandRightID         :string = 'select lr.id, r.id from T_land_right lr left join t_right r on (lr.right_id =r.id) where (lr.land_id = :p1) and (lr.status <> 3) and (lr.identify <> 3) and (r.status <> 3) and (r.identify <> 3) and (r.right_type_id <> 7)';     // ���� ������ �����, �������� ������
     const _sqlFindLandArendaID        :string = 'select lr.id, r.id from T_land_right lr left join t_right r on (lr.right_id =r.id) where (lr.land_id = :p1) and (lr.status <> 3) and (lr.identify <> 3) and (r.status <> 3) and (r.identify <> 3) and (r.right_type_id = 7)';     // ���� ������ ������
     const _sqlFindRightregistrationbyID :string = 'select Id from T_RIGHT_REGISTRATION_TYPE where (right_id = :p1) and (REGISTRATION_TYPE_ID = :p2)  and (status <> 3) and (identify <> 3)';
     const _sqlFindRightID             :string = 'select right_id from t_Land_right where (id = :p1) and (status <> 3) and (identify <>3) ' ;

     {Gen_Id}
     const _sqlGenLandId               :string = 'select Gen_id(g_land,1) from RDB$DATABASE';
     const _sqlGenLandAdrId            :string = 'select Gen_id(g_land_location,1) from RDB$DATABASE';
     const _sqlGenLandCharid           :string = 'select Gen_id(g_land_characteristic,1) from RDB$DATABASE';
     const _sqlGenLandUseID            :string = 'select Gen_id(g_land_use,1) from RDB$DATABASE';
     const _sqlGenUseID                :string = 'select Gen_id(G_PERMITTED_USE_TYPE,1) from RDB$DATABASE';

     const _sqlGenRightId              :string = 'select Gen_id(g_right,1) from RDB$DATABASE';
     const _sqlGenryteregId            :string = 'select Gen_id(G_RIGHT_REGISTRATION_TYPE,1) from RDB$DATABASE';
     const _sqlGenLandRightId          :string = 'select Gen_id(G_LAND_RIGHT,1) from RDB$DATABASE';
     const _sqlGenRightOwnerId         :string = 'select Gen_id(G_RIGHT_OWNER,1) from RDB$DATABASE';
     const _sqlGenDocId                :string = 'select Gen_id(G_ENTERPRISE_DOC,1) from RDB$DATABASE';
     const _sqlGenrightDocId           :string = 'select Gen_id(G_ENTERPRISE_DOC_RIGHT,1) from RDB$DATABASE';

     const _sqlGenPersonId             :string = 'select Gen_id(G_Person,1) from RDB$DATABASE';
     const _sqlGenEntrpriseId          :string = 'select Gen_id(G_ENTERPRISE,1) from RDB$DATABASE';
     {INSERT}
     const _sqlInsertPredok            :string = 'INSERT INTO T_LAND (ID, PARENT_ID, CAD_NUM, AREA, UNIT_TYPE_ID, KOLKHOZ_ID, STATUS, LOCAL_PREFIX, BEGIN_DATE, END_DATE, GUID, IDENTIFY, INTERNAL_ID1, INTERNAL_ID2) VALUES (:p1,:p2, :p3, null, null, null, 0, :p4, :p5, null, null, 0, null, null)';
     const _sqlInsertLand              :string = 'INSERT INTO T_LAND (ID, PARENT_ID, CAD_NUM, AREA, UNIT_TYPE_ID, KOLKHOZ_ID, STATUS, LOCAL_PREFIX, BEGIN_DATE, END_DATE, GUID, IDENTIFY, INTERNAL_ID1, INTERNAL_ID2) VALUES (:p1,:p2, :p3, :p4, :p5, null, 0, :6, :p7, null, null, 0, null, null)' ;
     const _sqlInsertLandAdr           :string = 'INSERT INTO T_LAND_LOCATION (ID, LAND_ID, ADDRESS_PLAN_ID, NOTES, STATUS, LOCAL_PREFIX, BEGIN_DATE, END_DATE, IDENTIFY, INTERNAL_ID) VALUES (:p1, :p2, :p3, :p4, 0, :p5, :p6, null, 0, null)';
     const _sqlInsertLandChar          :string = 'INSERT INTO T_LAND_CHARACTERISTIC (ID, LAND_CATEGORY_ID, LAND_ID, BEGIN_DATE, END_DATE, IS_INVESTMENT, CAD_PRICE, UNIT_TYPE_ID, STATUS, NOTES, LOCAL_PREFIX, IDENTIFY, INTERNAL_ID) VALUES (:p1, 1, :p2, :p3, null, 0, :p4, :p5, 0, null, :p6, 0, null)';
     const _sqlInsertLandUse           :string = 'INSERT INTO T_LAND_USE (ID, LAND_ID, PERMITTED_USE_TYPE_ID, FACTUAL_USE, BEGIN_DATE, END_DATE, STATUS, LOCAL_PREFIX, USEZUNOTCEL, NOTUSEZU, WITHOUT_RIGHT, IDENTIFY, INTERNAL_ID) VALUES (:p1, :p2, :p3, :p4, :p5, null, 0, :p6, 0, 0, 0, 0, null)';
     const _sqlIsertTOT_use            :string = 'INSERT INTO T_PERMITTED_USE_TYPE (ID, CODE, NAME, NOTES, IS_VISIBLE, ORIGIN) VALUES (:p1, :p2, :p3, NULL, 1, 1);';


     const _sqlRead_land               :string = 'Select Area, unit_type_id from t_land where id = :p1';
     const _sqlRead_landAdr            :string = 'Select address_plan_id, Note from t_land_location where land_id = :p1';
     const _sqlRead_landChar           :string = 'Select Category, Cad_price, unit_type_id from t_land_characteristic where land_id = :p1';
     const _sqlRead_landUse            :string = 'Select PERMITTED_USE_TYPE_id, FACTUAL_USE from t_land_use where land_id = :p1';

     {rights}

     {insert}
     const _sqlInsertLandRight         :string = 'INSERT INTO T_LAND_RIGHT (ID, RIGHT_ID, LAND_ID, LOCAL_PREFIX, BEGIN_DATE, END_DATE, STATUS, IDENTIFY, INTERNAL_ID) VALUES (:p1,:p2,:p3,:p4,:p5,null,0,0,null)';
     const _sqlInsertRight1            :string = 'INSERT INTO T_RIGHT (ID,RESTRICTION_TYPE_ID,CHARGE_TYPE_ID,RIGHT_TYPE_ID,BEGIN_DATE,END_DATE,RIGHT_BEGIN_DATE,RIGHT_END_DATE,PERIOD,UNIT_TYPE_ID,STATUS,LOCAL_PREFIX,NOTES,IDENTIFY,INTERNAL_ID) ';
     const _sqlInsertRight2            :string = ' VALUES (:p1,null,null,:p3,:p4,null,:p5,:p6,:p7,:p8,0,:p9,null,0,null)';
     const _sqlInsertRightOwner1       :string = 'INSERT INTO T_RIGHT_OWNER (ID, LAND_RIGHT_ID, PERSON_ID, ENTERPRISE_ID, OWNER_TYPE, AREA, UNIT_TYPE_ID, LOCAL_PREFIX, BEGIN_DATE, END_DATE, STATUS, SHARE_NUMERATOR, SHARE_DENOMINATOR, IDENTIFY, INTERNAL_ID) ';
     const _sqlInsertRightOwner2       :string = ' VALUES (:p1,:p2,:p3,:p4,:p5,:p66,:p77,:p8,:p9,null,0,:p10,:p11,0,null)';
     const _sqlInsertRightregistration :string = 'INSERT INTO T_RIGHT_REGISTRATION_TYPE (ID, REGISTRATION_TYPE_ID, RIGHT_ID, LOCAL_PREFIX, BEGIN_DATE, END_DATE, STATUS, IDENTIFY, INTERNAL_ID)   VALUES (:p1,:p2,:p3,:p4,:p5,null,0,0,null)';
     const _sqlInsertDoc               :string = 'INSERT INTO T_ENTERPRISE_DOC (ID, DOC_TYPE_ID, ENTERPRISE_ID, SERIA, NUM, DOC_DATE, WHOM, NOTES, BEGIN_DATE, END_DATE, STATUS, LOCAL_PREFIX, IDENTIFY, INTERNAL_ID) VALUES (:p1,:p2,null,:p3,:p4,:p5,:p6,:p7,:p8,null,0,:p9,0,null)';
     const _sqlInsertRightDoc          :string = 'INSERT INTO T_ENTERPRISE_DOC_RIGHT (ID, RIGHT_ID, ENTERPRISE_DOC_ID, LOCAL_PREFIX, BEGIN_DATE, END_DATE, STATUS, IDENTIFY, INTERNAL_ID) VALUES (:p1,:p2,:p3,:p4,:p5,null,0,0,null)';

     const _sqlInsertPerson            :string = 'INSERT INTO T_PERSON (ID, FAMILY, NAME, LAST_NAME, BIRTHDAY, BIRTHPLACE, LOCAL_PREFIX, BEGIN_DATE, END_DATE, STATUS, IDENTIFY, INTERNAL_ID) VALUES (:p1,:p2,:p3,:p4, NULL, NULL, :p5, :p6, NULL, 0, 0, NULL)';
     const _sqlInsertenterprise        :string = 'INSERT INTO T_ENTERPRISE (ID,PARENT_ID,OKOPF_ID,OWNERSHIP_TYPE_ID,OKOGU_ID,FULL_NAME,SHORT_NAME,INN,OGRN,LOCAL_PREFIX,BEGIN_DATE,END_DATE,STATUS,IDENTIFY,INTERNAL_ID) VALUES (:p1,NULL,NULL,NULL,NULL,:p2,:p3,null,null,:p4,:p5,NULL,0,0,NULL)';

     {update}

     const _sqlUpdateRight             :string = 'UPDATE T_RIGHT SET RIGHT_TYPE_ID = :p1, identify = 2  WHERE (ID = :p6)';
     const _sqlUpdateRightOwner1       :string = 'UPDATE T_RIGHT_OWNER SET PERSON_ID = :p1, ENTERPRISE_ID = :p2, OWNER_TYPE = :p3, AREA = :p4, UNIT_TYPE_ID = :p5, SHARE_NUMERATOR = :p6, SHARE_DENOMINATOR = :p7, identify = 2 WHERE (land_right_ID = :p8)';

     {remont}

     const _sqlSelectmultiRights       :string = 'select  lr.right_id  from t_land_right lr where lr.local_prefix = :p1 and lr.status <> 3 and lr.identify <> 3 group by lr.right_id having  count(lr.id)>1';
     const _sqlSelectRight             :string = 'select * from t_right where id = :p1';
     const _sqlInsertRight_1           :string = 'INSERT INTO T_RIGHT (ID,RESTRICTION_TYPE_ID,CHARGE_TYPE_ID,RIGHT_TYPE_ID,BEGIN_DATE,END_DATE,RIGHT_BEGIN_DATE,RIGHT_END_DATE,PERIOD,UNIT_TYPE_ID,STATUS,LOCAL_PREFIX,NOTES,IDENTIFY,INTERNAL_ID) ';
     const _sqlInsertRight_2           :string = ' VALUES (:p1,:p2,:p3,:p4,:p5,:p6,:p7,:p8,:p9,:p10,:p11,:p12,:p13,:p14,:p15)';
     const _sqlSelectLAndRight         :string = 'Select land_id from t_land_right where right_id =:p1';
     const _sqlUpdateLR                :string = 'Update t_land_right set right_id = :p1, identify = 2 where (land_id = :p2) and (right_id = :p3)';
     const _sqlSelectRightRegtype      :string = 'Select * from T_RIGHT_REGISTRATION_TYPE where right_id =:p1 and (status <> 3) and (identify <> 3)';
implementation

end.
